unit Unit1;

interface

uses
  Windows, Messages, Forms, SysUtils, Controls, StdCtrls, ExtCtrls, Classes, Dialogs, Graphics, Math,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxPC,
  AdvGlowButton, AdvEdit, AdvEdBtn,  ESBPCSEdit, ESBPCSNumEdit,
  ESBPCSCalculator, ESBPCSLabel, ESBPCSPanel, ESBPCSConvertors, ESBPCSConversion, ESBPCSUICtrl,
  AdvCombo, WaterSteamPro65, EV200const, MathImage, Mask, AdvSpin, Spin, dxStatusBar,
  GaugesDouble, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxGridCustomTableView, cxGridTableView, cxGridCustomView,
  cxClasses, cxGridLevel, cxGrid, cxGridExportLink, cxGridBandedTableView,
  Menus, cxGridCustomPopupMenu, cxGridPopupMenu, SMCells, SMXLS, Variants,
  cxSpinEdit, cxSpinButton, TeEngine, Series, TeeProcs, Chart, cxContainer,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore,
  dxPScxCommon, dxPScxGrid6Lnk, cxTextEdit, cxMaskEdit;
  
type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    ConvertButton: TAdvGlowButton;
    InputConvValueEdit: TESBFloatEdit;
    OutputConvValueEdit: TESBFloatEdit;
    ESBTempConvertor1: TESBTempConvertor;
    ESBPressureConvertor1: TESBPressureConvertor;
    ESBFlowConvertor1: TESBFlowConvertor;
    ESBFlowMassConvertor1: TESBFlowMassConvertor;
    ESBDensityConvertor1: TESBDensityConvertor;
    ESBVisDynConvertor1: TESBVisDynConvertor;
    ESBVisKinConvertor1: TESBVisKinConvertor;
    ESBUICtrl1: TESBUICtrl;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    Panel2: TPanel;
    Label2: TLabel;
    FlowConvertNormalButton: TAdvGlowButton;
    UnitAdvEditBtn1: TUnitAdvEditBtn;
    UnitAdvEditBtn2: TUnitAdvEditBtn;
    UnitAdvEditBtn3: TUnitAdvEditBtn;
    UnitAdvEditBtn4: TUnitAdvEditBtn;
    FlowConvertActualButton: TAdvGlowButton;
    cxTabSheet5: TcxTabSheet;
    cxTabSheet6: TcxTabSheet;
    cxTabSheet7: TcxTabSheet;
    cxTabSheet8: TcxTabSheet;
    Panel3: TPanel;
    Label3: TLabel;
    PipeThicknessCalcButton: TAdvGlowButton;
    UnitAdvEditBtn6: TUnitAdvEditBtn;
    UnitAdvEditBtn7: TUnitAdvEditBtn;
    UnitAdvEditBtn8: TUnitAdvEditBtn;
    UnitAdvEditBtn5: TUnitAdvEditBtn;
    PipePressureCalcButton: TAdvGlowButton;
    Panel4: TPanel;
    Label4: TLabel;
    Dyn2KinViscButton: TAdvGlowButton;
    UnitAdvEditBtn10: TUnitAdvEditBtn;
    UnitAdvEditBtn12: TUnitAdvEditBtn;
    UnitAdvEditBtn13: TUnitAdvEditBtn;
    Kin2DynViscButton: TAdvGlowButton;
    ESBCalculator1: TESBCalculator;
    Panel5: TPanel;
    Label5: TLabel;
    Flow2VelConvButton: TAdvGlowButton;
    UnitAdvEditBtn9: TUnitAdvEditBtn;
    UnitAdvEditBtn14: TUnitAdvEditBtn;
    UnitAdvEditBtn15: TUnitAdvEditBtn;
    Vel2FlowConvButton: TAdvGlowButton;
    UnitAdvEditBtn11: TUnitAdvEditBtn;
    UnitAdvEditBtn16: TUnitAdvEditBtn;
    UnitAdvEditBtn17: TUnitAdvEditBtn;
    Page1Add2TableButton: TAdvGlowButton;
    UnitAdvEditBtn18: TUnitAdvEditBtn;
    UnitAdvEditBtn20: TUnitAdvEditBtn;
    Label6: TLabel;
    ReynoldsQButton: TAdvGlowButton;
    UnitAdvEditBtn21: TUnitAdvEditBtn;
    ReynoldsVButton: TAdvGlowButton;
    Page2Add2TableButton: TAdvGlowButton;
    Panel6: TPanel;
    Label12: TLabel;
    PressureDropButton: TAdvGlowButton;
    UnitAdvEditBtn22: TUnitAdvEditBtn;
    UnitAdvEditBtn25: TUnitAdvEditBtn;
    UnitAdvEditBtn27: TUnitAdvEditBtn;
    Page5Add2TableButton: TAdvGlowButton;
    UnitAdvEditBtn24: TUnitAdvEditBtn;
    AdvComboBox1: TAdvComboBox;
    Label11: TLabel;
    AdvComboBox2: TAdvComboBox;
    Panel7: TPanel;
    Label13: TLabel;
    BigPipeFlowButton: TAdvGlowButton;
    UnitAdvEditBtn26: TUnitAdvEditBtn;
    UnitAdvEditBtn28: TUnitAdvEditBtn;
    Page6Add2TableButton: TAdvGlowButton;
    UnitAdvEditBtn29: TUnitAdvEditBtn;
    AdvComboBox4: TAdvComboBox;
    UnitAdvEditBtn23: TUnitAdvEditBtn;
    UnitAdvEditBtn30: TUnitAdvEditBtn;
    UnitAdvEditBtn32: TUnitAdvEditBtn;
    UnitAdvEditBtn31: TUnitAdvEditBtn;
    Panel8: TPanel;
    Label15: TLabel;
    WaterSteamButton: TAdvGlowButton;
    UnitAdvEditBtn39: TUnitAdvEditBtn;
    UnitAdvEditBtn40: TUnitAdvEditBtn;
    UnitAdvEditBtn41: TUnitAdvEditBtn;
    UnitAdvEditBtn42: TUnitAdvEditBtn;
    UnitAdvEditBtn43: TUnitAdvEditBtn;
    UnitAdvEditBtn33: TUnitAdvEditBtn;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    Page7Add2TableButton: TAdvGlowButton;
    MathImage1: TMathImage;
    MathImage2: TMathImage;
    MathImage3: TMathImage;
    MathImage4: TMathImage;
    MathImage5: TMathImage;
    MathImage6: TMathImage;
    MathImage7: TMathImage;
    MathImage8: TMathImage;
    MathImage9: TMathImage;
    MathImage10: TMathImage;
    MathImage11: TMathImage;
    MathImage12: TMathImage;
    MathImage14: TMathImage;
    MathImage15: TMathImage;
    MathImage16: TMathImage;
    MathImage17: TMathImage;
    SpinButton1: TSpinButton;
    SpinButton2: TSpinButton;
    SpinButton3: TSpinButton;
    cxTabSheet9: TcxTabSheet;
    Panel9: TPanel;
    Label7: TLabel;
    VFlowSteamButton: TAdvGlowButton;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    UnitAdvEditBtn45: TUnitAdvEditBtn;
    UnitAdvEditBtn46: TUnitAdvEditBtn;
    UnitAdvEditBtn47: TUnitAdvEditBtn;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    AdvComboBox5: TAdvComboBox;
    AdvComboBox6: TAdvComboBox;
    AdvComboBox7: TAdvComboBox;
    AdvSpinEdit1: TAdvSpinEdit;
    AdvSpinEdit2: TAdvSpinEdit;
    AdvSpinEdit3: TAdvSpinEdit;
    AdvSpinEdit4: TAdvSpinEdit;
    AdvSpinEdit5: TAdvSpinEdit;
    AdvSpinEdit6: TAdvSpinEdit;
    AdvSpinEdit7: TAdvSpinEdit;
    AdvSpinEdit8: TAdvSpinEdit;
    AdvSpinEdit9: TAdvSpinEdit;
    AdvSpinEdit10: TAdvSpinEdit;
    MathImage13: TMathImage;
    MathImage18: TMathImage;
    MathImage19: TMathImage;
    MathImage20: TMathImage;
    cxTabSheet10: TcxTabSheet;
    Panel10: TPanel;
    Label24: TLabel;
    MathImage21: TMathImage;
    Flow2FreqButton: TAdvGlowButton;
    UnitAdvEditBtn37: TUnitAdvEditBtn;
    UnitAdvEditBtn38: TUnitAdvEditBtn;
    Freq2FlowButton: TAdvGlowButton;
    AdvComboBox3: TAdvComboBox;
    UnitAdvEditBtn35: TUnitAdvEditBtn;
    AdvComboBox8: TAdvComboBox;
    AdvComboBox9: TAdvComboBox;
    AdvComboBox10: TAdvComboBox;
    AdvComboBox11: TAdvComboBox;
    MathImage22: TMathImage;
    InputUnitCombo: TAdvComboBox;
    OutputUnitCombo: TAdvComboBox;
    UnitParamCombo: TAdvComboBox;
    dxStatusBar1: TdxStatusBar;
    dxStatusBar1Container4: TdxStatusBarContainerControl;
    AdvComboBox12: TAdvComboBox;
    FlowDNCalcButton: TAdvGlowButton;
    Label25: TLabel;
    Label26: TLabel;
    GaugeDouble1: TGaugeDouble;
    AdvComboBox13: TAdvComboBox;
    Label27: TLabel;
    Label28: TLabel;
    Label30: TLabel;
    UnitAdvEditBtn34: TUnitAdvEditBtn;
    Label29: TLabel;
    cxTabSheet11: TcxTabSheet;
    cxTabSheet12: TcxTabSheet;
    cxTabSheet13: TcxTabSheet;
    Panel11: TPanel;
    Label31: TLabel;
    AdvComboBox14: TAdvComboBox;
    AdvComboBox15: TAdvComboBox;
    AdvComboBox16: TAdvComboBox;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1TableView1: TcxGridTableView;
    cxGrid1Column1: TcxGridColumn;
    cxGrid1Column2: TcxGridColumn;
    cxGrid1Column3: TcxGridColumn;
    cxGrid1Column4: TcxGridColumn;
    cxGrid1Column5: TcxGridColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxGrid2: TcxGrid;
    cxGrid2TableView1: TcxGridTableView;
    cxGrid2Column1: TcxGridColumn;
    cxGrid2Column2: TcxGridColumn;
    cxGrid2Column3: TcxGridColumn;
    cxGrid2Column4: TcxGridColumn;
    cxGrid2Column5: TcxGridColumn;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2Column6: TcxGridColumn;
    cxGrid3: TcxGrid;
    cxGrid3TableView1: TcxGridTableView;
    cxGrid3Column1: TcxGridColumn;
    cxGrid3Column2: TcxGridColumn;
    cxGrid3Column3: TcxGridColumn;
    cxGrid3Column4: TcxGridColumn;
    cxGrid3Column5: TcxGridColumn;
    cxGrid3Column6: TcxGridColumn;
    cxGrid3Level1: TcxGridLevel;
    cxGrid4: TcxGrid;
    cxGrid4TableView1: TcxGridTableView;
    cxGrid4Column1: TcxGridColumn;
    cxGrid4Column2: TcxGridColumn;
    cxGrid4Column3: TcxGridColumn;
    cxGrid4Column4: TcxGridColumn;
    cxGrid4Column5: TcxGridColumn;
    cxGrid4Column6: TcxGridColumn;
    cxGrid4Level1: TcxGridLevel;
    cxGrid4Column7: TcxGridColumn;
    cxGrid4Column8: TcxGridColumn;
    cxGrid4Column9: TcxGridColumn;
    cxGrid5: TcxGrid;
    cxGrid5TableView1: TcxGridTableView;
    cxGrid5Column1: TcxGridColumn;
    cxGrid5Column2: TcxGridColumn;
    cxGrid5Column3: TcxGridColumn;
    cxGrid5Column4: TcxGridColumn;
    cxGrid5Column5: TcxGridColumn;
    cxGrid5Column6: TcxGridColumn;
    cxGrid5Column7: TcxGridColumn;
    cxGrid5Column8: TcxGridColumn;
    cxGrid5Level1: TcxGridLevel;
    cxGrid6: TcxGrid;
    cxGrid6TableView1: TcxGridTableView;
    cxGrid6Column1: TcxGridColumn;
    cxGrid6Column2: TcxGridColumn;
    cxGrid6Column3: TcxGridColumn;
    cxGrid6Column4: TcxGridColumn;
    cxGrid6Column5: TcxGridColumn;
    cxGrid6Column6: TcxGridColumn;
    cxGrid6Column7: TcxGridColumn;
    cxGrid6Column8: TcxGridColumn;
    cxGrid6Level1: TcxGridLevel;
    SaveDialog1: TSaveDialog;
    Export2Excel9Button: TAdvGlowButton;
    Page8Add2TableButton: TAdvGlowButton;
    cxGrid7: TcxGrid;
    cxGrid7BandedTableView1: TcxGridBandedTableView;
    cxGrid7BandedTableView1Column1: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column2: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column3: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column4: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column5: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column6: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column7: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column8: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column9: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column10: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column11: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column12: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column13: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column14: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column15: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column16: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column17: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column18: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column19: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column20: TcxGridBandedColumn;
    cxGrid7Level1: TcxGridLevel;
    cxGrid7BandedTableView1Column21: TcxGridBandedColumn;
    cxGrid7BandedTableView1Column22: TcxGridBandedColumn;
    Panel12: TPanel;
    MathImage23: TMathImage;
    AirSteamFlowRangeButton: TAdvGlowButton;
    UnitAdvEditBtn36: TUnitAdvEditBtn;
    Page11Add2TableButton: TAdvGlowButton;
    cxGrid8: TcxGrid;
    cxGrid8TableView1: TcxGridTableView;
    cxGrid8Column1: TcxGridColumn;
    cxGrid8Column2: TcxGridColumn;
    cxGrid8Column3: TcxGridColumn;
    cxGrid8Column4: TcxGridColumn;
    cxGrid8Column6: TcxGridColumn;
    cxGrid8Level1: TcxGridLevel;
    AdvComboBox17: TAdvComboBox;
    UnitAdvEditBtn44: TUnitAdvEditBtn;
    UnitAdvEditBtn48: TUnitAdvEditBtn;
    MathImage24: TMathImage;
    UnitAdvEditBtn49: TUnitAdvEditBtn;
    UnitAdvEditBtn54: TUnitAdvEditBtn;
    UnitAdvEditBtn55: TUnitAdvEditBtn;
    MathImage25: TMathImage;
    MathImage26: TMathImage;
    cxGrid8Column7: TcxGridColumn;
    UnitAdvEditBtn57: TUnitAdvEditBtn;
    AdvComboBox18: TAdvComboBox;
    cxGrid8Column5: TcxGridColumn;
    Page9Add2TableButton: TAdvGlowButton;
    cxGrid9: TcxGrid;
    cxGrid9TableView1: TcxGridTableView;
    cxGrid9Column1: TcxGridColumn;
    cxGrid9Column2: TcxGridColumn;
    cxGrid9Column3: TcxGridColumn;
    cxGrid9Column4: TcxGridColumn;
    cxGrid9Column5: TcxGridColumn;
    cxGrid9Level1: TcxGridLevel;
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    AdvComboBox19: TAdvComboBox;
    AdvComboBox20: TAdvComboBox;
    AdvComboBox21: TAdvComboBox;
    AdvSpinEdit11: TAdvSpinEdit;
    AdvSpinEdit12: TAdvSpinEdit;
    AdvSpinEdit13: TAdvSpinEdit;
    cxGrid10: TcxGrid;
    cxGrid10TableView1: TcxGridTableView;
    cxGrid10TableView1Column1: TcxGridColumn;
    cxGrid10TableView1Column2: TcxGridColumn;
    cxGrid10TableView1Column3: TcxGridColumn;
    cxGrid10Level1: TcxGridLevel;
    FillDataTableButton: TAdvGlowButton;
    cxGrid10Level2: TcxGridLevel;
    cxGrid10Level3: TcxGridLevel;
    cxGrid10Level4: TcxGridLevel;
    cxGrid10TableView2: TcxGridTableView;
    cxGrid10TableView3: TcxGridTableView;
    cxGrid10TableView4: TcxGridTableView;
    cxGrid10TableView2Column1: TcxGridColumn;
    cxGrid10TableView2Column2: TcxGridColumn;
    cxGrid10TableView2Column3: TcxGridColumn;
    cxGrid10TableView2Column4: TcxGridColumn;
    cxGrid10TableView2Column5: TcxGridColumn;
    cxGrid10TableView2Column6: TcxGridColumn;
    cxGrid10TableView3Column1: TcxGridColumn;
    cxGrid10TableView3Column2: TcxGridColumn;
    cxGrid10TableView3Column3: TcxGridColumn;
    cxGrid10TableView3Column4: TcxGridColumn;
    cxGrid10TableView3Column5: TcxGridColumn;
    cxGrid10TableView3Column6: TcxGridColumn;
    cxGrid10TableView4Column1: TcxGridColumn;
    cxGrid10TableView4Column2: TcxGridColumn;
    cxGrid10TableView4Column3: TcxGridColumn;
    cxGrid10TableView4Column4: TcxGridColumn;
    cxGrid10TableView4Column5: TcxGridColumn;
    cxGrid10TableView4Column6: TcxGridColumn;
    cxGrid10TableView2Column7: TcxGridColumn;
    cxGrid10TableView2Column8: TcxGridColumn;
    cxGrid10TableView1Column4: TcxGridColumn;
    cxGrid10TableView1Column5: TcxGridColumn;
    cxGrid10TableView1Column6: TcxGridColumn;
    cxGrid10TableView1Column7: TcxGridColumn;
    cxGrid10TableView1Column8: TcxGridColumn;
    cxGrid10TableView1Column9: TcxGridColumn;
    cxGrid10TableView1Column10: TcxGridColumn;
    cxGrid10TableView1Column11: TcxGridColumn;
    cxGrid10TableView1Column12: TcxGridColumn;
    cxGrid10TableView1Column13: TcxGridColumn;
    cxGrid10TableView1Column14: TcxGridColumn;
    cxGrid10TableView1Column15: TcxGridColumn;
    cxGrid10TableView1Column16: TcxGridColumn;
    cxGrid10TableView1Column17: TcxGridColumn;
    cxGrid10TableView1Column18: TcxGridColumn;
    cxGrid10TableView1Column19: TcxGridColumn;
    cxGrid10TableView1Column20: TcxGridColumn;
    cxGrid10TableView1Column21: TcxGridColumn;
    cxGrid10TableView1Column22: TcxGridColumn;
    cxGrid10TableView1Column23: TcxGridColumn;
    cxGrid10TableView1Column24: TcxGridColumn;
    cxGrid10TableView1Column25: TcxGridColumn;
    cxGrid10TableView1Column26: TcxGridColumn;
    cxGrid10TableView1Column27: TcxGridColumn;
    cxGrid10TableView1Column28: TcxGridColumn;
    cxGrid10Level5: TcxGridLevel;
    cxGrid10TableView5: TcxGridTableView;
    cxGrid10TableView5Column1: TcxGridColumn;
    cxGrid10TableView5Column2: TcxGridColumn;
    cxGrid10TableView5Column3: TcxGridColumn;
    cxGrid10TableView5Column4: TcxGridColumn;
    cxGrid10TableView2Column9: TcxGridColumn;
    cxGrid10Level6: TcxGridLevel;
    cxGrid10TableView6: TcxGridTableView;
    cxGrid10TableView6Column1: TcxGridColumn;
    cxGrid10TableView6Column2: TcxGridColumn;
    cxGrid10TableView3Column7: TcxGridColumn;
    cxGrid10TableView3Column8: TcxGridColumn;
    cxGrid10TableView3Column9: TcxGridColumn;
    cxGrid10TableView3Column10: TcxGridColumn;
    cxGrid10TableView3Column11: TcxGridColumn;
    cxGrid10TableView4Column7: TcxGridColumn;
    cxGrid10TableView4Column8: TcxGridColumn;
    cxGrid10TableView4Column9: TcxGridColumn;
    cxGrid10TableView4Column10: TcxGridColumn;
    DataXLSImportButton: TAdvGlowButton;
    cxGrid10TableView1Column29: TcxGridColumn;
    Panel13: TPanel;
    dxComponentPrinter1: TdxComponentPrinter;
    PrintTableButton: TAdvGlowButton;
    dxComponentPrinter1Link1: TdxGridReportLink;
    UnitAdvEditBtn51: TUnitAdvEditBtn;
    UnitAdvEditBtn52: TUnitAdvEditBtn;
    ESBPowerConvertor1: TESBPowerConvertor;
    ESBAreaConvertor1: TESBAreaConvertor;
    RadioGroup1: TRadioGroup;
    UnitAdvEditBtn53: TUnitAdvEditBtn;
    cxGrid10TableView1Column30: TcxGridColumn;
    cxGrid10TableView1Column31: TcxGridColumn;
    UnitAdvEditBtn56: TUnitAdvEditBtn;
    UnitAdvEditBtn58: TUnitAdvEditBtn;
    UnitAdvEditBtn59: TUnitAdvEditBtn;
    MathImage27: TMathImage;
    cxGrid5Column9: TcxGridColumn;
    cxGrid5Column10: TcxGridColumn;
    cxGrid5Column11: TcxGridColumn;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    UnitAdvEditBtn60: TUnitAdvEditBtn;
    AdvComboBox22: TAdvComboBox;
    AdvComboBox23: TAdvComboBox;
    CheckBox6: TCheckBox;
    QVReynoldsButton: TAdvGlowButton;
    cxGrid10TableView1Column32: TcxGridColumn;
    cxGrid10TableView1Column33: TcxGridColumn;
    cxGrid10TableView1Column34: TcxGridColumn;
    cxGrid10TableView1Column35: TcxGridColumn;
    cxGrid11: TcxGrid;
    cxGrid11TableView1: TcxGridTableView;
    cxGrid11Column1: TcxGridColumn;
    cxGrid11Column2: TcxGridColumn;
    cxGrid11Column3: TcxGridColumn;
    cxGrid11Column4: TcxGridColumn;
    cxGrid11Column5: TcxGridColumn;
    cxGrid11Level1: TcxGridLevel;
    Page4Add2TableButton: TAdvGlowButton;
    AdvComboBox24: TAdvComboBox;
    cxGrid6Column9: TcxGridColumn;
    cxGrid6Column10: TcxGridColumn;
    cxGrid6Column11: TcxGridColumn;
    AdvComboBox25: TAdvComboBox;
    UnitAdvEditBtn19: TUnitAdvEditBtn;
    CheckBox7: TCheckBox;
    cxSpinButton1: TcxSpinButton;
    CheckBox8: TCheckBox;
    UnitAdvEditBtn50: TUnitAdvEditBtn;
    Label22: TLabel;
    AdvComboBox26: TAdvComboBox;
    GroupBox1: TGroupBox;
    AdvComboBox27: TAdvComboBox;
    UnitAdvEditBtn62: TUnitAdvEditBtn;
    CheckBox9: TCheckBox;
    UnitAdvEditBtn63: TUnitAdvEditBtn;
    UnitAdvEditBtn64: TUnitAdvEditBtn;
    cxGrid8Column8: TcxGridColumn;
    cxGrid10TableView4Column11: TcxGridColumn;
    Chart1: TChart;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    CheckBox10: TCheckBox;
    PresFlowTableButton: TAdvGlowButton;
    cxGrid10TableView4Column12: TcxGridColumn;
    CheckBox11: TCheckBox;
    Chart2: TChart;
    UnitAdvEditBtn65: TUnitAdvEditBtn;
    cxGrid8Column9: TcxGridColumn;
    CheckBox12: TCheckBox;
    UnitAdvEditBtn66: TUnitAdvEditBtn;
    MathImage28: TMathImage;
    PopupMenu2: TPopupMenu;
    N4: TMenuItem;
    SaveDialog2: TSaveDialog;
    AdvGlowButton1: TAdvGlowButton;
    UnitAdvEditBtn61: TUnitAdvEditBtn;
    MathImage29: TMathImage;
    cxGrid9Column6: TcxGridColumn;
    DNFlowTableButton: TAdvGlowButton;
    UnitAdvEditBtn67: TUnitAdvEditBtn;
    UnitAdvEditBtn68: TUnitAdvEditBtn;
    MathImage30: TMathImage;
    MathImage31: TMathImage;
    MathImage32: TMathImage;
    UnitAdvEditBtn69: TUnitAdvEditBtn;
    MathImage33: TMathImage;
    MathImage34: TMathImage;
    UnitAdvEditBtn70: TUnitAdvEditBtn;
    procedure ConvertButtonClick(Sender: TObject);
    procedure InputConvValueEditChange(Sender: TObject);
    procedure InputUnitComboChange(Sender: TObject);
    procedure OutputUnitComboChange(Sender: TObject);
    procedure UnitParamComboChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FlowConvertNormalButtonClick(Sender: TObject);
    procedure FlowConvertActualButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn3KeyPress(Sender: TObject; var Key: Char);
    procedure UnitAdvEditBtn4KeyPress(Sender: TObject; var Key: Char);
    procedure PipePressureCalcButtonClick(Sender: TObject);
    procedure PipeThicknessCalcButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn8KeyPress(Sender: TObject; var Key: Char);
    procedure UnitAdvEditBtn6UnitChanged(Sender: TObject; NewUnit: String);
    procedure Dyn2KinViscButtonClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Flow2VelConvButtonClick(Sender: TObject);
    procedure Vel2FlowConvButtonClick(Sender: TObject);
    procedure Kin2DynViscButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn12KeyPress(Sender: TObject; var Key: Char);
    procedure UnitAdvEditBtn13KeyPress(Sender: TObject; var Key: Char);
    procedure UnitAdvEditBtn14KeyPress(Sender: TObject; var Key: Char);
    procedure UnitAdvEditBtn15KeyPress(Sender: TObject; var Key: Char);
    procedure UnitAdvEditBtn17KeyPress(Sender: TObject; var Key: Char);
    procedure ReynoldsQButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn18KeyPress(Sender: TObject; var Key: Char);
    procedure ReynoldsVButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn21KeyPress(Sender: TObject; var Key: Char);
    procedure Page1Add2TableButtonClick(Sender: TObject);
    procedure Page2Add2TableButtonClick(Sender: TObject);
    procedure AdvComboBox2Change(Sender: TObject);
    procedure Page5Add2TableButtonClick(Sender: TObject);
    procedure PressureDropButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn25KeyPress(Sender: TObject; var Key: Char);
    procedure AdvComboBox4Change(Sender: TObject);
    procedure UnitAdvEditBtn32KeyPress(Sender: TObject; var Key: Char);
    procedure BigPipeFlowButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn26KeyPress(Sender: TObject; var Key: Char);
    procedure Page6Add2TableButtonClick(Sender: TObject);
    procedure WaterSteamButtonClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure UnitAdvEditBtn39KeyPress(Sender: TObject; var Key: Char);
    procedure Page7Add2TableButtonClick(Sender: TObject);
    procedure SpinButton1DownClick(Sender: TObject);
    procedure SpinButton1UpClick(Sender: TObject);
    procedure SpinButton2DownClick(Sender: TObject);
    procedure SpinButton2UpClick(Sender: TObject);
    procedure SpinButton3DownClick(Sender: TObject);
    procedure SpinButton3UpClick(Sender: TObject);
    procedure VFlowSteamButtonClick(Sender: TObject);
    procedure CheckBox4Click(Sender: TObject);
    procedure CheckBox5Click(Sender: TObject);
    procedure AdvComboBox3Change(Sender: TObject);
    procedure Freq2FlowButtonClick(Sender: TObject);
    procedure Flow2FreqButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn35KeyPress(Sender: TObject; var Key: Char);
    procedure FlowDNCalcButtonClick(Sender: TObject);
    procedure AdvComboBox13Change(Sender: TObject);
    procedure AdvComboBox14Change(Sender: TObject);
    procedure AdvComboBox15Change(Sender: TObject);
    procedure Export2Excel9ButtonClick(Sender: TObject);
    procedure Page8Add2TableButtonClick(Sender: TObject);
    procedure AdvComboBox17Change(Sender: TObject);
    procedure AirSteamFlowRangeButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn36KeyPress(Sender: TObject; var Key: Char);
    procedure Page11Add2TableButtonClick(Sender: TObject);
    procedure Page9Add2TableButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn37KeyPress(Sender: TObject; var Key: Char);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure AdvComboBox19Change(Sender: TObject);
    procedure FillDataTableButtonClick(Sender: TObject);
    procedure ImportXLSArrayData;
    procedure DataXLSImportButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn5UnitChanged(Sender: TObject; NewUnit: String);
    procedure cxPageControl1MouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure UnitAdvEditBtn6KeyPress(Sender: TObject; var Key: Char);
    procedure AdvSpinEdit10Change(Sender: TObject);
    procedure AdvSpinEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure PrintTableButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn1UnitChanged(Sender: TObject; NewUnit: String);
    function SetESBConvertorsUnits(cUnit: string; Dir: ShortInt): Real;
    procedure UnitAdvEditBtn1ClickBtn(Sender: TObject);
    procedure AdvComboBox9Change(Sender: TObject);
    procedure AdvComboBox9Enter(Sender: TObject);
    procedure AdvSpinEdit11KeyPress(Sender: TObject; var Key: Char);
    procedure RadioGroup1Click(Sender: TObject);
    procedure AdvComboBox22Change(Sender: TObject);
    procedure AdvComboBox23Change(Sender: TObject);
    procedure QVReynoldsButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn20KeyPress(Sender: TObject; var Key: Char);
    procedure Page4Add2TableButtonClick(Sender: TObject);
    procedure UnitAdvEditBtn36UnitChanged(Sender: TObject;
      NewUnit: String);
    procedure CheckBox8Click(Sender: TObject);
    procedure AdvComboBox24Change(Sender: TObject);
    procedure AdvComboBox27Change(Sender: TObject);
    procedure UnitAdvEditBtn55MouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure AdvComboBox25Change(Sender: TObject);
    procedure cxPageControl1DrawTabEx(AControl: TcxCustomTabControl;
      ATab: TcxTab; Font: TFont);
    procedure DrawPressureLossGraph(allDN: Boolean);
    procedure AdvComboBox1Change(Sender: TObject);
    procedure CheckBox10Click(Sender: TObject);
    procedure CalcPresFlowRate(p, t: Real);
    procedure AdvComboBox18Change(Sender: TObject);
    procedure PresFlowTableButtonClick(Sender: TObject);
    procedure CheckBox11Click(Sender: TObject);
    procedure AdvComboBox19KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure N4Click(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure DNFlowTableButtonClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
var TempUnits, PressureUnits, DensityUnits, VolumeFlowUnits, MassFlowUnits,
    DynViscUnits, KinViscUnits, PowerUnits: TStringList;
var UnitScaleIn, UnitScaleOut: Real;
const dirIn: ShortInt = 0;
const dirOut: ShortInt = 1;
var OldUnit: string;
var EV200_QmaxP, EV200_QminP: array [0..12] of Real;

implementation

{$R *.dfm}

procedure TForm1.FormShow(Sender: TObject);
var i: Integer;
begin
  unitScaleIn:=1;
  unitScaleOut:=1;
  TempUnits:=TStringList.Create;
  TempUnits.Add('C');
  TempUnits.Add('K');
  TempUnits.Add('F');

  PressureUnits:=TStringList.Create;
  PressureUnits.Add('���');
  PressureUnits.Add('���');
  PressureUnits.Add('��');
  PressureUnits.Add('���');
  PressureUnits.Add('���');
  PressureUnits.Add('PSI');
  PressureUnits.Add('���/��2');
  PressureUnits.Add('��.��.��');
  PressureUnits.Add('�.���.��');

  DensityUnits:=TStringList.Create;
  DensityUnits.Add('��/�3');
  DensityUnits.Add('�/��3');

  VolumeFlowUnits:=TStringList.Create;
  VolumeFlowUnits.Add('�3/�');
  VolumeFlowUnits.Add('�/�');
  VolumeFlowUnits.Add('�3/���');
  VolumeFlowUnits.Add('�/���');

  MassFlowUnits:=TStringList.Create;
  MassFlowUnits.Add('��/�');
  MassFlowUnits.Add('��/���');
  MassFlowUnits.Add('��/�');
  MassFlowUnits.Add('�/�');
  MassFlowUnits.Add('�/���');
  MassFlowUnits.Add('�/�');
  MassFlowUnits.Add('�/���');

  DynViscUnits:=TStringList.Create;
  DynViscUnits.Add('��');
  DynViscUnits.Add('��*�');
  DynViscUnits.Add('���*�/�2');
  DynViscUnits.Add('�*�/��2');
  DynViscUnits.Add('���*�');

  KinViscUnits:=TStringList.Create;
  KinViscUnits.Add('���');
  KinViscUnits.Add('��2/�');
  KinViscUnits.Add('�2/�');

  PowerUnits:=TStringList.Create;
  PowerUnits.Add('��');
  PowerUnits.Add('���');
  PowerUnits.Add('���/�');
  PowerUnits.Add('����/�');
  PowerUnits.Add('���*�/�');
  PowerUnits.Add('�.�.');
  PowerUnits.Add('��/�');
  PowerUnits.Add('���/�');
  PowerUnits.Add('���/�');
  PowerUnits.Add('����/�');

  ImportXLSArrayData;
  UnitAdvEditBtn5.Units.Clear;
  for i:=0 to Length(EV200_PipeSizes)-1 do UnitAdvEditBtn5.Units.Add(EV200_PipeSizes[i]);
  UnitAdvEditBtn6.Units.Clear;
  for i:=0 to Length(Steel_Mechprop)-1 do UnitAdvEditBtn6.Units.Add(Steel_Mechprop[i]);

  cxTabSheet13.TabVisible:=False;
  AdvComboBox2Change(Sender);
  AdvComboBox3Change(Sender);
  AdvComboBox15Change(Sender);
  AdvComboBox17Change(Sender);
  AdvComboBox19Change(Sender);
  cxPageControl1.ActivePage:=cxTabSheet2;
end;

procedure TForm1.UnitParamComboChange(Sender: TObject);
begin
  InputUnitCombo.Items.Clear;
  OutputUnitCombo.Items.Clear;
  case UnitParamCombo.ItemIndex of
    0: begin // �����������
      InputUnitCombo.Items.AddStrings(TempUnits);
      OutputUnitCombo.Items.AddStrings(TempUnits);
    end;
    1: begin // ��������
      InputUnitCombo.Items.AddStrings(PressureUnits);
      OutputUnitCombo.Items.AddStrings(PressureUnits);
    end;
    2: begin // ���������
      InputUnitCombo.Items.AddStrings(DensityUnits);
      OutputUnitCombo.Items.AddStrings(DensityUnits);
    end;
    3: begin // �������� ������
      InputUnitCombo.Items.AddStrings(VolumeFlowUnits);
      OutputUnitCombo.Items.AddStrings(VolumeFlowUnits);
    end;
    4: begin // �������� ������
      InputUnitCombo.Items.AddStrings(MassFlowUnits);
      OutputUnitCombo.Items.AddStrings(MassFlowUnits);
    end;
    5: begin // ������������ ��������
      InputUnitCombo.Items.AddStrings(DynViscUnits);
      OutputUnitCombo.Items.AddStrings(DynViscUnits);
    end;
    6: begin // �������������� ��������
      InputUnitCombo.Items.AddStrings(KinViscUnits);
      OutputUnitCombo.Items.AddStrings(KinViscUnits);
    end;
    7: begin // ��������
      InputUnitCombo.Items.AddStrings(PowerUnits);
      OutputUnitCombo.Items.AddStrings(PowerUnits);
    end;
  end;
  InputUnitCombo.ItemIndex:=0;
  OutputUnitCombo.ItemIndex:=0;
  InputUnitComboChange(Sender);
  OutputUnitComboChange(Sender);
  ConvertButtonClick(Sender);
end;

procedure TForm1.InputUnitComboChange(Sender: TObject);
begin
  UnitScaleIn:=SetESBConvertorsUnits(InputUnitCombo.Text, dirIn);
  ConvertButtonClick(Sender);
end;

procedure TForm1.OutputUnitComboChange(Sender: TObject);
begin
  UnitScaleOut:=SetESBConvertorsUnits(OutputUnitCombo.Text, dirOut);
  ConvertButtonClick(Sender);
end;

procedure TForm1.InputConvValueEditChange(Sender: TObject);
begin
  ConvertButtonClick(Sender);
end;

procedure TForm1.ConvertButtonClick(Sender: TObject);
begin
  case UnitParamCombo.ItemIndex of
    0: begin // �����������
      ESBTempConvertor1.ValueIn:=InputConvValueEdit.AsFloat;
      OutputConvValueEdit.AsFloat:=ESBTempConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
    end;
    1: begin // ��������
      ESBPressureConvertor1.ValueIn:=InputConvValueEdit.AsFloat;
      OutputConvValueEdit.AsFloat:=ESBPressureConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
    end;
    2: begin // ���������
      ESBDensityConvertor1.ValueIn:=InputConvValueEdit.AsFloat;
      OutputConvValueEdit.AsFloat:=ESBDensityConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
    end;
    3: begin // �������� ������
      ESBFlowConvertor1.ValueIn:=InputConvValueEdit.AsFloat;
      OutputConvValueEdit.AsFloat:=ESBFlowConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
    end;
    4: begin // �������� ������
      ESBFlowMassConvertor1.ValueIn:=InputConvValueEdit.AsFloat;
      OutputConvValueEdit.AsFloat:=ESBFlowMassConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
    end;
    5: begin // ������������ ��������
      ESBVisDynConvertor1.ValueIn:=InputConvValueEdit.AsFloat;
      OutputConvValueEdit.AsFloat:=ESBVisDynConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
    end;
    6: begin // �������������� ��������
      ESBVisKinConvertor1.ValueIn:=InputConvValueEdit.AsFloat;
      OutputConvValueEdit.AsFloat:=ESBVisKinConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
    end;
    7: begin // ��������
      ESBPowerConvertor1.ValueIn:=InputConvValueEdit.AsFloat;
      OutputConvValueEdit.AsFloat:=ESBPowerConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
    end;
  end;
end;

procedure TForm1.RadioGroup1Click(Sender: TObject);
var s: string;
begin
  if RadioGroup1.ItemIndex=0 then begin
    UnitAdvEditBtn4.LabelCaption:='������ � �.�. Q�';
    UnitAdvEditBtn4.Hint:='������ � ���������� ��������';
    UnitAdvEditBtn67.LabelCaption:='��������� � �.�.';
    UnitAdvEditBtn67.Hint:='��������� � ���������� ��������';
    FlowConvertNormalButton.Caption:='������� � �.�.';
    FlowConvertNormalButton.Hint:='������� � ���������� �������';
    s:='�';
  end else begin
    UnitAdvEditBtn4.LabelCaption:='������ � ��.�. Q��';
    UnitAdvEditBtn4.Hint:='������ � ����������� ��������';
    UnitAdvEditBtn67.LabelCaption:='��������� � ��.�.';
    UnitAdvEditBtn67.Hint:='��������� � ����������� ��������';
    FlowConvertNormalButton.Caption:='������� � ��.�.';
    FlowConvertNormalButton.Hint:='������� � ����������� �������';
    s:='��';
  end;
  MathImage30.Formula:='rho&'+s;
  MathImage34.Formula:='G=Q'+s+'*+rho&'+s;

  if MathImage1.Formula[2]='�' then begin // ������� � �.�.
    MathImage1.Formula:='Q�=Q'+s+'*+P�*+T/((P+P�)*+T�*+Kc)';
    MathImage32.Formula:='rho=(rho&'+s+')*+(P+P�)*+To*+Kc/(P�*+T)';
  end else begin // ������� � �.�.
    MathImage1.Formula:='Q'+s+'=Q�*+((P+P�)*+To/(P�*+T))*+Kc';
    MathImage32.Formula:='rho&'+s+'=rho*+P�*+T/((P+P�)*+T�*+Kc)';
  end;
end;

procedure TForm1.FlowConvertNormalButtonClick(Sender: TObject);
var p,p0,t,t0,q,pl,Kc: Real;
var s: string;
begin
  if RadioGroup1.ItemIndex=0 then s:='�' else s:='��';
  MathImage1.Formula:='Q'+s+'=Q�*+((P+P�)*+To/(P�*+T))*+Kc';
  MathImage32.Formula:='rho&'+s+'=rho*+P�*+T/((P+P�)*+T�*+Kc)';

  SetESBConvertorsUnits(UnitAdvEditBtn1.UnitID, dirIn);
  ESBPressureConvertor1.PressureUnitsOut:=eptBars;
  ESBPressureConvertor1.ValueIn:=UnitAdvEditBtn1.FloatValue;
  p:=ESBPressureConvertor1.ValueOut;

  SetESBConvertorsUnits(UnitAdvEditBtn53.UnitID, dirIn);
  ESBPressureConvertor1.PressureUnitsOut:=eptBars;
  ESBPressureConvertor1.ValueIn:=UnitAdvEditBtn53.FloatValue;
  p0:=ESBPressureConvertor1.ValueOut;

  if p<=-1 then begin
    ShowMessage('���������� �������� ������ ���� ������ -1 ���');
    Exit;
  end;

  SetESBConvertorsUnits(UnitAdvEditBtn2.UnitID, dirIn);
  ESBTempConvertor1.TempUnitsOut:=ettKelvin;
  ESBTempConvertor1.ValueIn:=UnitAdvEditBtn2.FloatValue;
  t:=ESBTempConvertor1.ValueOut;
  if RadioGroup1.ItemIndex=0 then t0:=273.15 else t0:=293.15;
  Kc:=UnitAdvEditBtn70.FloatValue;
  if Kc=0 then begin
    ShowMessage('����������� ����������� �� ����� ���� ����� 0');
    Exit;
  end;

  q:=UnitAdvEditBtn3.FloatValue;
  UnitAdvEditBtn4.FloatValue:=q*(p+p0)*t0/t/p0*Kc;
  pl:=UnitAdvEditBtn68.FloatValue;
  UnitAdvEditBtn67.FloatValue:=pl*p0*t/(p+p0)/t0/Kc;
  UnitAdvEditBtn69.FloatValue:=q*pl;
end;

procedure TForm1.FlowConvertActualButtonClick(Sender: TObject);
var p,p0,t,t0,q,pl,Kc: Real;
var s: string;
begin                            
  if RadioGroup1.ItemIndex=0 then s:='�' else s:='��';
  MathImage1.Formula:='Q�=Q'+s+'*+P�*+T/((P+P�)*+T�*+Kc)';
  MathImage32.Formula:='rho=(rho&'+s+')*+(P+P�)*+To*+Kc/(P�*+T)';

  SetESBConvertorsUnits(UnitAdvEditBtn1.UnitID, dirIn);
  ESBPressureConvertor1.PressureUnitsOut:=eptPascals;
  ESBPressureConvertor1.ValueIn:=UnitAdvEditBtn1.FloatValue;
  p:=ESBPressureConvertor1.ValueOut;

  SetESBConvertorsUnits(UnitAdvEditBtn53.UnitID, dirIn);
  ESBPressureConvertor1.PressureUnitsOut:=eptPascals;
  ESBPressureConvertor1.ValueIn:=UnitAdvEditBtn53.FloatValue;
  p0:=ESBPressureConvertor1.ValueOut;

  if p<=-1 then begin
    ShowMessage('���������� �������� ������ ���� ������ -1 ���');
    Exit;
  end;

  SetESBConvertorsUnits(UnitAdvEditBtn2.UnitID, dirIn);
  ESBTempConvertor1.TempUnitsOut:=ettKelvin;
  ESBTempConvertor1.ValueIn:=UnitAdvEditBtn2.FloatValue;
  t:=ESBTempConvertor1.ValueOut;
  if RadioGroup1.ItemIndex=0 then t0:=273.15 else t0:=293.15;
  Kc:=UnitAdvEditBtn70.FloatValue;
  if Kc=0 then begin
    ShowMessage('����������� ����������� �� ����� ���� ����� 0');
    Exit;
  end;

  q:=UnitAdvEditBtn4.FloatValue;
  UnitAdvEditBtn3.FloatValue:=q/t0*t*p0/(p+p0)/Kc;
  pl:=UnitAdvEditBtn67.FloatValue;
  UnitAdvEditBtn68.FloatValue:=pl*(p+p0)*t0/p0/t*Kc;
  UnitAdvEditBtn69.FloatValue:=q*pl;
end;

procedure TForm1.UnitAdvEditBtn3KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then FlowConvertNormalButtonClick(Sender);
end;

procedure TForm1.UnitAdvEditBtn4KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then FlowConvertActualButtonClick(Sender);
end;

procedure TForm1.PipePressureCalcButtonClick(Sender: TObject);
var pt,z,dn,p,s: Real;
begin
  pt:=UnitAdvEditBtn6.FloatValue;
  z:=AdvSpinEdit10.Value;
  dn:=UnitAdvEditBtn5.FloatValue;
  s:=UnitAdvEditBtn7.FloatValue;
  if (dn=0) or (z=0) then begin
    ShowMessage('������� ��������� ��������'); Exit;
  end;
  p:=2*s*pt/dn/z;
  UnitAdvEditBtn8.FloatValue:=p;
end;

procedure TForm1.PipeThicknessCalcButtonClick(Sender: TObject);
var pt,z,dn,p,s: Real;
begin
  pt:=UnitAdvEditBtn6.FloatValue;
  z:=AdvSpinEdit10.Value;
  dn:=UnitAdvEditBtn5.FloatValue;
  p:=UnitAdvEditBtn8.FloatValue;
  s:=p*dn*z/2/pt;
  UnitAdvEditBtn7.FloatValue:=s;
end;

procedure TForm1.UnitAdvEditBtn8KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then PipeThicknessCalcButtonClick(Sender);
end;

procedure TForm1.UnitAdvEditBtn6UnitChanged(Sender: TObject; NewUnit: String);
var s: string;
var n: Integer;
begin
  n:=Pos(' ', NewUnit);
  s:=Copy(NewUnit,0,n-1);
  UnitAdvEditBtn6.FloatValue:=StrToInt(s);
  UnitAdvEditBtn6.UnitID:='���';
end;

procedure TForm1.UnitAdvEditBtn5UnitChanged(Sender: TObject;
  NewUnit: String);
var s: string;
var n: Integer;
begin
  n:=Pos('�', NewUnit);
  s:=Copy(NewUnit,0,n-1);
  UnitAdvEditBtn5.FloatValue:=StrToFloat(s);
  UnitAdvEditBtn5.UnitID:='��';
  s:=Copy(NewUnit,n+1,5);
  n:=Pos(' ', s);
  s:=Copy(s,0,n-1);
  UnitAdvEditBtn7.FloatValue:=StrToFloat(s);
  PipePressureCalcButtonClick(Sender);
end;

procedure TForm1.AdvSpinEdit10Change(Sender: TObject);
begin
  PipePressureCalcButtonClick(Sender);
end;

procedure TForm1.UnitAdvEditBtn6KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then PipePressureCalcButtonClick(Sender);
end;

procedure TForm1.Dyn2KinViscButtonClick(Sender: TObject);
var pl,dv,kv: Real;
begin
  SetESBConvertorsUnits(UnitAdvEditBtn10.UnitID, dirIn);
  ESBDensityConvertor1.DensityUnitsOut:=edtGPerCuCm;
  ESBDensityConvertor1.ValueIn:=UnitAdvEditBtn10.FloatValue;
  pl:=ESBDensityConvertor1.ValueOut;

  SetESBConvertorsUnits(UnitAdvEditBtn12.UnitID, dirIn);
  ESBVisDynConvertor1.VisDynUnitsOut:=evdtCentipoise;
  ESBVisDynConvertor1.ValueIn:=UnitAdvEditBtn12.FloatValue;
  dv:=ESBVisDynConvertor1.ValueOut;

  if dv=0 then begin
    ShowMessage('������� ��������� �������� ��������'); Exit;
  end;
  kv:=dv/pl;

  ESBVisKinConvertor1.VisKinUnitsIn:=evktCentiStokes;
  SetESBConvertorsUnits(UnitAdvEditBtn13.UnitID, dirOut);
  ESBVisKinConvertor1.ValueIn:=kv;
  UnitAdvEditBtn13.FloatValue:=ESBVisKinConvertor1.ValueOut;
  ReynoldsQButtonClick(Sender);
end;

procedure TForm1.Kin2DynViscButtonClick(Sender: TObject);
var pl,dv,kv: Real;
begin
  SetESBConvertorsUnits(UnitAdvEditBtn10.UnitID, dirIn);
  ESBDensityConvertor1.DensityUnitsOut:=edtGPerCuCm;
  ESBDensityConvertor1.ValueIn:=UnitAdvEditBtn10.FloatValue;
  pl:=ESBDensityConvertor1.ValueOut;

  SetESBConvertorsUnits(UnitAdvEditBtn13.UnitID, dirIn);
  ESBVisKinConvertor1.VisKinUnitsOut:=evktCentiStokes;
  ESBVisKinConvertor1.ValueIn:=UnitAdvEditBtn13.FloatValue;
  kv:=ESBVisKinConvertor1.ValueOut;
  dv:=kv*pl;

  ESBVisDynConvertor1.VisDynUnitsIn:=evdtCentipoise;
  SetESBConvertorsUnits(UnitAdvEditBtn12.UnitID, dirOut);
  ESBVisDynConvertor1.ValueIn:=dv;
  UnitAdvEditBtn12.FloatValue:=ESBVisDynConvertor1.ValueOut;
  ReynoldsQButtonClick(Sender);
end;

procedure TForm1.QVReynoldsButtonClick(Sender: TObject);
var q, d, re, kv, s, v: Real;
begin
  if AdvComboBox23.Text='' then exit else d:=StrToFloat(AdvComboBox23.Text);
  if d=0 then begin
    ShowMessage('������� ��������� �������� ��������');
    Exit;
  end;
  re:=UnitAdvEditBtn20.FloatValue;
  SetESBConvertorsUnits(UnitAdvEditBtn13.UnitID, dirIn);
  ESBVisKinConvertor1.VisKinUnitsOut:=evktSqMPerSec;
  ESBVisKinConvertor1.ValueIn:=UnitAdvEditBtn13.FloatValue;
  kv:=ESBVisKinConvertor1.ValueOut;
  s:=3.1416*d*d/4;
  q:=re*kv*s*3.6/d;
  ESBFlowConvertor1.FlowUnitsIn:=eftCubicMetresPerHour;
  SetESBConvertorsUnits(UnitAdvEditBtn18.UnitID, dirOut);
  ESBFlowConvertor1.ValueIn:=q;
  UnitAdvEditBtn18.FloatValue:=ESBFlowConvertor1.ValueOut;
  v:=q*4/3.1416/d/d/0.0036;
  UnitAdvEditBtn21.FloatValue:=v;
end;

procedure TForm1.UnitAdvEditBtn20KeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key)=13 then QVReynoldsButtonClick(Sender);
end;

procedure TForm1.AdvComboBox22Change(Sender: TObject);
var i: ShortInt;
begin
  i:=AdvComboBox22.ItemIndex;
  SetESBConvertorsUnits(UnitAdvEditBtn10.UnitID, dirOut);
  ESBDensityConvertor1.DensityUnitsIn:=edtKgPerCuM;
  ESBDensityConvertor1.ValueIn:=MediumDensVisc[i,0];
  UnitAdvEditBtn10.FloatValue:=ESBDensityConvertor1.ValueOut;

  SetESBConvertorsUnits(UnitAdvEditBtn12.UnitID, dirOut);
  ESBVisDynConvertor1.VisDynUnitsIn:=evdtPascalSecs;
  ESBVisDynConvertor1.ValueIn:=MediumDensVisc[i,1]/1000;
  UnitAdvEditBtn12.FloatValue:=ESBVisDynConvertor1.ValueOut;

  SetESBConvertorsUnits(UnitAdvEditBtn13.UnitID, dirOut);
  ESBVisKinConvertor1.VisKinUnitsIn:=evktCentiStokes; //
  ESBVisKinConvertor1.ValueIn:=MediumDensVisc[i,1]/MediumDensVisc[i,0]*1000;
  UnitAdvEditBtn13.FloatValue:=ESBVisKinConvertor1.ValueOut;
end;

procedure TForm1.UnitAdvEditBtn12KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then Dyn2KinViscButtonClick(Sender);
end;

procedure TForm1.UnitAdvEditBtn13KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then Kin2DynViscButtonClick(Sender);
end;

procedure TForm1.ReynoldsQButtonClick(Sender: TObject);
var d,q,kv,s,v,re: Real;
begin
  if AdvComboBox23.Text='' then exit else d:=StrToFloat(AdvComboBox23.Text);
  if d=0 then begin
    ShowMessage('������� ��������� �������� ��������');
    Exit;
  end;

  SetESBConvertorsUnits(UnitAdvEditBtn18.UnitID, dirIn);
  ESBFlowConvertor1.FlowUnitsOut:=eftCubicMetresPerHour;
  ESBFlowConvertor1.ValueIn:=UnitAdvEditBtn18.FloatValue;
  q:=ESBFlowConvertor1.ValueOut;

  SetESBConvertorsUnits(UnitAdvEditBtn13.UnitID, dirIn);
  ESBVisKinConvertor1.VisKinUnitsOut:=evktSqMPerSec;
  ESBVisKinConvertor1.ValueIn:=UnitAdvEditBtn13.FloatValue;
  kv:=ESBVisKinConvertor1.ValueOut;
  if kv=0 then begin
    ShowMessage('������� ��������� �������� ��������'); Exit;
  end;
  v:=q*4/3.1416/d/d/0.0036;
  UnitAdvEditBtn21.FloatValue:=v;
  s:=3.1416*d*d/4;
  re:=q*d/kv/s/3.6;
  UnitAdvEditBtn20.FloatValue:=re;
end;

procedure TForm1.UnitAdvEditBtn18KeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key)=13 then ReynoldsQButtonClick(Sender);
end;

procedure TForm1.ReynoldsVButtonClick(Sender: TObject);
var v,d,s,q: Real;
begin
  v:=UnitAdvEditBtn21.FloatValue;
  if AdvComboBox23.Text='' then d:=0 else d:=StrToFloat(AdvComboBox23.Text);
  s:=3.1416*d*d/4;
  q:=v*s*0.0036;
  ESBFlowConvertor1.FlowUnitsIn:=eftCubicMetresPerHour;
  SetESBConvertorsUnits(UnitAdvEditBtn18.UnitID, dirOut);
  ESBFlowConvertor1.ValueIn:=q;
  UnitAdvEditBtn18.FloatValue:=ESBFlowConvertor1.ValueOut;
  ReynoldsQButtonClick(Sender);
  UnitAdvEditBtn21.FloatValue:=v;
end;

procedure TForm1.AdvComboBox23Change(Sender: TObject);
var medium, idn: ShortInt;
var Qmin: Real;
begin
  if CheckBox6.Checked=False then Exit;
  if AdvComboBox22.ItemIndex=0 then medium:=0 else medium:=1;
  idn:=AdvComboBox23.ItemIndex;
  Qmin:=EV200_Qmin[0,medium,idn];

  SetESBConvertorsUnits(UnitAdvEditBtn18.UnitID, dirOut);
  ESBFlowConvertor1.FlowUnitsIn:=eftCubicMetresPerHour;
  ESBFlowConvertor1.ValueIn:=Qmin;
  UnitAdvEditBtn18.FloatValue:=ESBFlowConvertor1.ValueOut;
  ReynoldsQButtonClick(Sender);
end;

procedure TForm1.UnitAdvEditBtn21KeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key)=13 then ReynoldsVButtonClick(Sender);
end;

procedure TForm1.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var p: ShortInt;
begin
  p:=0;
  if ord(Key)=112 then begin //F1 Help
    ShowMessage('F2 - �����������'+chr(13)+'F5 - ��������� ��������'+chr(13)
                +'F6 - ��������� ��������'+chr(13)+chr(13)+'��������� ������ �2013-2015');
  end;
  if ord(Key)=113 then begin //F2 �����������
    if not( (ActiveControl is TESBFloatEdit) or (ActiveControl is TUnitAdvEditBtn)
    or (ActiveControl is TAdvSpinEdit) ) then exit;
    if ESBCalculator1.Execute then begin
      if ActiveControl is TESBFloatEdit then (ActiveControl as TESBFloatEdit).AsFloat:=ESBCalculator1.Value
      else if ActiveControl is TUnitAdvEditBtn then (ActiveControl as TUnitAdvEditBtn).FloatValue:=ESBCalculator1.Value
      else if ActiveControl is TAdvSpinEdit then (ActiveControl as TAdvSpinEdit).FloatValue:=ESBCalculator1.Value;
    end;
  end;
  if ord(Key)=114 then begin //F3 �������� � �������
    case cxPageControl1.ActivePageIndex of
      1: Page1Add2TableButtonClick(Sender); // ������ ��<->��
      2: Page2Add2TableButtonClick(Sender); // ����� - ��������
      4: Page4Add2TableButtonClick(Sender); // ������ <-> ��������
      5: Page5Add2TableButtonClick(Sender); // ������ ��������
      6: Page6Add2TableButtonClick(Sender); // ������ ��-205
      7: Page7Add2TableButtonClick(Sender); // �������� ����
      8: Page8Add2TableButtonClick(Sender); // ��. ������ ����
      9: Page9Add2TableButtonClick(Sender); // ������� <-> ������
     11: Page11Add2TableButtonClick(Sender); // ����. �������� ���/������
    end;
  end;
  if (ord(Key)=115) and (cxPageControl1.ActivePageIndex=8) then begin //F4 ������ ����������� �� ������� ����
    FlowDNCalcButtonClick(Sender);
  end;

  if ord(Key)=116 then begin //F5 ��������� ��������
    if not( (ActiveControl is TESBFloatEdit) or (ActiveControl is TUnitAdvEditBtn)
    or (ActiveControl is TAdvSpinEdit) ) then exit;
    if ActiveControl is TESBFloatEdit then begin
      p:=(ActiveControl as TESBFloatEdit).DecimalPlaces-1;
      if p<0 then p:=0;
      (ActiveControl as TESBFloatEdit).DecimalPlaces:=p;
    end else if ActiveControl is TUnitAdvEditBtn then begin
      p:=(ActiveControl as TUnitAdvEditBtn).Precision-1;
      if p<0 then p:=0;
      (ActiveControl as TUnitAdvEditBtn).Precision:=p;
    end else if ActiveControl is TAdvSpinEdit then begin
      p:=(ActiveControl as TAdvSpinEdit).Precision-1;
      if p<0 then p:=0;
      (ActiveControl as TAdvSpinEdit).Precision:=p;
    end;
    dxStatusBar1.Panels[0].Text:='�������� 0.'+ StringOfChar('0',p);
  end;
  if ord(Key)=117 then begin //F6 ��������� ��������
    if not( (ActiveControl is TESBFloatEdit) or (ActiveControl is TUnitAdvEditBtn)
    or (ActiveControl is TAdvSpinEdit) ) then exit;
    if ActiveControl is TESBFloatEdit then begin
      p:=(ActiveControl as TESBFloatEdit).DecimalPlaces+1;
      if p>10 then p:=10;
      (ActiveControl as TESBFloatEdit).DecimalPlaces:=p;
    end else if ActiveControl is TUnitAdvEditBtn then begin
      p:=(ActiveControl as TUnitAdvEditBtn).Precision+1;
      if p>10 then p:=10;
      (ActiveControl as TUnitAdvEditBtn).Precision:=p;
    end else if ActiveControl is TAdvSpinEdit then begin
      p:=(ActiveControl as TAdvSpinEdit).Precision+1;
      if p>10 then p:=10;
      (ActiveControl as TAdvSpinEdit).Precision:=p;
    end;
    dxStatusBar1.Panels[0].Text:='�������� 0.'+ StringOfChar('0',p);
  end;

end;

procedure TForm1.Flow2VelConvButtonClick(Sender: TObject);
var d,qv,qm,pl,v,s: Real;
var unitScale: Real;
begin
  d:=UnitAdvEditBtn9.FloatValue;
  if d=0 then begin
    ShowMessage('������� ��������� �������� ��������'); Exit;
  end;
  s:=3.1416*d*d/4;
  if UnitAdvEditBtn11.UnitID='�2' then s:=s/1000000;
  UnitAdvEditBtn11.FloatValue:=s;

  SetESBConvertorsUnits(UnitAdvEditBtn14.UnitID, dirIn);
  ESBFlowConvertor1.FlowUnitsOut:=eftCubicMetresPerHour;
  ESBFlowConvertor1.ValueIn:=UnitAdvEditBtn14.FloatValue;
  qv:=ESBFlowConvertor1.ValueOut;

  v:=qv*4/3.1416/d/d/0.0036;
  UnitAdvEditBtn15.FloatValue:=v;

// Mass flow Calc
  SetESBConvertorsUnits(UnitAdvEditBtn16.UnitID, dirIn);
  ESBDensityConvertor1.DensityUnitsOut:=edtKgPerCuM;
  ESBDensityConvertor1.ValueIn:=UnitAdvEditBtn16.FloatValue;
  pl:=ESBDensityConvertor1.ValueOut;
  qm:=qv*pl;

  ESBFlowMassConvertor1.FlowUnitsIn:=eftKilogramsPerHour;
  unitScale:=SetESBConvertorsUnits(UnitAdvEditBtn17.UnitID, dirOut);
  ESBFlowMassConvertor1.ValueIn:=qm;
  UnitAdvEditBtn17.FloatValue:=ESBFlowMassConvertor1.ValueOut*unitScale;
end;

procedure TForm1.Vel2FlowConvButtonClick(Sender: TObject);
var d,qv, qm,v,s, pl: Real;
var unitScale: Real;
begin
  d:=UnitAdvEditBtn9.FloatValue;
  s:=3.1416*d*d/4;
  if UnitAdvEditBtn11.UnitID='�2' then s:=s/1000000;
  UnitAdvEditBtn11.FloatValue:=s;

  SetESBConvertorsUnits(UnitAdvEditBtn16.UnitID, dirIn);
  ESBDensityConvertor1.DensityUnitsOut:=edtKgPerCuM;
  ESBDensityConvertor1.ValueIn:=UnitAdvEditBtn16.FloatValue;
  pl:=ESBDensityConvertor1.ValueOut;

  v:=UnitAdvEditBtn15.FloatValue;
  qv:=v*3.1416*d*d/4*0.0036;
  qm:=qv*pl;

  ESBFlowConvertor1.FlowUnitsIn:=eftCubicMetresPerHour;
  SetESBConvertorsUnits(UnitAdvEditBtn14.UnitID, dirOut);
  ESBFlowConvertor1.ValueIn:=qv;
  UnitAdvEditBtn14.FloatValue:=ESBFlowConvertor1.ValueOut;

  ESBFlowMassConvertor1.FlowUnitsIn:=eftKilogramsPerHour;
  unitScale:=SetESBConvertorsUnits(UnitAdvEditBtn17.UnitID, dirOut);
  ESBFlowMassConvertor1.ValueIn:=qm;
  UnitAdvEditBtn17.FloatValue:=ESBFlowMassConvertor1.ValueOut*unitScale;
end;

procedure TForm1.UnitAdvEditBtn14KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then Flow2VelConvButtonClick(Sender);
end;

procedure TForm1.UnitAdvEditBtn15KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then Vel2FlowConvButtonClick(Sender);
end;

procedure TForm1.UnitAdvEditBtn17KeyPress(Sender: TObject; var Key: Char);
var qv,qm,qm0,pl: Real;
var unitScale: Real;
begin
  if ord(Key)<>13 then exit;

  qm0:=UnitAdvEditBtn17.FloatValue;
  unitScale:=SetESBConvertorsUnits(UnitAdvEditBtn17.UnitID, dirIn);
  ESBFlowMassConvertor1.FlowUnitsOut:=eftKilogramsPerHour;
  ESBFlowMassConvertor1.ValueIn:=UnitAdvEditBtn17.FloatValue;
  qm:=ESBFlowMassConvertor1.ValueOut*unitScale;

  SetESBConvertorsUnits(UnitAdvEditBtn16.UnitID, dirIn);
  ESBDensityConvertor1.DensityUnitsOut:=edtKgPerCuM;
  ESBDensityConvertor1.ValueIn:=UnitAdvEditBtn16.FloatValue;
  pl:=ESBDensityConvertor1.ValueOut;
  if pl=0 then begin
    ShowMessage('������� ��������� �������� ���������'); Exit;
  end;

  qv:=qm/pl;
  ESBFlowConvertor1.FlowUnitsIn:=eftCubicMetresPerHour;
  SetESBConvertorsUnits(UnitAdvEditBtn14.UnitID, dirOut);
  ESBFlowConvertor1.ValueIn:=qv;
  UnitAdvEditBtn14.FloatValue:=ESBFlowConvertor1.ValueOut;

  Flow2VelConvButtonClick(Sender);
  UnitAdvEditBtn17.FloatValue:=qm0;
end;

procedure TForm1.Page1Add2TableButtonClick(Sender: TObject);
var n: ShortInt;
begin
  cxGrid1TableView1.BeginUpdate;
  n:=cxGrid1TableView1.DataController.AppendRecord;
  cxGrid1TableView1.DataController.SetValue(n,cxGrid1TableView1.FindItemByTag(0).index,cxGrid1TableView1.DataController.RecordCount);
  cxGrid1TableView1.DataController.SetValue(n,cxGrid1TableView1.FindItemByTag(1).index,UnitAdvEditBtn1.Text);
  cxGrid1TableView1.DataController.SetValue(n,cxGrid1TableView1.FindItemByTag(2).index,UnitAdvEditBtn2.Text);
  cxGrid1TableView1.DataController.SetValue(n,cxGrid1TableView1.FindItemByTag(3).index,UnitAdvEditBtn3.Text);
  cxGrid1TableView1.DataController.SetValue(n,cxGrid1TableView1.FindItemByTag(4).index,UnitAdvEditBtn4.Text);
  cxGrid1TableView1.EndUpdate;
end;

procedure TForm1.Page2Add2TableButtonClick(Sender: TObject);
var n: ShortInt;
begin
  cxGrid2TableView1.BeginUpdate;
  n:=cxGrid2TableView1.DataController.AppendRecord;
  cxGrid2TableView1.DataController.SetValue(n,cxGrid2TableView1.FindItemByTag(0).index,cxGrid2TableView1.DataController.RecordCount);
  cxGrid2TableView1.DataController.SetValue(n,cxGrid2TableView1.FindItemByTag(1).index,UnitAdvEditBtn6.Text);
  cxGrid2TableView1.DataController.SetValue(n,cxGrid2TableView1.FindItemByTag(2).index,AdvSpinEdit10.Text);
  cxGrid2TableView1.DataController.SetValue(n,cxGrid2TableView1.FindItemByTag(3).index,UnitAdvEditBtn5.Text);
  cxGrid2TableView1.DataController.SetValue(n,cxGrid2TableView1.FindItemByTag(4).index,UnitAdvEditBtn7.Text);
  cxGrid2TableView1.DataController.SetValue(n,cxGrid2TableView1.FindItemByTag(5).index,UnitAdvEditBtn8.Text);
  cxGrid2TableView1.EndUpdate;
end;

procedure TForm1.AdvComboBox2Change(Sender: TObject);
var i: ShortInt;
begin
  AdvComboBox1.Items.Clear;
  case AdvComboBox2.ItemIndex of
    0: for i:=0 to Length(EV1200_DN)-1 do AdvComboBox1.Items.Add(IntToStr(EV1200_DN[i])); // ����������������
    1: for i:=0 to Length(EV200_DN)-1 do AdvComboBox1.Items.Add(IntToStr(EV200_DN[i]));  // ��200 �������
    2: for i:=0 to Length(EV200_DN)-1 do AdvComboBox1.Items.Add(IntToStr(EV200_DN[i]));  // ��200 ���������
    3: for i:=0 to Length(EV200_FRDN)-1 do AdvComboBox1.Items.Add(IntToStr(EV200_FRDN[i])); // ��200 ��
    4: for i:=0 to Length(EV200PPD_DN)-1 do AdvComboBox1.Items.Add(EV200PPD_DN[i]);         // ��200 ���
    5: for i:=0 to Length(EV205_DN)-1 do AdvComboBox1.Items.Add(IntToStr(EV205_DN[i]));  // ��205 ��
  end;
  if AdvComboBox2.ItemIndex=0 then MathImage12.Formula:='Delta&P=3*+0.5*+rho*+V^2'
  else MathImage12.Formula:='Delta&P=A*+rho*+Q^2/(D^4)';
  AdvComboBox1.ItemIndex:=0;
  DrawPressureLossGraph(True);  // ������ ������ ���� ��
end;

procedure TForm1.Page5Add2TableButtonClick(Sender: TObject);
var n: ShortInt;
begin
  cxGrid3TableView1.BeginUpdate;
  n:=cxGrid3TableView1.DataController.AppendRecord;
  cxGrid3TableView1.DataController.SetValue(n,cxGrid3TableView1.FindItemByTag(0).index,cxGrid3TableView1.DataController.RecordCount);
  cxGrid3TableView1.DataController.SetValue(n,cxGrid3TableView1.FindItemByTag(1).index,AdvComboBox2.Text);
  cxGrid3TableView1.DataController.SetValue(n,cxGrid3TableView1.FindItemByTag(2).index,AdvComboBox1.Text);
  cxGrid3TableView1.DataController.SetValue(n,cxGrid3TableView1.FindItemByTag(3).index,UnitAdvEditBtn25.Text);
  cxGrid3TableView1.DataController.SetValue(n,cxGrid3TableView1.FindItemByTag(4).index,UnitAdvEditBtn22.Text);
  cxGrid3TableView1.DataController.SetValue(n,cxGrid3TableView1.FindItemByTag(5).index,UnitAdvEditBtn27.Text);
  cxGrid3TableView1.EndUpdate;
end;

procedure TForm1.PressureDropButtonClick(Sender: TObject);
var pl,d,q,v,dp, A: Real;
begin
  SetESBConvertorsUnits(UnitAdvEditBtn22.UnitID, dirIn);
  ESBDensityConvertor1.DensityUnitsOut:=edtKgPerCuM;
  ESBDensityConvertor1.ValueIn:=UnitAdvEditBtn22.FloatValue;
  pl:=ESBDensityConvertor1.ValueOut;

  SetESBConvertorsUnits(UnitAdvEditBtn25.UnitID, dirIn);
  ESBFlowConvertor1.FlowUnitsOut:=eftCubicMetresPerHour;
  ESBFlowConvertor1.ValueIn:=UnitAdvEditBtn25.FloatValue;
  q:=ESBFlowConvertor1.ValueOut;

  
  if AdvComboBox2.ItemIndex=4 then d:=EV200PPD_InnerD[AdvComboBox1.ItemIndex] // ���
  else d:=StrToFloat(AdvComboBox1.Text);
  if AdvComboBox2.ItemIndex=3 then d:=EV200_FR_InnerD[AdvComboBox1.ItemIndex]; // ��
//  if AdvComboBox2.ItemIndex<>4 then d:=StrToFloat(AdvComboBox1.Text)
//  else d:=EV200PPD_OuterD[AdvComboBox1.ItemIndex]; // ���

  if d=0 then begin
    ShowMessage('������� ��������� �������� ��������'); Exit;
  end;

  v:=q*4/3.1416/d/d/0.0036;
  UnitAdvEditBtn24.FloatValue:=v;

  A:=0;
  case AdvComboBox2.ItemIndex of
    1: A:=EV200_APLoss_WF[AdvComboBox1.ItemIndex]; // �
    2: A:=EV200_APLoss_WF[AdvComboBox1.ItemIndex]; // �
    3: A:=EV200_APLoss_FR[AdvComboBox1.ItemIndex]; // ��
    4: A:=EV200PPD_APLoss[AdvComboBox1.ItemIndex];// ���
    5: A:=EV205_APLoss; // ��-205
  end;

  if AdvComboBox2.ItemIndex=0 then dp:=3*0.5*pl*v*v/1000 // ��1200
  else dp:=A*pl*q*q/(d*d*d*d); //��200-205

  ESBPressureConvertor1.PressureUnitsIn:=eptKilopascals;
  SetESBConvertorsUnits(UnitAdvEditBtn27.UnitID, dirOut);
  ESBPressureConvertor1.ValueIn:=dp;
  UnitAdvEditBtn27.FloatValue:=ESBPressureConvertor1.ValueOut;
end;

procedure TForm1.UnitAdvEditBtn25KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then PressureDropButtonClick(Sender);
end;

procedure TForm1.AdvComboBox4Change(Sender: TObject);
begin
  if AdvComboBox4.ItemIndex=0 then begin // ����� �����
    UnitAdvEditBtn23.FloatValue:=0.84;
    UnitAdvEditBtn30.FloatValue:=EV205_Kp[0];
  end else begin // ����� ������� ���������
    UnitAdvEditBtn23.FloatValue:=1;
    UnitAdvEditBtn30.FloatValue:=EV205_Kp[1];
  end;
end;

procedure TForm1.UnitAdvEditBtn32KeyPress(Sender: TObject; var Key: Char);
var d: Real;
var k: ShortInt;
begin
  if ord(Key)<>13 then Exit;
  d:=UnitAdvEditBtn32.FloatValue;
  if d<230 then k:=0
  else if d<280 then k:=1
  else if d<330 then k:=2
  else if d<380 then k:=3
  else if d<430 then k:=4
  else if d<480 then k:=5
  else if d<530 then k:=6
  else if d<650 then k:=7
  else if d<750 then k:=8
  else if d<850 then k:=9
  else if d<950 then k:=10
  else if d<1050 then k:=11
  else if d<1150 then k:=12
  else if d<1250 then k:=13
  else if d<1350 then k:=14
  else if d<1450 then k:=15
  else if d<1550 then k:=16
  else if d<1650 then k:=17
  else if d<1850 then k:=18
  else k:=19;
  UnitAdvEditBtn31.FloatValue:=EV205_K3[k];
  BigPipeFlowButtonClick(Sender);
end;

procedure TForm1.BigPipeFlowButtonClick(Sender: TObject);
var D, Kv, Kp, K3, S, q, Qf: Real;
begin
  Kv:=UnitAdvEditBtn23.FloatValue;
  Kp:=UnitAdvEditBtn30.FloatValue;
  K3:=UnitAdvEditBtn31.FloatValue;
  D:=UnitAdvEditBtn32.FloatValue;

  S:=D*D/40/40*Kv*Kp*K3;
  UnitAdvEditBtn29.FloatValue:=S;

  q:=UnitAdvEditBtn26.FloatValue;
  Qf:=q*S;
  UnitAdvEditBtn28.FloatValue:=Qf;
  UnitAdvEditBtn28.UnitID:=UnitAdvEditBtn26.UnitID;
end;


procedure TForm1.UnitAdvEditBtn26KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then BigPipeFlowButtonClick(Sender);
end;

procedure TForm1.Page6Add2TableButtonClick(Sender: TObject);
var n: ShortInt;
begin
  cxGrid4TableView1.BeginUpdate;
  n:=cxGrid4TableView1.DataController.AppendRecord;
  cxGrid4TableView1.DataController.SetValue(n,cxGrid4TableView1.FindItemByTag(0).index,cxGrid4TableView1.DataController.RecordCount);
  cxGrid4TableView1.DataController.SetValue(n,cxGrid4TableView1.FindItemByTag(1).index,AdvComboBox4.Text);
  cxGrid4TableView1.DataController.SetValue(n,cxGrid4TableView1.FindItemByTag(2).index,UnitAdvEditBtn23.Text);
  cxGrid4TableView1.DataController.SetValue(n,cxGrid4TableView1.FindItemByTag(3).index,UnitAdvEditBtn30.Text);
  cxGrid4TableView1.DataController.SetValue(n,cxGrid4TableView1.FindItemByTag(4).index,UnitAdvEditBtn31.Text);
  cxGrid4TableView1.DataController.SetValue(n,cxGrid4TableView1.FindItemByTag(5).index,UnitAdvEditBtn32.Text);
  cxGrid4TableView1.DataController.SetValue(n,cxGrid4TableView1.FindItemByTag(6).index,UnitAdvEditBtn26.Text);
  cxGrid4TableView1.DataController.SetValue(n,cxGrid4TableView1.FindItemByTag(7).index,UnitAdvEditBtn29.Text);
  cxGrid4TableView1.DataController.SetValue(n,cxGrid4TableView1.FindItemByTag(8).index,UnitAdvEditBtn28.Text);
  cxGrid4TableView1.EndUpdate;
end;

procedure TForm1.CheckBox1Click(Sender: TObject);
begin
  UnitAdvEditBtn39.EditorEnabled:=CheckBox1.Checked;
  if CheckBox1.Checked then UnitAdvEditBtn39.Color:=clWindow
  else UnitAdvEditBtn39.Color:=clSilver;
  if (CheckBox1.Checked=False) and (CheckBox2.Checked=False) then CheckBox1.Checked:=True;
  if (CheckBox1.Checked=False) or (CheckBox2.Checked=False) then AdvComboBox12.ItemIndex:=1  // ���������� ���
  else AdvComboBox12.ItemIndex:=-1;
  WaterSteamButtonClick(Sender);
end;

procedure TForm1.CheckBox2Click(Sender: TObject);
begin
  UnitAdvEditBtn40.EditorEnabled:=CheckBox2.Checked;
  if CheckBox2.Checked then UnitAdvEditBtn40.Color:=clWindow
  else UnitAdvEditBtn40.Color:=clSilver;
  if (CheckBox1.Checked=False) and (CheckBox2.Checked=False) then CheckBox2.Checked:=True;
  if (CheckBox1.Checked=False) or (CheckBox2.Checked=False) then AdvComboBox12.ItemIndex:=1  // ���������� ���
  else AdvComboBox12.ItemIndex:=-1;
  WaterSteamButtonClick(Sender);
end;

procedure TForm1.CheckBox3Click(Sender: TObject);
begin
  UnitAdvEditBtn33.Enabled:=CheckBox3.Checked;
  WaterSteamButtonClick(Sender);
end;

procedure TForm1.WaterSteamButtonClick(Sender: TObject);
var p, t, x, r, dv, kv, state, ent: Real;
var unitScale, G, Qm, Qv: Real;
begin
  SetESBConvertorsUnits(UnitAdvEditBtn39.UnitID, dirIn);
  ESBPressureConvertor1.PressureUnitsOut:=eptPascals;
  ESBPressureConvertor1.ValueIn:=UnitAdvEditBtn39.FloatValue;
  p:=ESBPressureConvertor1.ValueOut;
  if RadioButton1.Checked then p:=p+101325; // ���������� �������� -> ����������
  if p<=0 then begin
    ShowMessage('���������� �������� ������ ���� ������ -1 ���');
    Exit;
  end;

  SetESBConvertorsUnits(UnitAdvEditBtn40.UnitID, dirIn);
  ESBTempConvertor1.TempUnitsOut:=ettKelvin;
  ESBTempConvertor1.ValueIn:=UnitAdvEditBtn40.FloatValue;
  t:=ESBTempConvertor1.ValueOut;

  x:=UnitAdvEditBtn33.FloatValue;
  r:=0; dv:=0; kv:=0; ent:=0;

  if (CheckBox1.Checked=True) and (CheckBox2.Checked=True) then begin //�� �������� � �����������
    if CheckBox3.Checked then begin //������� �������
      r:=wspDPTX(p,t,x);
      dv:=wspDYNVISPTX(p, t, x);
      kv:=wspKINVISPTX(p, t, x);
      ent:=wspHPTX(p, t, x);
    end else begin
      r:=wspDPT(p,t);
      dv:=wspDYNVISPT(p, t);
      kv:=wspKINVISPT(p, t);
      ent:=wspHPT(p, t);
    end;

    state:=wspPHASESTATEPT(p, t);
    AdvComboBox12.ItemIndex:=Trunc(state)+1;
  end else if CheckBox1.Checked=False then begin //�� �����������
    if CheckBox3.Checked then begin //������� �������
      p:=wspPST(t);
      r:=wspDSTX(t, x);
      dv:=wspDYNVISSST(t);
      kv:=wspKINVISSST(t);
      ent:=wspHPTX(p, t, x);
    end else begin
      p:=wspPST(t);
      r:=wspDSST(t);
      dv:=wspDYNVISSST(t);
      kv:=wspKINVISSST(t);
      ent:=wspHPT(p, t);
    end;
    SetESBConvertorsUnits(UnitAdvEditBtn39.UnitID, dirOut);
    ESBPressureConvertor1.PressureUnitsIn:=eptPascals;
    if RadioButton1.Checked then p:=p-101325; // ���������� �������� -> ����������
    ESBPressureConvertor1.ValueIn:=p;
    UnitAdvEditBtn39.FloatValue:=ESBPressureConvertor1.ValueOut;
  end else if CheckBox2.Checked=False then begin //�� ��������
    if CheckBox3.Checked then begin //������� �������
      t:=wspTSP(p);
      r:=wspDSTX(t, x);
      dv:=wspDYNVISSST(t);
      kv:=wspKINVISSST(t);
      ent:=wspHPTX(p, t, x);
    end else begin
      t:=wspTSP(p);
      r:=wspDSST(t);
      dv:=wspDYNVISSST(t);
      kv:=wspKINVISSST(t);
      ent:=wspHPT(p, t);
    end;
    if t<0 then t:=0;
    SetESBConvertorsUnits(UnitAdvEditBtn40.UnitID, dirOut);
    ESBTempConvertor1.TempUnitsIn:=ettKelvin;
    ESBTempConvertor1.ValueIn:=t;
    UnitAdvEditBtn40.FloatValue:=ESBTempConvertor1.ValueOut;
  end;

  UnitAdvEditBtn41.FloatValue:=r;

  ESBPowerConvertor1.PowerUnitsIn:=eptJoulesPerHour;
  unitScale:=SetESBConvertorsUnits(UnitAdvEditBtn56.UnitID, dirOut);
  ESBPowerConvertor1.ValueIn:=ent;
  UnitAdvEditBtn56.FloatValue:=ESBPowerConvertor1.ValueOut*unitScale;

  ESBVisDynConvertor1.VisDynUnitsIn:=evdtPascalSecs;
  SetESBConvertorsUnits(UnitAdvEditBtn42.UnitID, dirOut);
  ESBVisDynConvertor1.ValueIn:=dv;
  UnitAdvEditBtn42.FloatValue:=ESBVisDynConvertor1.ValueOut;

  ESBVisKinConvertor1.VisKinUnitsIn:=evktSqMPerSec;
  SetESBConvertorsUnits(UnitAdvEditBtn43.UnitID, dirOut);
  ESBVisKinConvertor1.ValueIn:=kv;
  UnitAdvEditBtn43.FloatValue:=ESBVisKinConvertor1.ValueOut;

  unitScale:=SetESBConvertorsUnits(UnitAdvEditBtn59.UnitID, dirIn);
  ESBPowerConvertor1.PowerUnitsOut:=eptJoulesPerHour;
  ESBPowerConvertor1.ValueIn:=UnitAdvEditBtn59.FloatValue;
  G:=ESBPowerConvertor1.ValueOut*unitScale;
  if ent=0 then Qm:=0 else Qm:=G/ent;

  ESBFlowMassConvertor1.FlowUnitsIn:=eftKilogramsPerHour;
  unitScale:=SetESBConvertorsUnits(UnitAdvEditBtn58.UnitID, dirOut);
  ESBFlowMassConvertor1.ValueIn:=Qm;
  UnitAdvEditBtn58.FloatValue:=ESBFlowMassConvertor1.ValueOut*unitScale;

  Qv:=Qm/r;
  UnitAdvEditBtn60.FloatValue:=Qv;
end;

procedure TForm1.UnitAdvEditBtn39KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then WaterSteamButtonClick(Sender);
end;

procedure TForm1.Page7Add2TableButtonClick(Sender: TObject);
var n: ShortInt;
begin
  cxGrid5TableView1.BeginUpdate;
  n:=cxGrid5TableView1.DataController.AppendRecord;
  cxGrid5TableView1.DataController.SetValue(n,cxGrid5TableView1.FindItemByTag(0).index,cxGrid5TableView1.DataController.RecordCount);
  cxGrid5TableView1.DataController.SetValue(n,cxGrid5TableView1.FindItemByTag(1).index,UnitAdvEditBtn39.Text);
  cxGrid5TableView1.DataController.SetValue(n,cxGrid5TableView1.FindItemByTag(2).index,UnitAdvEditBtn40.Text);
  cxGrid5TableView1.DataController.SetValue(n,cxGrid5TableView1.FindItemByTag(3).index,UnitAdvEditBtn33.Text);
  cxGrid5TableView1.DataController.SetValue(n,cxGrid5TableView1.FindItemByTag(4).index,AdvComboBox12.Text);
  cxGrid5TableView1.DataController.SetValue(n,cxGrid5TableView1.FindItemByTag(5).index,UnitAdvEditBtn41.Text);
  cxGrid5TableView1.DataController.SetValue(n,cxGrid5TableView1.FindItemByTag(6).index,UnitAdvEditBtn42.Text);
  cxGrid5TableView1.DataController.SetValue(n,cxGrid5TableView1.FindItemByTag(7).index,UnitAdvEditBtn43.Text);
  cxGrid5TableView1.DataController.SetValue(n,cxGrid5TableView1.FindItemByTag(8).index,UnitAdvEditBtn56.Text);
  cxGrid5TableView1.DataController.SetValue(n,cxGrid5TableView1.FindItemByTag(9).index,UnitAdvEditBtn59.Text);
  cxGrid5TableView1.DataController.SetValue(n,cxGrid5TableView1.FindItemByTag(10).index,UnitAdvEditBtn58.Text);
  cxGrid5TableView1.EndUpdate;
end;

procedure TForm1.SpinButton1DownClick(Sender: TObject);
begin
  UnitAdvEditBtn7.FloatValue:=UnitAdvEditBtn7.FloatValue-0.5;
  PipePressureCalcButtonClick(Sender);
end;

procedure TForm1.SpinButton1UpClick(Sender: TObject);
begin
  UnitAdvEditBtn7.FloatValue:=UnitAdvEditBtn7.FloatValue+0.5;
  PipePressureCalcButtonClick(Sender);
end;

procedure TForm1.SpinButton2DownClick(Sender: TObject);
begin
  UnitAdvEditBtn39.FloatValue:=UnitAdvEditBtn39.FloatValue-0.1;
  WaterSteamButtonClick(Sender);
end;

procedure TForm1.SpinButton2UpClick(Sender: TObject);
begin
  UnitAdvEditBtn39.FloatValue:=UnitAdvEditBtn39.FloatValue+0.1;
  WaterSteamButtonClick(Sender);
end;

procedure TForm1.SpinButton3DownClick(Sender: TObject);
begin
  UnitAdvEditBtn40.FloatValue:=UnitAdvEditBtn40.FloatValue-1;
  WaterSteamButtonClick(Sender);
end;

procedure TForm1.SpinButton3UpClick(Sender: TObject);
begin
  UnitAdvEditBtn40.FloatValue:=UnitAdvEditBtn40.FloatValue+1;
  WaterSteamButtonClick(Sender);
end;

procedure TForm1.CheckBox4Click(Sender: TObject);
begin
  if CheckBox4.Checked then begin
    AdvSpinEdit1.EditorEnabled:=True;
    AdvSpinEdit2.EditorEnabled:=True;
    AdvSpinEdit3.EditorEnabled:=True;
    AdvSpinEdit1.Color:=clWindow;
    AdvSpinEdit2.Color:=clWindow;
    AdvSpinEdit3.Color:=clWindow;
  end else begin
    AdvSpinEdit1.EditorEnabled:=False;
    AdvSpinEdit2.EditorEnabled:=False;
    AdvSpinEdit3.EditorEnabled:=False;
    AdvSpinEdit1.Color:=clSilver;
    AdvSpinEdit2.Color:=clSilver;
    AdvSpinEdit3.Color:=clSilver;
  end;
  if (CheckBox4.Checked=False) and (CheckBox5.Checked=False) then CheckBox4.Checked:=True;

  if (CheckBox4.Checked=False) or (CheckBox5.Checked=False) then begin // �����. ���
    AdvComboBox5.ItemIndex:=1;
    AdvComboBox6.ItemIndex:=1;
    AdvComboBox7.ItemIndex:=1;
  end else begin
    AdvComboBox5.ItemIndex:=-1;
    AdvComboBox6.ItemIndex:=-1;
    AdvComboBox7.ItemIndex:=-1;
  end;
//  VFlowSteamButtonClick(Sender);
end;

procedure TForm1.CheckBox5Click(Sender: TObject);
begin
  if CheckBox5.Checked then begin
    AdvSpinEdit4.EditorEnabled:=True;
    AdvSpinEdit5.EditorEnabled:=True;
    AdvSpinEdit6.EditorEnabled:=True;
    AdvSpinEdit4.Color:=clWindow;
    AdvSpinEdit5.Color:=clWindow;
    AdvSpinEdit6.Color:=clWindow;
  end else begin
    AdvSpinEdit4.EditorEnabled:=False;
    AdvSpinEdit5.EditorEnabled:=False;
    AdvSpinEdit6.EditorEnabled:=False;
    AdvSpinEdit4.Color:=clSilver;
    AdvSpinEdit5.Color:=clSilver;
    AdvSpinEdit6.Color:=clSilver;
  end;
  if (CheckBox4.Checked=False) and (CheckBox5.Checked=False) then CheckBox5.Checked:=True;

  if (CheckBox4.Checked=False) or (CheckBox5.Checked=False) then begin // �����. ���
    AdvComboBox5.ItemIndex:=1;
    AdvComboBox6.ItemIndex:=1;
    AdvComboBox7.ItemIndex:=1;
  end else begin
    AdvComboBox5.ItemIndex:=-1;
    AdvComboBox6.ItemIndex:=-1;
    AdvComboBox7.ItemIndex:=-1;
  end;
//  VFlowSteamButtonClick(Sender);
end;

procedure TForm1.AdvComboBox19Change(Sender: TObject);
begin
  case AdvComboBox19.ItemIndex of
  0: begin // ��������
     AdvSpinEdit7.Hide;
     AdvSpinEdit8.Hide;
     AdvSpinEdit9.Hide;
     AdvComboBox11.Hide;
     AdvSpinEdit11.EditorEnabled:=True;
     AdvSpinEdit12.EditorEnabled:=True;
     AdvSpinEdit13.EditorEnabled:=True;
     AdvSpinEdit11.Color:=clWindow;
     AdvSpinEdit12.Color:=clWindow;
     AdvSpinEdit13.Color:=clWindow;
     AdvComboBox5.Text:='��������';
     AdvComboBox6.Text:='��������';
     AdvComboBox7.Text:='��������';
     CheckBox4.Hide;
     CheckBox5.Hide;
     Label20.Caption:='';
     cxGrid7BandedTableView1.Bands[3].Caption:='��. ������ �.�.';
     cxGrid7BandedTableView1.Bands[3].Visible:=False;
     AdvComboBox20.Enabled:=True;
     AdvComboBox21.Enabled:=True;
     CheckBox12.Hide;
     UnitAdvEditBtn66.Hide;
     UnitAdvEditBtn66.FloatValue:=1000;
     MathImage28.Visible:=UnitAdvEditBtn66.Visible;
     VFlowSteamButton.Enabled:=False;
     end;
  1: begin // ���
     AdvSpinEdit7.Show;
     AdvSpinEdit8.Show;
     AdvSpinEdit9.Show;
     AdvSpinEdit7.Hint:='����������� �������� ������ � ���������� ��������';
     AdvSpinEdit8.Hint:='����������� �������� ������ � ���������� ��������';
     AdvSpinEdit9.Hint:='������������ �������� ������ � ���������� ��������';
     AdvComboBox11.Style:=csSimple;
     AdvComboBox11.ItemIndex:=0;
     AdvComboBox11.Text:='��3/�';
     AdvComboBox11.Enabled:=False;
     AdvComboBox11.Show;
     AdvSpinEdit11.EditorEnabled:=True;
     AdvSpinEdit12.EditorEnabled:=True;
     AdvSpinEdit13.EditorEnabled:=True;
     AdvSpinEdit11.Color:=clWindow;
     AdvSpinEdit12.Color:=clWindow;
     AdvSpinEdit13.Color:=clWindow;
     AdvComboBox5.Text:='���';
     AdvComboBox6.Text:='���';
     AdvComboBox7.Text:='���';
     CheckBox4.Hide;
     CheckBox5.Hide;
     Label20.Caption:='�����. ������ �.�. Qv�';
     cxGrid7BandedTableView1.Bands[3].Caption:='��. ������ �.�.';
     cxGrid7BandedTableView1.Bands[3].Visible:=True;
     AdvComboBox20.Enabled:=True;
     AdvComboBox21.Enabled:=True;
     CheckBox12.Show;
     UnitAdvEditBtn66.Show;
     UnitAdvEditBtn66.FloatValue:=1.2;
     MathImage28.Visible:=UnitAdvEditBtn66.Visible;
     VFlowSteamButton.Enabled:=True;
     end;
  2: begin // ���
     AdvSpinEdit7.Show;
     AdvSpinEdit8.Show;
     AdvSpinEdit9.Show;
     AdvSpinEdit7.Hint:='����������� �������� ������';
     AdvSpinEdit8.Hint:='����������� �������� ������';
     AdvSpinEdit9.Hint:='������������ �������� ������';
     AdvComboBox11.Style:=csDropDownList;
     AdvComboBox11.ItemIndex:=0;
     AdvComboBox11.Enabled:=True;
     AdvComboBox11.Show;
     AdvSpinEdit11.EditorEnabled:=False;
     AdvSpinEdit12.EditorEnabled:=False;
     AdvSpinEdit13.EditorEnabled:=False;
     AdvSpinEdit11.Color:=clSilver;
     AdvSpinEdit12.Color:=clSilver;
     AdvSpinEdit13.Color:=clSilver;
     AdvComboBox5.Text:='';
     AdvComboBox6.Text:='';
     AdvComboBox7.Text:='';
     CheckBox4.Show;
     CheckBox5.Show;
     Label20.Caption:='�������� ������ Q�';
     cxGrid7BandedTableView1.Bands[3].Caption:='�������� ������';
     cxGrid7BandedTableView1.Bands[3].Visible:=True;
     AdvComboBox20.ItemIndex:=0;
     AdvComboBox20.Enabled:=False;
     AdvComboBox21.ItemIndex:=0;
     AdvComboBox21.Enabled:=False;
     CheckBox12.Show;
     UnitAdvEditBtn66.Hide;
     UnitAdvEditBtn66.FloatValue:=0.6;
     MathImage28.Visible:=UnitAdvEditBtn66.Visible;
     VFlowSteamButton.Enabled:=True;
     end;
  end;
end;

procedure TForm1.VFlowSteamButtonClick(Sender: TObject);
var Pmin, Pnom, Pmax, Tmin, Tnom, Tmax, Denmin, Dennom, Denmax, StateMin, StateNom, StateMax: Real;
var Qmmin, Qmnom, Qmmax, Qvmin, Qvnom, Qvmax, UnitScale: Real;
var EntMin, EntNom, EntMax, den0: Real;
begin
  if AdvComboBox19.ItemIndex=0 then Exit; // ��������
  SetESBConvertorsUnits(AdvComboBox9.Text, dirIn);
  ESBPressureConvertor1.PressureUnitsOut:=eptPascals;
  ESBPressureConvertor1.ValueIn:=AdvSpinEdit1.FloatValue;
  Pmin:=ESBPressureConvertor1.ValueOut+101325;
  ESBPressureConvertor1.ValueIn:=AdvSpinEdit2.FloatValue;
  Pnom:=ESBPressureConvertor1.ValueOut+101325;
  ESBPressureConvertor1.ValueIn:=AdvSpinEdit3.FloatValue;
  Pmax:=ESBPressureConvertor1.ValueOut+101325;
  if (Pmin<0) or (Pnom<0) or (Pmax<0) then begin
    ShowMessage('���������� �������� ������ ���� ������ -1 ���');
    Exit;
  end;

  SetESBConvertorsUnits(AdvComboBox10.Text, dirIn);
  ESBTempConvertor1.TempUnitsOut:=ettKelvin;
  ESBTempConvertor1.ValueIn:=AdvSpinEdit4.FloatValue;
  Tmin:=ESBTempConvertor1.ValueOut;
  ESBTempConvertor1.ValueIn:=AdvSpinEdit5.FloatValue;
  Tnom:=ESBTempConvertor1.ValueOut;
  ESBTempConvertor1.ValueIn:=AdvSpinEdit6.FloatValue;
  Tmax:=ESBTempConvertor1.ValueOut;

  if AdvComboBox19.ItemIndex=1 then begin // ���
    Qmmin:=AdvSpinEdit7.FloatValue;
    Qmnom:=AdvSpinEdit8.FloatValue;
    Qmmax:=AdvSpinEdit9.FloatValue;
    if Pmin<>0 then Qvmin:=Qmmin*Tmin/273.15*101325/Pmin else Qvmin:=0;
    if Pnom<>0 then Qvnom:=Qmnom*Tnom/273.15*101325/Pnom else Qvnom:=0;
    if Pmax<>0 then Qvmax:=Qmmax*Tmax/273.15*101325/Pmax else Qvmax:=0;
    den0:=UnitAdvEditBtn66.FloatValue;
    if Tmin<>0 then Denmin:=den0*Pmin/101325*293.15/Tmin else Denmin:=0;
    if Tnom<>0 then Dennom:=den0*Pnom/101325*293.15/Tnom else Dennom:=0;
    if Tmax<>0 then Denmax:=den0*Pmax/101325*293.15/Tmax else Denmax:=0;
  end else begin // ���
    unitScale:=SetESBConvertorsUnits(AdvComboBox11.Text, dirIn);
    ESBFlowMassConvertor1.FlowUnitsOut:=eftKilogramsPerHour;
    ESBFlowMassConvertor1.ValueIn:=AdvSpinEdit7.FloatValue;
    Qmmin:=ESBFlowMassConvertor1.ValueOut*unitScale;
    ESBFlowMassConvertor1.ValueIn:=AdvSpinEdit8.FloatValue;
    Qmnom:=ESBFlowMassConvertor1.ValueOut*unitScale;
    ESBFlowMassConvertor1.ValueIn:=AdvSpinEdit9.FloatValue;
    Qmmax:=ESBFlowMassConvertor1.ValueOut*unitScale;
    Denmin:=1; Dennom:=1; Denmax:=1;
    if (CheckBox4.Checked=True) and (CheckBox5.Checked=True) then begin //�� �������� � �����������
      Denmin:=wspDPT(Pmin, Tmin);
      StateMin:=wspPHASESTATEPT(Pmin, Tmin);
      AdvComboBox5.ItemIndex:=Trunc(StateMin)+1;
      Dennom:=wspDPT(Pnom, Tnom);
      StateNom:=wspPHASESTATEPT(Pnom, Tnom);
      AdvComboBox6.ItemIndex:=Trunc(StateNom)+1;
      Denmax:=wspDPT(Pmax, Tmax);
      StateMax:=wspPHASESTATEPT(Pmax, Tmax);
      AdvComboBox7.ItemIndex:=Trunc(StateMax)+1;
    end else if CheckBox4.Checked=False then begin //�� �����������
      Denmin:=wspDSST(Tmin);
      Dennom:=wspDSST(Tnom);
      Denmax:=wspDSST(Tmax);
      Pmin:=wspPST(Tmin);
      Pnom:=wspPST(Tnom);
      Pmax:=wspPST(Tmax);
      AdvComboBox5.ItemIndex:=1;
      AdvComboBox6.ItemIndex:=1;
      AdvComboBox7.ItemIndex:=1;
      SetESBConvertorsUnits(AdvComboBox9.Text, dirOut);
      ESBPressureConvertor1.PressureUnitsIn:=eptPascals;
      ESBPressureConvertor1.ValueIn:=Pmin-101325;
      AdvSpinEdit1.FloatValue:=ESBPressureConvertor1.ValueOut;
      ESBPressureConvertor1.ValueIn:=Pnom-101325;
      AdvSpinEdit2.FloatValue:=ESBPressureConvertor1.ValueOut;
      ESBPressureConvertor1.ValueIn:=Pmax-101325;
      AdvSpinEdit3.FloatValue:=ESBPressureConvertor1.ValueOut;
    end else if CheckBox5.Checked=False then begin //�� ��������
      Tmin:=wspTSP(Pmin);
      Tnom:=wspTSP(Pnom);
      Tmax:=wspTSP(Pmax);
      Denmin:=wspDSST(Tmin);
      Dennom:=wspDSST(Tnom);
      Denmax:=wspDSST(Tmax);
      AdvComboBox5.ItemIndex:=1;
      AdvComboBox6.ItemIndex:=1;
      AdvComboBox7.ItemIndex:=1;
      ESBTempConvertor1.TempUnitsIn:=ettKelvin;
      SetESBConvertorsUnits(AdvComboBox10.Text, dirOut);
      ESBTempConvertor1.ValueIn:=Tmin;
      AdvSpinEdit4.FloatValue:=ESBTempConvertor1.ValueOut;
      ESBTempConvertor1.ValueIn:=Tnom;
      AdvSpinEdit5.FloatValue:=ESBTempConvertor1.ValueOut;
      ESBTempConvertor1.ValueIn:=Tmax;
      AdvSpinEdit6.FloatValue:=ESBTempConvertor1.ValueOut;
    end;

// ������ ������� �� ������� ����
    if (AdvComboBox11.Text='����/�') or (AdvComboBox11.Text='����/�') or (AdvComboBox11.Text='���/�') then begin
      EntMin:=wspHPT(Pmin, Tmin); //��/��
      EntNom:=wspHPT(Pnom, Tnom);
      EntMax:=wspHPT(Pmax, Tmax);
      UnitScale:=1;
      if AdvComboBox11.Text='����/�' then UnitScale:=4186.8
      else if AdvComboBox11.Text='����/�' then UnitScale:=4.1868e9
      else if AdvComboBox11.Text='���/�' then UnitScale:=1000;
      if EntMin=0 then Qmmin:=0 else Qmmin:=Qmmin/EntMin*UnitScale;
      if EntNom=0 then Qmnom:=0 else Qmnom:=Qmnom/EntNom*UnitScale;
      if EntMax=0 then Qmmax:=0 else Qmmax:=Qmmax/EntMax*UnitScale;
    end;

    if Denmin=0 then Qvmin:=0 else Qvmin:=Qmmin/Denmin;
    if Dennom=0 then Qvnom:=0 else Qvnom:=Qmnom/Dennom;
    if Denmax=0 then Qvmax:=0 else Qvmax:=Qmmax/Denmax;
  end;

  UnitAdvEditBtn45.FloatValue:=Denmin;
  UnitAdvEditBtn46.FloatValue:=Dennom;
  UnitAdvEditBtn47.FloatValue:=Denmax;
  AdvSpinEdit11.FloatValue:=Qvmin;
  AdvSpinEdit12.FloatValue:=Qvnom;
  AdvSpinEdit13.FloatValue:=Qvmax;
  AdvComboBox20.OnChange:=nil;
  AdvComboBox20.ItemIndex:=0; // �3/�
  AdvComboBox20.OnChange:=AdvComboBox9Change;
//  if CheckBox12.Checked then FlowDNCalcButtonClick(Sender);
end;

procedure TForm1.AdvSpinEdit1KeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key)=13 then VFlowSteamButtonClick(Sender);
end;

procedure TForm1.AdvSpinEdit11KeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key)=13 then FlowDNCalcButtonClick(Sender);
end;

procedure TForm1.AdvComboBox3Change(Sender: TObject);
var Kf, Qmax, Fmax: Real;
begin
  Kf:=EV200_Kf[AdvComboBox8.ItemIndex, AdvComboBox3.ItemIndex];
  Qmax:=EV200_Qmax[AdvComboBox8.ItemIndex, AdvComboBox3.ItemIndex];
  Fmax:=Qmax/3.6/Kf;
  UnitAdvEditBtn34.FloatValue:=Fmax;
  if UnitAdvEditBtn35.UnitID='���/�3' then Kf:=1000/Kf;
  UnitAdvEditBtn35.FloatValue:=Kf;
  UnitAdvEditBtn61.FloatValue:=1024/Fmax;
end;

procedure TForm1.Freq2FlowButtonClick(Sender: TObject);
var Kf, F, Q: Real;
begin
  Kf:=UnitAdvEditBtn35.FloatValue;
  F:=UnitAdvEditBtn38.FloatValue;
  if UnitAdvEditBtn35.UnitID='���/�3' then Kf:=1000/Kf;
  Q:=3.6*F*Kf;
  UnitAdvEditBtn37.FloatValue:=Q;
  if F<>0 then UnitAdvEditBtn61.FloatValue:=1024/F else UnitAdvEditBtn61.FloatValue:=0;
end;

procedure TForm1.Flow2FreqButtonClick(Sender: TObject);
var Kf, F, Q: Real;
begin
  Kf:=UnitAdvEditBtn35.FloatValue;
  Q:=UnitAdvEditBtn37.FloatValue;
  if UnitAdvEditBtn35.UnitID='���/�3' then Kf:=1000/Kf;
  if Kf=0 then begin
    ShowMessage('������� ��������� �������� �-�������'); Exit;
  end;
  F:=Q/3.6/Kf;
  UnitAdvEditBtn38.FloatValue:=F;
  if F<>0 then UnitAdvEditBtn61.FloatValue:=1024/F else UnitAdvEditBtn61.FloatValue:=0;
end;

procedure TForm1.UnitAdvEditBtn35KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then Freq2FlowButtonClick(Sender);
end;

procedure TForm1.UnitAdvEditBtn37KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then Flow2FreqButtonClick(Sender);
end;

procedure TForm1.Page9Add2TableButtonClick(Sender: TObject);
var n: ShortInt;
begin
  cxGrid9TableView1.BeginUpdate;
  n:=cxGrid9TableView1.DataController.AppendRecord;
  cxGrid9TableView1.DataController.SetValue(n,cxGrid9TableView1.FindItemByTag(0).index,cxGrid9TableView1.DataController.RecordCount);
  cxGrid9TableView1.DataController.SetValue(n,cxGrid9TableView1.FindItemByTag(1).index,AdvComboBox3.Text);
  cxGrid9TableView1.DataController.SetValue(n,cxGrid9TableView1.FindItemByTag(2).index,UnitAdvEditBtn35.Text);
  cxGrid9TableView1.DataController.SetValue(n,cxGrid9TableView1.FindItemByTag(3).index,UnitAdvEditBtn38.Text);
  cxGrid9TableView1.DataController.SetValue(n,cxGrid9TableView1.FindItemByTag(4).index,UnitAdvEditBtn37.Text);
  cxGrid9TableView1.DataController.SetValue(n,cxGrid9TableView1.FindItemByTag(5).index,UnitAdvEditBtn61.Text);
  cxGrid9TableView1.EndUpdate;
end;

procedure TForm1.FlowDNCalcButtonClick(Sender: TObject);
var Q1, Q2, Q3, Qmin, Qmax: Real;
var i, nmin, nmax, tn, medium: Integer;
var QmaxP, QminP, TOmax, TOmin, POmax, POmin: Real;
var Pmin, Pnom, Pmax, Tmin, Tnom, Tmax: Real;
begin
  SetESBConvertorsUnits(AdvComboBox20.Text, dirIn);
  ESBFlowConvertor1.FlowUnitsOut:=eftCubicMetresPerHour;
  ESBFlowConvertor1.ValueIn:=AdvSpinEdit11.FloatValue;
  Q1:=ESBFlowConvertor1.ValueOut;
  ESBFlowConvertor1.ValueIn:=AdvSpinEdit12.FloatValue;
  Q2:=ESBFlowConvertor1.ValueOut;
  ESBFlowConvertor1.ValueIn:=AdvSpinEdit13.FloatValue;
  Q3:=ESBFlowConvertor1.ValueOut;

  if Q1=0 then Qmin:=MinValue([Q2,Q3])
  else Qmin:=MinValue([Q1,Q2,Q3]);
  Qmax:=MaxValue([Q1,Q2,Q3]);

  if AdvComboBox19.ItemIndex=0 then medium:=0 else medium:=1;
  for i:=0 to Length(EV200_DN)-1 do begin
    EV200_QminP[i]:=0;
    EV200_QmaxP[i]:=0;
  end;

  SetESBConvertorsUnits(AdvComboBox9.Text, dirIn);
  ESBPressureConvertor1.PressureUnitsOut:=eptPascals;
  ESBPressureConvertor1.ValueIn:=AdvSpinEdit1.FloatValue;
  Pmin:=ESBPressureConvertor1.ValueOut+101325;
  ESBPressureConvertor1.ValueIn:=AdvSpinEdit2.FloatValue;
  Pnom:=ESBPressureConvertor1.ValueOut+101325;
  ESBPressureConvertor1.ValueIn:=AdvSpinEdit3.FloatValue;
  Pmax:=ESBPressureConvertor1.ValueOut+101325;
  POmax:=MaxValue([Pmin,Pnom,Pmax]);
  POmin:=MinValue([Pmin,Pnom,Pmax]);

  SetESBConvertorsUnits(AdvComboBox10.Text, dirIn);
  ESBTempConvertor1.TempUnitsOut:=ettKelvin;
  ESBTempConvertor1.ValueIn:=AdvSpinEdit4.FloatValue;
  Tmin:=ESBTempConvertor1.ValueOut;
  ESBTempConvertor1.ValueIn:=AdvSpinEdit5.FloatValue;
  Tnom:=ESBTempConvertor1.ValueOut;
  ESBTempConvertor1.ValueIn:=AdvSpinEdit6.FloatValue;
  Tmax:=ESBTempConvertor1.ValueOut;
  TOmax:=MaxValue([Tmin,Tnom,Tmax]);
  TOmin:=MinValue([Tmin,Tnom,Tmax]);

  if TOmax>593 then tn:=3
  else if TOmax>523 then tn:=2
  else if TOmax>373 then tn:=1
  else tn:=0;
  AdvComboBox13.Tag:=tn;
  AdvComboBox13.Clear;

// ��������� ���. �������� �� ��������
  if CheckBox12.Checked then begin
    if AdvComboBox19.ItemIndex=1 then AdvComboBox17.ItemIndex:=0 // ����� �� ������� ������ ����.
    else begin // ���
      AdvComboBox17.ItemIndex:=1;
      CheckBox8.Checked:=CheckBox4.Checked xor CheckBox5.Checked;
    end;
    UnitAdvEditBtn55.FloatValue:=UnitAdvEditBtn66.FloatValue; // ���������

    if POmax>6.4*1e6 then AdvComboBox27.ItemIndex:=2  // ���. �� ����. �� ������� ������ ����.
    else if POmax>2.5*1e6 then AdvComboBox27.ItemIndex:=1
    else AdvComboBox27.ItemIndex:=0;

    if TOmax>593 then AdvComboBox25.ItemIndex:=3  // ���. �� ����. �� ������� ������ ����.
    else if TOmax>373 then AdvComboBox25.ItemIndex:=2
    else AdvComboBox25.ItemIndex:=0;
  end;

// ������ ������������ ������������
 // ������ ��-200
  nmin:=-1;
  for i:=0 to Length(EV200_DN)-1 do begin
    QmaxP:=EV200_Qmax[medium][i];
    if CheckBox12.Checked and (medium<>0) then begin
      AdvComboBox18.ItemIndex:=i;
      CalcPresFlowRate(POmax, TOmax);
      QmaxP:=UnitAdvEditBtn48.FloatValue;
      EV200_QmaxP[i]:=QmaxP;
    end;
    if (QmaxP>=Qmax) and (nmin=-1) then nmin:=i;
  end;
  nmax:=-1;
  for i:=Length(EV200_DN)-1 downto 0 do begin
    QminP:=EV200_QminExt[tn][medium][i];
    if CheckBox12.Checked and (medium<>0) then begin
      AdvComboBox18.ItemIndex:=i;
      CalcPresFlowRate(POmin,TOmin);
      QminP:=UnitAdvEditBtn44.FloatValue;
      EV200_QminP[i]:=QminP;
    end;
    if (QminP<=Qmin) and (nmax=-1) then nmax:=i;
  end;
  if (nmin<>-1) and (nmax<>-1) then begin
    for i:=nmin to nmax do AdvComboBox13.Items.Add(IntToStr(EV200_DN[i])+' - ��200');
  end;

 // ������ ��-205
  if (tn<2) and (POmax<=4101325) then begin // T<250C
    nmin:=-1;
    for i:=0 to Length(EV205_DN)-1 do begin
      if EV205_Qmax[medium][i]>=Qmax then begin
        nmin:=i;      Break;
      end;
    end;
    nmax:=-1;
    for i:=Length(EV205_DN)-1 downto 0 do begin
      if EV205_QminExt[medium][i]<=Qmin then begin
        nmax:=i;      Break;
      end;
    end;
    if (nmin<>-1) and (nmax<>-1) then begin
      for i:=nmin to nmax do AdvComboBox13.Items.Add(IntToStr(EV205_DN[i])+' - ��205');
    end;
  end;
// ����� ������� ������������

  if AdvComboBox13.Items.Count=0 then begin
    Label29.Show;  Label25.Hide;  Label26.Hide;
    GaugeDouble1.Progress:=0;
    Exit;
  end;
  Label29.Hide;  Label25.Show;  Label26.Show;
  GaugeDouble1.Progress:=Trunc(Qmax);
  GaugeDouble1.ProgressMin:=Trunc(Qmin);
  AdvComboBox13.ItemIndex:=AdvComboBox13.Items.Count-1;
  AdvComboBox13Change(Sender);
end;

procedure TForm1.AdvComboBox13Change(Sender: TObject);
var i, n, dn, tn, medium: Integer;
var s: string;
begin
  s:=AdvComboBox13.Text;
  if AdvComboBox19.ItemIndex=0 then medium:=0 else medium:=1;
  Val(s,dn,n);
  tn:=AdvComboBox13.Tag;
  n:=LastDelimiter(' ',s);
  s:=Copy(s,n+1,50);
  n:=0;

  if s='��200' then begin
    for i:=0 to Length(EV200_DN)-1 do begin
      if dn=EV200_DN[i] then begin
        n:=i;      Break;
      end;
    end;
    if CheckBox12.Checked and (medium<>0) then begin  // ��������� ���. ����.
      GaugeDouble1.MaxValue:=Trunc(EV200_QmaxP[n]);
      Label25.Caption:=floattostr(EV200_QminP[n]);
      Label26.Caption:=floattostr(EV200_QmaxP[n]);
    end else begin
      GaugeDouble1.MaxValue:=Trunc(EV200_Qmax[medium][n]);
      Label25.Caption:=floattostr(EV200_QminExt[tn][medium][n]);
      Label26.Caption:=floattostr(EV200_Qmax[medium][n]);
    end;
    Exit;
  end;
  if s='��205' then begin
    for i:=0 to Length(EV205_DN)-1 do begin
      if dn=EV205_DN[i] then begin
        n:=i;      Break;
      end;
    end;
    GaugeDouble1.MaxValue:=Trunc(EV205_Qmax[medium][n]);
    Label25.Caption:=floattostr(EV205_QminExt[medium][n]);
    Label26.Caption:=floattostr(EV205_Qmax[medium][n]);
    Exit;
  end;
end;

procedure TForm1.AdvComboBox14Change(Sender: TObject);
begin
  AdvComboBox15.Clear;
  AdvComboBox15.Items.Add('��������');
  AdvComboBox15.Items.Add('���');
  case AdvComboBox14.ItemIndex of
    0: begin // ��-200
      AdvComboBox16.Clear;
      AdvComboBox16.Items.Add('100');
      AdvComboBox16.Items.Add('250');
      AdvComboBox16.Items.Add('320');
      AdvComboBox16.Items.Add('460');
      AdvComboBox24.Enabled:=True;
    end;
    1: begin // ��-205
      AdvComboBox16.Clear;
      AdvComboBox16.Items.Add('100');
      AdvComboBox16.Items.Add('250');
      AdvComboBox24.Enabled:=False;
    end;
    2: begin // ��200-���
      AdvComboBox16.Clear;
      AdvComboBox16.Items.Add('100');
      AdvComboBox15.Items.Delete(1);
      AdvComboBox24.Enabled:=False;
    end;
  end;
  AdvComboBox15.ItemIndex:=0;
  AdvComboBox16.ItemIndex:=0;
  AdvComboBox15Change(Sender);
end;

procedure TForm1.AdvComboBox24Change(Sender: TObject);
var i: ShortInt;
begin
  i:=AdvComboBox16.ItemIndex;
  AdvComboBox16.Clear;
  AdvComboBox16.Items.Add('100');
  AdvComboBox16.Items.Add('250');
  if AdvComboBox24.ItemIndex<2 then begin
    AdvComboBox16.Items.Add('320');
    AdvComboBox16.Items.Add('460');
  end;
  if i>AdvComboBox16.Items.Count-1 then i:=0;
  AdvComboBox16.ItemIndex:=i;
  AdvComboBox15Change(Sender);
end;

procedure TForm1.AdvComboBox15Change(Sender: TObject);
var i, n, dncount, t, medium, devType, istart: ShortInt;
var sdn, sqmin, svmin, dd: string;
var qmin, qminExt, qmax, qminFull, qmaxFull, InnerD, pw, vmin, vminExt, vmax: Real;
begin
  devType:=AdvComboBox14.ItemIndex;
  case devType of
    0: dncount:=Length(EV200_DN);
    1: dncount:=Length(EV205_DN);
    2: dncount:=Length(EV200PPD_DN);
    else dncount:=0;
  end;
  if devType=2 then begin
    cxGrid6Column3.Caption:='Qmin �����.';
    cxGrid6Column4.Caption:='Qmax �����.';
    cxGrid6Column5.Visible:=True;
    cxGrid6Column6.Visible:=True;
    cxGrid6Column7.Visible:=True;
  end else begin
    cxGrid6Column3.Caption:='Qmin';
    cxGrid6Column4.Caption:='Qmax';
    cxGrid6Column5.Visible:=False;
    cxGrid6Column6.Visible:=False;
    if devType=0 then cxGrid6Column7.Visible:=True
    else cxGrid6Column7.Visible:=False;
  end;
  t:=AdvComboBox16.ItemIndex;
  medium:=AdvComboBox15.ItemIndex;
  cxGrid6TableView1.BeginUpdate;
  cxGrid6TableView1.DataController.SetRecordCount(0);

  istart:=0;
  if AdvComboBox24.ItemIndex=2 then dncount:=8;   // 16-25 ���
  if AdvComboBox16.ItemIndex=3 then istart:=3;   // +460C

  for i:=istart to dncount-1 do begin
    n:=cxGrid6TableView1.DataController.AppendRecord;
    qminFull:=0;  qmaxFull:=0;  InnerD:=0;
    sdn:='';  qmin:=0;  qmax:=0;  pw:=0;  qminExt:=0;  vmin:=0;  vminExt:=0;  vmax:=0;
    case devType of
      0: begin  // ��200
        sdn:=IntToStr(EV200_DN[i]);
        qmin:=EV200_Qmin[t,medium,i];
        qminExt:=EV200_QminExt[t,medium,i];
        qmax:=EV200_Qmax[medium,i];
        pw:=EV200_PulseWeight[medium,i];
        InnerD:=EV200_InnerD[AdvComboBox24.ItemIndex,i];
        if (AdvComboBox16.ItemIndex=3) and (AdvComboBox24.ItemIndex=0) and (EV200_DN[i]>100) then begin // +460C 2,5 ��� ��>100
          InnerD:=EV200_InnerD[2,i];
        end;
        vmin:=qmin*4/3.1416/InnerD/InnerD/0.0036;
        vminExt:=qminExt*4/3.1416/InnerD/InnerD/0.0036;
        vmax:=qmax*4/3.1416/InnerD/InnerD/0.0036;
      end;
      1: begin  // ��205
        sdn:=IntToStr(EV205_DN[i]);
        qmin:=EV205_Qmin[medium,i];
        qminExt:=EV205_QminExt[medium,i];
        qmax:=EV205_Qmax[medium,i];
        pw:=EV205_PulseWeight[medium];
        InnerD:=EV205_DN[i];
        vmin:=qmin*4/3.1416/InnerD/InnerD/0.0036;
        vminExt:=qminExt*4/3.1416/InnerD/InnerD/0.0036;
        vmax:=qmax*4/3.1416/InnerD/InnerD/0.0036;
      end;
      2: begin  // ��200-���
        sdn:=EV200PPD_DN[i];
        qmin:=EV200PPD_Qmin[0,i];
        qmax:=EV200PPD_Qmax[0,i];
        qminFull:=EV200PPD_Qmin[1,i];
        qmaxFull:=EV200PPD_Qmax[1,i];
        InnerD:=EV200PPD_InnerD[i];
        pw:=EV200PPD_PulseWeight[i];
        vmin:=qminFull*4/3.1416/InnerD/InnerD/0.0036;
        vmax:=qmaxFull*4/3.1416/InnerD/InnerD/0.0036;
      end;
    end;

    if vmin=0 then dd:=''
    else dd:='1:'+FloatToStr(RoundTo(vmax/vmin,0));
    vmin:=RoundTo(vmin,-2);
    vminExt:=RoundTo(vminExt,-2);
    vmax:=RoundTo(vmax,-2);
    sqmin:=FloatToStr(qmin);
    svmin:=FloatToStr(vmin);
    if t=0 then begin
      if (devType=0) or ((devType=1) and (medium=1)) then begin
       sqmin:=sqmin+' ('+ FloatToStr(qminExt)+')';
       svmin:=svmin+' ('+ FloatToStr(vminExt)+')';
      end;
    end;

    cxGrid6TableView1.DataController.SetValue(n,cxGrid6TableView1.FindItemByTag(0).index,cxGrid6TableView1.DataController.RecordCount);
    cxGrid6TableView1.DataController.SetValue(n,cxGrid6TableView1.FindItemByTag(1).index,sdn);
    cxGrid6TableView1.DataController.SetValue(n,cxGrid6TableView1.FindItemByTag(2).index,sqmin);
    cxGrid6TableView1.DataController.SetValue(n,cxGrid6TableView1.FindItemByTag(3).index,qmax);
    if devType=2 then begin
      cxGrid6TableView1.DataController.SetValue(n,cxGrid6TableView1.FindItemByTag(4).index,qminFull);
      cxGrid6TableView1.DataController.SetValue(n,cxGrid6TableView1.FindItemByTag(5).index,qmaxFull);
    end;
    cxGrid6TableView1.DataController.SetValue(n,cxGrid6TableView1.FindItemByTag(6).index,InnerD);
    cxGrid6TableView1.DataController.SetValue(n,cxGrid6TableView1.FindItemByTag(7).index,pw);
    cxGrid6TableView1.DataController.SetValue(n,cxGrid6TableView1.FindItemByTag(8).index,svmin);
    cxGrid6TableView1.DataController.SetValue(n,cxGrid6TableView1.FindItemByTag(9).index,vmax);
    cxGrid6TableView1.DataController.SetValue(n,cxGrid6TableView1.FindItemByTag(10).index,dd);
    cxGrid6TableView1.DataController.Post(True);
  end;

  cxGrid6TableView1.EndUpdate;
end;

procedure TForm1.Export2Excel9ButtonClick(Sender: TObject);
begin
  if SaveDialog1.Execute then begin
    ExportGridToExcel(SaveDialog1.FileName, cxGrid7,True,True,False);
  end;
end;

procedure TForm1.Page8Add2TableButtonClick(Sender: TObject);
var n: ShortInt;
begin
  cxGrid7BandedTableView1.BeginUpdate;
  n:=cxGrid7BandedTableView1.DataController.AppendRecord;
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(0).index,cxGrid7BandedTableView1.DataController.RecordCount);
// ��������
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(1).index,AdvSpinEdit1.FloatValue);
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(2).index,AdvSpinEdit2.FloatValue);
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(3).index,AdvSpinEdit3.FloatValue);
// �����������
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(4).index,AdvSpinEdit4.FloatValue);
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(5).index,AdvSpinEdit5.FloatValue);
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(6).index,AdvSpinEdit6.FloatValue);
// ����. ������
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(7).index,AdvSpinEdit7.FloatValue);
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(8).index,AdvSpinEdit8.FloatValue);
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(9).index,AdvSpinEdit9.FloatValue);
// ���������
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(10).index,UnitAdvEditBtn45.Text);
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(11).index,UnitAdvEditBtn46.Text);
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(12).index,UnitAdvEditBtn47.Text);
// ���������
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(13).index,AdvComboBox5.Text);
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(14).index,AdvComboBox6.Text);
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(15).index,AdvComboBox7.Text);
// ��. ������
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(16).index,AdvSpinEdit11.FloatValue);
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(17).index,AdvSpinEdit12.FloatValue);
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(18).index,AdvSpinEdit13.FloatValue);
// ������
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(19).index,AdvComboBox13.Text); //DN
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(20).index,''); //Order
  cxGrid7BandedTableView1.DataController.SetValue(n,cxGrid7BandedTableView1.FindItemByTag(21).index,''); //Comments

  cxGrid7BandedTableView1.EndUpdate;
end;

procedure TForm1.AdvComboBox17Change(Sender: TObject);
begin
  case AdvComboBox17.ItemIndex of // density
    0: begin // ����� - ������
      UnitAdvEditBtn55.FloatValue:=1.2;
      CheckBox8.Hide;
      AdvComboBox26.Hide;
      UnitAdvEditBtn50.EditorEnabled:=True;
      UnitAdvEditBtn50.Color:=clWindow;
      UnitAdvEditBtn50.ReadOnly:=False;
      UnitAdvEditBtn50.Text:='20';
    end;
    1: begin // ����� - ���
      UnitAdvEditBtn55.FloatValue:=0.6;
      CheckBox8.Show;
      AdvComboBox26.Show;
      CheckBox8Click(Sender);
    end;
    2: begin // ����� - ������
      UnitAdvEditBtn55.FloatValue:=0.67;
      CheckBox8.Hide;
      AdvComboBox26.Hide;
      UnitAdvEditBtn50.EditorEnabled:=True;
      UnitAdvEditBtn50.Color:=clWindow;
      UnitAdvEditBtn50.ReadOnly:=False;
      UnitAdvEditBtn50.Text:='20';
    end;
  end;
  AirSteamFlowRangeButtonClick(Sender);
end;

procedure TForm1.AirSteamFlowRangeButtonClick(Sender: TObject);
var p,t: Real;
begin
  SetESBConvertorsUnits(UnitAdvEditBtn36.UnitID, dirIn);
  ESBPressureConvertor1.PressureUnitsOut:=eptPascals;
  ESBPressureConvertor1.ValueIn:=UnitAdvEditBtn36.FloatValue;
  p:=ESBPressureConvertor1.ValueOut+101325;
  if p<=0 then begin
    ShowMessage('���������� �������� ������ ���� ������ -1 ���');
    exit;
  end;
  SetESBConvertorsUnits(UnitAdvEditBtn50.UnitID, dirIn);
  ESBTempConvertor1.TempUnitsOut:=ettKelvin;
  ESBTempConvertor1.ValueIn:=UnitAdvEditBtn50.FloatValue;
  t:=ESBTempConvertor1.ValueOut;
  CalcPresFlowRate(p,t);
end;

procedure TForm1.AdvComboBox18Change(Sender: TObject);
var LSmin, LSmax: TLineSeries;
var p,t: Real;
var n: ShortInt;
var max: Real;
begin
  if CheckBox11.Checked then begin
    Chart2.SeriesList.Clear;
    LSmin:=TLineSeries.Create(Self);
    Chart2.AddSeries(LSmin);
    LSmin.LinePen.Width:=2;
    LSmin.Marks.Visible:=True;
    LSmin.Marks.Style:=smsLegend;
    LSmax:=TLineSeries.Create(Self);
    Chart2.AddSeries(LSmax);
    LSmax.LinePen.Width:=2;
    LSmax.Marks.Visible:=True;
    LSmax.Marks.Style:=smsLegend;
    SetESBConvertorsUnits(UnitAdvEditBtn50.UnitID, dirIn);
    ESBTempConvertor1.TempUnitsOut:=ettKelvin;
    ESBTempConvertor1.ValueIn:=UnitAdvEditBtn50.FloatValue;
    t:=ESBTempConvertor1.ValueOut;
    for n:=1 to Length(Pressure_List) do begin
      p:=Pressure_List[n]*1000+101325;
      CalcPresFlowRate(p,t);
      if n=Length(Pressure_List)-2 then begin
        LSmin.AddXY(Pressure_List[n]/1000,UnitAdvEditBtn44.FloatValue,'Qmin',clBlue);
        LSmax.AddXY(Pressure_List[n]/1000,UnitAdvEditBtn48.FloatValue,'Qmax',clRed);
      end else begin
        LSmin.AddXY(Pressure_List[n]/1000,UnitAdvEditBtn44.FloatValue,'',clBlue);
        LSmax.AddXY(Pressure_List[n]/1000,UnitAdvEditBtn48.FloatValue,'',clRed);
      end;
    end;
    max:=LSmax.MaxYValue;
    Chart2.LeftAxis.Automatic:=False;
    Chart2.LeftAxis.Maximum:=max*1.05;
    Chart2.LeftAxis.Minimum:=0;
  end;
  AirSteamFlowRangeButtonClick(Sender);
end;

procedure TForm1.PresFlowTableButtonClick(Sender: TObject);
var i,n: Integer;
var p,t: Real;
begin
  cxGrid8TableView1.BeginUpdate;
  cxGrid8TableView1.DataController.SetRecordCount(0);
  SetESBConvertorsUnits(UnitAdvEditBtn50.UnitID, dirIn);
  ESBTempConvertor1.TempUnitsOut:=ettKelvin;
  ESBTempConvertor1.ValueIn:=UnitAdvEditBtn50.FloatValue;
  t:=ESBTempConvertor1.ValueOut;
  for i:=1 to Length(Pressure_List) do begin
    p:=Pressure_List[i]*1000+101325;
    CalcPresFlowRate(p,t);
    n:=cxGrid8TableView1.DataController.AppendRecord;
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(0).index,cxGrid8TableView1.DataController.RecordCount);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(1).index,StrToFloat(AdvComboBox18.Text));
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(2).index,Pressure_List[i]);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(3).index,UnitAdvEditBtn57.Text);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(4).index,UnitAdvEditBtn44.Text);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(5).index,UnitAdvEditBtn48.Text);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(6).index,UnitAdvEditBtn51.Text);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(7).index,UnitAdvEditBtn52.Text);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(8).index,'1:'+FloatToStr(Round(UnitAdvEditBtn48.FloatValue/UnitAdvEditBtn44.FloatValue)));
  end;
  cxGrid8TableView1.EndUpdate;
  AirSteamFlowRangeButtonClick(Sender);
end;

procedure TForm1.CalcPresFlowRate(p, t: Real);
var Qmin, Qmax, den0, den, K1, K2, Vmin, Vmax, S, denAir0 : Real;
var state: ShortInt;
var d, InnerD: Integer;
begin
  denAir0:=1.2;
  d:=AdvComboBox18.ItemIndex; // �� index
  InnerD:=EV200_InnerD[AdvComboBox27.ItemIndex,d];
  if (AdvComboBox25.ItemIndex=3) and (AdvComboBox27.ItemIndex=0) and (EV200_DN[d]>100) then begin // +460C 2,5 ��� ��>100
    InnerD:=EV200_InnerD[2,d];
  end;
  S:=3.1416*InnerD*InnerD/4;

  den0:=UnitAdvEditBtn55.FloatValue;
  case AdvComboBox25.ItemIndex of
    0: Qmin:=EV200_Qmin[0,1,AdvComboBox18.ItemIndex]; // +100�
    1: Qmin:=EV200_QminExt[0,1,AdvComboBox18.ItemIndex]; // +100� ������. ����.
    2: Qmin:=EV200_Qmin[1,1,AdvComboBox18.ItemIndex]; // +250, +320�
    3: Qmin:=EV200_Qmin[3,1,AdvComboBox18.ItemIndex]; // +460�
  else Qmin:=0;
  end;
  Qmax:=EV200_Qmax[1,AdvComboBox18.ItemIndex];

  if AdvComboBox17.ItemIndex=1 then begin // ���
    if CheckBox8.Checked then // ���������� ���
    begin
      t:=wspTSP(p);
      if t<0 then t:=0;
      ESBTempConvertor1.TempUnitsIn:=ettKelvin;
      ESBTempConvertor1.TempUnitsOut:=ettCelsius;
      ESBTempConvertor1.ValueIn:=t;
      UnitAdvEditBtn50.FloatValue:=ESBTempConvertor1.ValueOut;
      AdvComboBox26.Hide;
    end else begin // ���������� ���
      ESBTempConvertor1.TempUnitsIn:=ettCelsius;
      ESBTempConvertor1.TempUnitsOut:=ettKelvin;
      ESBTempConvertor1.ValueIn:=UnitAdvEditBtn50.FloatValue;
      t:=ESBTempConvertor1.ValueOut;
      state:=wspPHASESTATEPT(p, t);
      AdvComboBox26.ItemIndex:=Trunc(state)+1;
      if (AdvComboBox26.ItemIndex=1) or (AdvComboBox26.ItemIndex=3) then begin // ���
        AdvComboBox26.Hide;
      end else begin  // �������� ��� ������
        AdvComboBox26.Show;
      end;
    end;
    den:=wspDPT(p,t);
  end else begin  // �� ���
    den:=den0*p/101325*293.15/t;
  end;
  if den<=0 then begin
    ShowMessage('��������� ��� ���������� ����������');
    Exit;
  end;

  K1:=Sqr(Qmin/S/0.0036)*denAir0;
  Vmax:=Qmax/S/0.0036;
  K2:=UnitAdvEditBtn63.FloatValue;
  Vmin:=UnitAdvEditBtn64.FloatValue;
  if InnerD<=15 then Vmin:=UnitAdvEditBtn65.FloatValue; // Vmin ��� ��15

  UnitAdvEditBtn49.FloatValue:=K1;
  UnitAdvEditBtn54.FloatValue:=Vmax;
  if CheckBox7.Checked then Vmax:=UnitAdvEditBtn19.FloatValue;
  if CheckBox9.Checked then K1:=UnitAdvEditBtn62.FloatValue;
  Qmin:=MaxValue([Sqrt(K1/den),Vmin])*S*0.0036;
  Qmax:=MinValue([Sqrt(K2/den),Vmax])*S*0.0036;
  UnitAdvEditBtn57.FloatValue:=den;
  UnitAdvEditBtn44.FloatValue:=Qmin;
  UnitAdvEditBtn48.FloatValue:=Qmax;
  UnitAdvEditBtn51.FloatValue:=Qmin*den;
  UnitAdvEditBtn52.FloatValue:=Qmax*den;
end;

procedure TForm1.UnitAdvEditBtn36KeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key)=13 then AirSteamFlowRangeButtonClick(Sender);
end;

procedure TForm1.Page11Add2TableButtonClick(Sender: TObject);
var n: ShortInt;
begin
  cxGrid8TableView1.BeginUpdate;
  n:=cxGrid8TableView1.DataController.AppendRecord;
  cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(0).index,cxGrid8TableView1.DataController.RecordCount);
  cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(1).index,StrToFloat(AdvComboBox18.Text));
  cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(2).index,UnitAdvEditBtn36.Text);
  cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(3).index,UnitAdvEditBtn57.Text);
  cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(4).index,UnitAdvEditBtn44.Text);
  cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(5).index,UnitAdvEditBtn48.Text);
  cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(6).index,UnitAdvEditBtn51.Text);
  cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(7).index,UnitAdvEditBtn52.Text);
  cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(8).index,'1:'+FloatToStr(Round(UnitAdvEditBtn48.FloatValue/UnitAdvEditBtn44.FloatValue)));
  cxGrid8TableView1.EndUpdate;
end;

procedure TForm1.N1Click(Sender: TObject);
begin
  if not(ActiveControl.GetParentComponent is TcxGrid) then Exit;
  if SaveDialog1.Execute then begin
    ExportGridToExcel(SaveDialog1.FileName, (ActiveControl.GetParentComponent as TcxGrid),True,True,False);
    dxStatusBar1.Panels[0].Text:='��������� � '+ SaveDialog1.FileName;
  end;
end;

procedure TForm1.N2Click(Sender: TObject); // Edit
begin
  if not(ActiveControl.GetParentComponent is TcxGrid) then Exit;
  ((ActiveControl.GetParentComponent as TcxGrid).ActiveView as TcxGridTableView).OptionsData.Editing:=N2.Checked;
end;

procedure TForm1.N3Click(Sender: TObject); // Fit width
begin
  if not(ActiveControl.GetParentComponent is TcxGrid) then Exit;
  ((ActiveControl.GetParentComponent as TcxGrid).ActiveView as TcxGridTableView).OptionsView.ColumnAutoWidth:=N3.Checked;
end;

procedure TForm1.PopupMenu1Popup(Sender: TObject);
begin
  if not(ActiveControl.GetParentComponent is TcxGrid) then Exit;
  N2.Checked:=((ActiveControl.GetParentComponent as TcxGrid).ActiveView as TcxGridTableView).OptionsData.Editing;
  N3.Checked:=((ActiveControl.GetParentComponent as TcxGrid).ActiveView as TcxGridTableView).OptionsView.ColumnAutoWidth;
end;

procedure TForm1.FillDataTableButtonClick(Sender: TObject);
var i, n: Integer;
begin
  cxGrid10TableView1.BeginUpdate; // ��-200
  cxGrid10TableView1.DataController.RecordCount:=0;
  for i:=0 to Length(EV200_DN)-1 do begin
    n:=cxGrid10TableView1.DataController.AppendRecord;
    cxGrid10TableView1.DataController.SetValue(n,0,i);
    cxGrid10TableView1.DataController.SetValue(n,1,EV200_DN[i]);
    cxGrid10TableView1.DataController.SetValue(n,2,EV200_InnerD[0,i]);
    cxGrid10TableView1.DataController.SetValue(n,3,EV200_InnerD[1,i]);
    cxGrid10TableView1.DataController.SetValue(n,4,EV200_InnerD[2,i]);
    cxGrid10TableView1.DataController.SetValue(n,5,EV200_InnerD[3,i]);
    cxGrid10TableView1.DataController.SetValue(n,6,EV200_InnerD[4,i]);
    cxGrid10TableView1.DataController.SetValue(n,7,EV200_Qmax1000Hz[0,i]);
    cxGrid10TableView1.DataController.SetValue(n,8,EV200_Qmax1000Hz[1,i]);
    cxGrid10TableView1.DataController.SetValue(n,9,EV200_Kf[0,i]);
    cxGrid10TableView1.DataController.SetValue(n,10,EV200_Kf[1,i]);
    cxGrid10TableView1.DataController.SetValue(n,11,EV200_PulseWeight[0,i]);
    cxGrid10TableView1.DataController.SetValue(n,12,EV200_PulseWeight[1,i]);
    cxGrid10TableView1.DataController.SetValue(n,13,EV200_Qmin[0,0,i]);
    cxGrid10TableView1.DataController.SetValue(n,14,EV200_Qmin[0,1,i]);
    cxGrid10TableView1.DataController.SetValue(n,15,EV200_Qmin[1,0,i]);
    cxGrid10TableView1.DataController.SetValue(n,16,EV200_Qmin[1,1,i]);
    cxGrid10TableView1.DataController.SetValue(n,17,EV200_Qmin[2,0,i]);
    cxGrid10TableView1.DataController.SetValue(n,18,EV200_Qmin[2,1,i]);
    cxGrid10TableView1.DataController.SetValue(n,19,EV200_Qmin[3,0,i]);
    cxGrid10TableView1.DataController.SetValue(n,20,EV200_Qmin[3,1,i]);
    cxGrid10TableView1.DataController.SetValue(n,21,EV200_QminExt[0,0,i]);
    cxGrid10TableView1.DataController.SetValue(n,22,EV200_QminExt[0,1,i]);
    cxGrid10TableView1.DataController.SetValue(n,23,EV200_QminExt[1,0,i]);
    cxGrid10TableView1.DataController.SetValue(n,24,EV200_QminExt[1,1,i]);
    cxGrid10TableView1.DataController.SetValue(n,25,EV200_QminExt[2,0,i]);
    cxGrid10TableView1.DataController.SetValue(n,26,EV200_QminExt[2,1,i]);
    cxGrid10TableView1.DataController.SetValue(n,27,EV200_QminExt[3,0,i]);
    cxGrid10TableView1.DataController.SetValue(n,28,EV200_QminExt[3,1,i]);
    cxGrid10TableView1.DataController.SetValue(n,29,EV200_Qmax[0,i]);
    cxGrid10TableView1.DataController.SetValue(n,30,EV200_Qmax[1,i]);
    cxGrid10TableView1.DataController.SetValue(n,31,EV200_APLoss_WF[i]);
    cxGrid10TableView1.DataController.SetValue(n,32,EV200_PipeSizes[i]);
  end;
  cxGrid10TableView1.EndUpdate;

  cxGrid10TableView5.BeginUpdate; // ��-200-��
  cxGrid10TableView5.DataController.RecordCount:=0;
  for i:=0 to Length(EV200_FRDN)-1 do begin
    n:=cxGrid10TableView5.DataController.AppendRecord;
    cxGrid10TableView5.DataController.SetValue(n,0,i);
    cxGrid10TableView5.DataController.SetValue(n,1,EV200_FRDN[i]);
    cxGrid10TableView5.DataController.SetValue(n,2,EV200_FR_InnerD[i]);
    cxGrid10TableView5.DataController.SetValue(n,3,EV200_APLoss_FR[i]);
  end;
  cxGrid10TableView5.EndUpdate;

  cxGrid10TableView2.BeginUpdate; // ��-205
  cxGrid10TableView2.DataController.RecordCount:=0;
  for i:=0 to Length(EV205_DN)-1 do begin
    n:=cxGrid10TableView2.DataController.AppendRecord;
    cxGrid10TableView2.DataController.SetValue(n,0,i);
    cxGrid10TableView2.DataController.SetValue(n,1,EV205_DN[i]);
    cxGrid10TableView2.DataController.SetValue(n,2,EV205_K3[i]);
    cxGrid10TableView2.DataController.SetValue(n,3,EV205_Qmin[0,i]);
    cxGrid10TableView2.DataController.SetValue(n,4,EV205_Qmin[1,i]);
    cxGrid10TableView2.DataController.SetValue(n,5,EV205_QminExt[0,i]);
    cxGrid10TableView2.DataController.SetValue(n,6,EV205_QminExt[1,i]);
    cxGrid10TableView2.DataController.SetValue(n,7,EV205_Qmax[0,i]);
    cxGrid10TableView2.DataController.SetValue(n,8,EV205_Qmax[1,i]);
  end;
  cxGrid10TableView2.EndUpdate;
  
  cxGrid10TableView3.BeginUpdate; // ��200-���
  cxGrid10TableView3.DataController.RecordCount:=0;
  for i:=0 to Length(EV200PPD_DN)-1 do begin
    n:=cxGrid10TableView3.DataController.AppendRecord;
    cxGrid10TableView3.DataController.SetValue(n,0,i);
    cxGrid10TableView3.DataController.SetValue(n,1,EV200PPD_DN[i]);
    cxGrid10TableView3.DataController.SetValue(n,2,EV200PPD_InnerD[i]);
    cxGrid10TableView3.DataController.SetValue(n,3,EV200PPD_OuterD[i]);
    cxGrid10TableView3.DataController.SetValue(n,4,EV200PPD_Qmin[0,i]);
    cxGrid10TableView3.DataController.SetValue(n,5,EV200PPD_Qmin[1,i]);
    cxGrid10TableView3.DataController.SetValue(n,6,EV200PPD_Qmax[0,i]);
    cxGrid10TableView3.DataController.SetValue(n,7,EV200PPD_Qmax[1,i]);
    cxGrid10TableView3.DataController.SetValue(n,8,EV200PPD_PulseWeight[i]);
    cxGrid10TableView3.DataController.SetValue(n,9,EV200PPD_Kf[i]);
    cxGrid10TableView3.DataController.SetValue(n,10,EV200PPD_APLoss[i]);
  end;
  cxGrid10TableView3.EndUpdate;

  cxGrid10TableView6.BeginUpdate; // ��-1200
  cxGrid10TableView6.DataController.RecordCount:=0;
  for i:=0 to Length(EV1200_DN)-1 do begin
    n:=cxGrid10TableView6.DataController.AppendRecord;
    cxGrid10TableView6.DataController.SetValue(n,0,i);
    cxGrid10TableView6.DataController.SetValue(n,1,EV1200_DN[i]);
  end;
  cxGrid10TableView6.EndUpdate;

  cxGrid10TableView4.BeginUpdate; // ������
  cxGrid10TableView4.DataController.RecordCount:=0;
  for i:=0 to Length(Pressure_List)-1 do begin
    n:=cxGrid10TableView4.DataController.AppendRecord;
    cxGrid10TableView4.DataController.SetValue(n,0,i);
    cxGrid10TableView4.DataController.SetValue(n,11,Pressure_List[i+1]);
    if i<Length(Steel_Mechprop) then
      cxGrid10TableView4.DataController.SetValue(n,4,Steel_Mechprop[i]);
    if i<4 then begin
      cxGrid10TableView4.DataController.SetValue(n,6,AirSteam_FlowParams[i,1]);
      cxGrid10TableView4.DataController.SetValue(n,7,AirSteam_FlowParams[i,2]);
      cxGrid10TableView4.DataController.SetValue(n,8,AirSteam_FlowParams[i,3]);
      cxGrid10TableView4.DataController.SetValue(n,9,AirSteam_FlowParams[i,4]);
      cxGrid10TableView4.DataController.SetValue(n,10,AirSteam_FlowParams[i,5]);
    end;
  end;

  cxGrid10TableView4.DataController.SetValue(0,1,EV205_PulseWeight[0]);
  cxGrid10TableView4.DataController.SetValue(1,1,EV205_PulseWeight[1]);
  cxGrid10TableView4.DataController.SetValue(0,2,EV205_Kp[0]);
  cxGrid10TableView4.DataController.SetValue(1,2,EV205_Kp[1]);
  cxGrid10TableView4.DataController.SetValue(0,3,EV205_APLoss);
  cxGrid10TableView4.EndUpdate;
end;

procedure TForm1.ImportXLSArrayData;
var sheet: TSpreadSheet;
var xlsEngine: TCustomMSExcel;
var i, rc: Integer;
const row1: ShortInt = 3;
begin
  if not(FileExists('EV200const.xls')) then Exit;
  xlsEngine:=TMSExcel.Create(Self);

  with xlsEngine do
    try
      LoadFromFile('EV200const.xls');
      if LoadNULLs then Exit;
// ��-200
//      sheet:=Sheets.SpreadSheet(0); // ��-200
      sheet:=Sheets.SheetByName('��200'); // ��-200
      rc:=sheet.Cells.UsedRowCount-row1;
      if rc<1 then Exit;
      for i:=0 to rc-1 do
      begin
        EV200_DN[i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(1, i+row1)),0);
        EV200_InnerD[0,i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(2, i+row1)),0);
        EV200_InnerD[1,i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(3, i+row1)),0);
        EV200_InnerD[2,i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(4, i+row1)),0);
        EV200_InnerD[3,i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(5, i+row1)),0);
        EV200_InnerD[4,i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(6, i+row1)),0);
        EV200_Qmax1000Hz[0,i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(7, i+row1)),0);
        EV200_Qmax1000Hz[1,i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(8, i+row1)),0);
        EV200_Kf[0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(9, i+row1)),0);
        EV200_Kf[1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(10, i+row1)),0);
        EV200_PulseWeight[0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(11, i+row1)),0);
        EV200_PulseWeight[1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(12, i+row1)),0);
        EV200_Qmin[0,0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(13, i+row1)),0);
        EV200_Qmin[0,1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(14, i+row1)),0);
        EV200_Qmin[1,0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(15, i+row1)),0);
        EV200_Qmin[1,1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(16, i+row1)),0);
        EV200_Qmin[2,0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(17, i+row1)),0);
        EV200_Qmin[2,1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(18, i+row1)),0);
        EV200_Qmin[3,0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(19, i+row1)),0);
        EV200_Qmin[3,1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(20, i+row1)),0);
        EV200_QminExt[0,0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(21, i+row1)),0);
        EV200_QminExt[0,1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(22, i+row1)),0);
        EV200_QminExt[1,0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(23, i+row1)),0);
        EV200_QminExt[1,1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(24, i+row1)),0);
        EV200_QminExt[2,0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(25, i+row1)),0);
        EV200_QminExt[2,1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(26, i+row1)),0);
        EV200_QminExt[3,0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(27, i+row1)),0);
        EV200_QminExt[3,1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(28, i+row1)),0);
        EV200_Qmax[0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(29, i+row1)),0);
        EV200_Qmax[1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(30, i+row1)),0);
        EV200_APLoss_WF[i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(31, i+row1)),0);
        EV200_PipeSizes[i]:=VarToStr(sheet.Cells.GetValue(32, i+row1));
      end;

// ��-200-��
//      sheet:=Sheets.SpreadSheet(1); // ��-200-��
      sheet:=Sheets.SheetByName('��200-��'); // ��-200-��
      rc:=sheet.Cells.UsedRowCount-row1;
      if rc<1 then Exit;
      for i:=0 to rc-1 do
      begin
        EV200_FRDN[i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(1, i+row1)),0);
        EV200_FR_InnerD[i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(2, i+row1)),0);
        EV200_APLoss_FR[i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(3, i+row1)),0);
      end;

// ��-205
//      sheet:=Sheets.SpreadSheet(2); // ��-205
      sheet:=Sheets.SheetByName('��205'); // ��-205
      rc:=sheet.Cells.UsedRowCount-row1;
      if rc<1 then Exit;
      for i:=0 to rc-1 do
      begin
        EV205_DN[i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(1, i+row1)),0);
        EV205_K3[i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(2, i+row1)),0);
        EV205_Qmin[0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(3, i+row1)),0);
        EV205_Qmin[1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(4, i+row1)),0);
        EV205_QminExt[0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(5, i+row1)),0);
        EV205_QminExt[1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(6, i+row1)),0);
        EV205_Qmax[0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(7, i+row1)),0);
        EV205_Qmax[1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(8, i+row1)),0);
      end;

// ��-200-���
//      sheet:=Sheets.SpreadSheet(3); // ��-200-���
      sheet:=Sheets.SheetByName('��200-���'); // ��-200-���
      rc:=sheet.Cells.UsedRowCount-row1;
      if rc<1 then Exit;
      for i:=0 to rc-1 do
      begin
        EV200PPD_DN[i]:=VarToStr(sheet.Cells.GetValue(1, i+row1));
        EV200PPD_InnerD[i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(2, i+row1)),0);
        EV200PPD_OuterD[i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(3, i+row1)),0);
        EV200PPD_Qmin[0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(4, i+row1)),0);
        EV200PPD_Qmin[1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(5, i+row1)),0);
        EV200PPD_Qmax[0,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(6, i+row1)),0);
        EV200PPD_Qmax[1,i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(7, i+row1)),0);
        EV200PPD_PulseWeight[i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(8, i+row1)),0);
        EV200PPD_Kf[i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(9, i+row1)),0);
        EV200PPD_APLoss[i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(10, i+row1)),0);
      end;

// ��-1200
//      sheet:=Sheets.SpreadSheet(4); // ��-1200
      sheet:=Sheets.SheetByName('��1200'); // ��-1200
      rc:=sheet.Cells.UsedRowCount-row1;
      if rc<1 then Exit;
      for i:=0 to rc-1 do
      begin
        EV1200_DN[i]:=StrToIntDef(VarToStr(sheet.Cells.GetValue(1, i+row1)),0);
      end;

// ������
//      sheet:=Sheets.SpreadSheet(5); // ������
      sheet:=Sheets.SheetByName('������'); // ������
      rc:=sheet.Cells.UsedRowCount-row1;
      if rc<1 then Exit;
      for i:=0 to Length(Steel_Mechprop)-1 do begin
        Steel_Mechprop[i]:=VarToStr(sheet.Cells.GetValue(4, row1+i));
      end;
      for i:=1 to Length(Pressure_List) do begin
        Pressure_List[i]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(11, row1+i-1)),0);
      end;
      EV205_PulseWeight[0]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(1, row1)),0);
      EV205_PulseWeight[1]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(1, row1+1)),0);
      EV205_Kp[0]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(2, row1)),0);
      EV205_Kp[1]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(2, row1+1)),0);
      EV205_APLoss:=StrToIntDef(VarToStr(sheet.Cells.GetValue(3, row1)),0);

      for i:=0 to 3 do begin
        AirSteam_FlowParams[i,1]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(6, row1+i)),0); // K1
        AirSteam_FlowParams[i,2]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(7, row1+i)),0); // K2
        AirSteam_FlowParams[i,3]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(8, row1+i)),0); // Vmin
        AirSteam_FlowParams[i,4]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(9, row1+i)),0); // Vmax
        AirSteam_FlowParams[i,5]:=StrToFloatDef(VarToStr(sheet.Cells.GetValue(10, row1+i)),0); // Vmin DN15
      end;
      UnitAdvEditBtn62.FloatValue:=AirSteam_FlowParams[0,1]; // K1' ����.
      UnitAdvEditBtn63.FloatValue:=AirSteam_FlowParams[0,2]; // K2
      UnitAdvEditBtn64.FloatValue:=AirSteam_FlowParams[0,3]; // Vmin
      UnitAdvEditBtn19.FloatValue:=AirSteam_FlowParams[0,4]; // Vmax' ����.
      UnitAdvEditBtn65.FloatValue:=AirSteam_FlowParams[0,5]; // Vmin ��15
    finally
      Free
    end;
  dxStatusBar1.Panels[0].Text:='������ �������������';
end;

procedure TForm1.DataXLSImportButtonClick(Sender: TObject);
begin
  ImportXLSArrayData;
end;

procedure TForm1.cxPageControl1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button=mbRight) and (ssShift in Shift) then cxTabSheet13.TabVisible:=not(cxTabSheet13.TabVisible);
end;

procedure TForm1.PrintTableButtonClick(Sender: TObject);
begin
  dxComponentPrinter1.Preview(True);
end;

// ������� ������������� ������� ��������� � ����������� � ���������� ����� ��������������� (1) ��� 0, ���� ��� ����� ������
function TForm1.SetESBConvertorsUnits(cUnit: string; Dir: ShortInt): Real;
begin
  result:=1;
  if Dir=dirIn then begin // ������� �������
// �����������
    if cUnit='C' then ESBTempConvertor1.TempUnitsIn:=ettCelsius
    else if cUnit='K' then ESBTempConvertor1.TempUnitsIn:=ettKelvin
    else if cUnit='F' then ESBTempConvertor1.TempUnitsIn:=ettFahrenheit

// ��������
    else if cUnit='���' then ESBPressureConvertor1.PressureUnitsIn:=eptMegapascals
    else if cUnit='���' then ESBPressureConvertor1.PressureUnitsIn:=eptKilopascals
    else if cUnit='��' then ESBPressureConvertor1.PressureUnitsIn:=eptPascals
    else if cUnit='���' then ESBPressureConvertor1.PressureUnitsIn:=eptAtmospheres
    else if cUnit='���' then ESBPressureConvertor1.PressureUnitsIn:=eptBars
    else if cUnit='PSI' then ESBPressureConvertor1.PressureUnitsIn:=eptPSI
    else if cUnit='���/��2' then ESBPressureConvertor1.PressureUnitsIn:=eptKilogramsPerSqCentimetre
    else if cUnit='��.��.��' then ESBPressureConvertor1.PressureUnitsIn:=eptMillimetresOfMercury0C
    else if cUnit='�.���.��' then ESBPressureConvertor1.PressureUnitsIn:=eptMetresOfWater4C

// ���������
    else if cUnit='��/�3' then ESBDensityConvertor1.DensityUnitsIn:=edtKgPerCuM
    else if cUnit='�/��3' then ESBDensityConvertor1.DensityUnitsIn:=edtGPerCuCm

// �������� ������
    else if cUnit='�3/�' then ESBFlowConvertor1.FlowUnitsIn:=eftCubicMetresPerHour
    else if cUnit='�/�' then ESBFlowConvertor1.FlowUnitsIn:=eftLitresPerHour
    else if cUnit='�3/���' then ESBFlowConvertor1.FlowUnitsIn:=eftCubicMetresPerMinute
    else if cUnit='�/���' then ESBFlowConvertor1.FlowUnitsIn:=eftLitresPerMinute

// �������� ������
    else if cUnit='��/�' then ESBFlowMassConvertor1.FlowUnitsIn:=eftKilogramsPerHour
    else if cUnit='��/���' then ESBFlowMassConvertor1.FlowUnitsIn:=eftKilogramsPerMinute
    else if cUnit='��/�' then ESBFlowMassConvertor1.FlowUnitsIn:=eftKilogramsPerSecond
    else if cUnit='�/�' then ESBFlowMassConvertor1.FlowUnitsIn:=eftGramsPerSecond
    else if cUnit='�/���' then ESBFlowMassConvertor1.FlowUnitsIn:=eftGramsPerMinute
    else if cUnit='�/�' then begin
      ESBFlowMassConvertor1.FlowUnitsIn:=eftKilogramsPerHour;
      Result:=1000;
    end
    else if cUnit='�/���' then begin
      ESBFlowMassConvertor1.FlowUnitsIn:=eftKilogramsPerHour;
      Result:=1000/24;
    end
    
// ������������ ��������
    else if cUnit='��' then ESBVisDynConvertor1.VisDynUnitsIn:=evdtCentipoise
    else if cUnit='��*�' then ESBVisDynConvertor1.VisDynUnitsIn:=evdtPascalSecs
    else if cUnit='���*�/�2' then ESBVisDynConvertor1.VisDynUnitsIn:=evdtKGSecsPerSqM
    else if cUnit='�*�/��2' then ESBVisDynConvertor1.VisDynUnitsIn:=evdtGSecsPerSqCM
    else if cUnit='���*�' then ESBVisDynConvertor1.VisDynUnitsIn:=evdtCentipoise // =

// �������������� ��������
    else if cUnit='���' then ESBVisKinConvertor1.VisKinUnitsIn:=evktCentiStokes
    else if cUnit='��2/�' then ESBVisKinConvertor1.VisKinUnitsIn:=evktSqCmPerSec
    else if cUnit='�2/�' then ESBVisKinConvertor1.VisKinUnitsIn:=evktSqMPerSec

// ��������
    else if cUnit='��' then ESBPowerConvertor1.PowerUnitsIn:=eptWatts
    else if cUnit='���' then ESBPowerConvertor1.PowerUnitsIn:=eptKilowatts
    else if cUnit='���/�' then ESBPowerConvertor1.PowerUnitsIn:=eptCaloriesITPerHour
    else if cUnit='����/�' then ESBPowerConvertor1.PowerUnitsIn:=eptKilocaloriesITPerHour
    else if cUnit='���*�/�' then ESBPowerConvertor1.PowerUnitsIn:=eptKilogramMetresPerSecond
    else if cUnit='�.�.' then ESBPowerConvertor1.PowerUnitsIn:=eptHorsepower
    else if cUnit='��/�' then ESBPowerConvertor1.PowerUnitsIn:=eptJoulesPerHour
    else if cUnit='���/�' then begin
      ESBPowerConvertor1.PowerUnitsIn:=eptJoulesPerHour;
      Result:=1e3;
    end
    else if cUnit='���/�' then begin
      ESBPowerConvertor1.PowerUnitsIn:=eptJoulesPerHour;
      Result:=1e6;
    end
    else if cUnit='����/�' then begin
      ESBPowerConvertor1.PowerUnitsIn:=eptKilocaloriesITPerHour;
      Result:=1e6;
    end

// ���������
    else if cUnit='����/��' then ESBPowerConvertor1.PowerUnitsIn:=eptKilocaloriesITPerHour
    else if cUnit='��/��' then ESBPowerConvertor1.PowerUnitsIn:=eptJoulesPerHour
    else if cUnit='���/��' then begin
      ESBPowerConvertor1.PowerUnitsIn:=eptJoulesPerHour;
      Result:=1e3;
    end
    else if cUnit='���/��' then begin
      ESBPowerConvertor1.PowerUnitsIn:=eptJoulesPerHour;
      Result:=1e6;
    end
    else if cUnit='����/��' then begin
      ESBPowerConvertor1.PowerUnitsIn:=eptKilocaloriesITPerHour;
      Result:=1e6;
    end

    else if cUnit='��2' then ESBAreaConvertor1.AreaUnitsIn:=eatSqMillimetres
    else if cUnit='��2' then ESBAreaConvertor1.AreaUnitsIn:=eatSqCentimetres
    else if cUnit='�2' then ESBAreaConvertor1.AreaUnitsIn:=eatSqMetres

    else Result:=0;
  end else begin  // �������� �������
// �����������
    if cUnit='C' then ESBTempConvertor1.TempUnitsOut:=ettCelsius
    else if cUnit='K' then ESBTempConvertor1.TempUnitsOut:=ettKelvin
    else if cUnit='F' then ESBTempConvertor1.TempUnitsOut:=ettFahrenheit

// ��������
    else if cUnit='���' then ESBPressureConvertor1.PressureUnitsOut:=eptMegapascals
    else if cUnit='���' then ESBPressureConvertor1.PressureUnitsOut:=eptKilopascals
    else if cUnit='��' then ESBPressureConvertor1.PressureUnitsOut:=eptPascals
    else if cUnit='���' then ESBPressureConvertor1.PressureUnitsOut:=eptAtmospheres
    else if cUnit='���' then ESBPressureConvertor1.PressureUnitsOut:=eptBars
    else if cUnit='PSI' then ESBPressureConvertor1.PressureUnitsOut:=eptPSI
    else if cUnit='���/��2' then ESBPressureConvertor1.PressureUnitsOut:=eptKilogramsPerSqCentimetre
    else if cUnit='��.��.��' then ESBPressureConvertor1.PressureUnitsOut:=eptMillimetresOfMercury0C
    else if cUnit='�.���.��' then ESBPressureConvertor1.PressureUnitsOut:=eptMetresOfWater4C

// ���������
    else if cUnit='��/�3' then ESBDensityConvertor1.DensityUnitsOut:=edtKgPerCuM
    else if cUnit='�/��3' then ESBDensityConvertor1.DensityUnitsOut:=edtGPerCuCm

// �������� ������
    else if cUnit='�3/�' then ESBFlowConvertor1.FlowUnitsOut:=eftCubicMetresPerHour
    else if cUnit='�/�' then ESBFlowConvertor1.FlowUnitsOut:=eftLitresPerHour
    else if cUnit='�3/���' then ESBFlowConvertor1.FlowUnitsOut:=eftCubicMetresPerMinute
    else if cUnit='�/���' then ESBFlowConvertor1.FlowUnitsOut:=eftLitresPerMinute

// �������� ������
    else if cUnit='��/�' then ESBFlowMassConvertor1.FlowUnitsOut:=eftKilogramsPerHour
    else if cUnit='��/���' then ESBFlowMassConvertor1.FlowUnitsOut:=eftKilogramsPerMinute
    else if cUnit='��/�' then ESBFlowMassConvertor1.FlowUnitsOut:=eftKilogramsPerSecond
    else if cUnit='�/�' then ESBFlowMassConvertor1.FlowUnitsOut:=eftGramsPerSecond
    else if cUnit='�/���' then ESBFlowMassConvertor1.FlowUnitsOut:=eftGramsPerMinute
    else if cUnit='�/�' then begin
      ESBFlowMassConvertor1.FlowUnitsOut:=eftKilogramsPerHour;
      Result:=1e-3;
    end
    else if cUnit='�/���' then begin
      ESBFlowMassConvertor1.FlowUnitsOut:=eftKilogramsPerHour;
      Result:=24e-3;
    end

// ������������ ��������
    else if cUnit='��' then ESBVisDynConvertor1.VisDynUnitsOut:=evdtCentipoise
    else if cUnit='��*�' then ESBVisDynConvertor1.VisDynUnitsOut:=evdtPascalSecs
    else if cUnit='���*�/�2' then ESBVisDynConvertor1.VisDynUnitsOut:=evdtKGSecsPerSqM
    else if cUnit='�*�/��2' then ESBVisDynConvertor1.VisDynUnitsOut:=evdtGSecsPerSqCM
    else if cUnit='���*�' then ESBVisDynConvertor1.VisDynUnitsOut:=evdtCentipoise // =

// �������������� ��������
    else if cUnit='���' then ESBVisKinConvertor1.VisKinUnitsOut:=evktCentiStokes
    else if cUnit='��2/�' then ESBVisKinConvertor1.VisKinUnitsOut:=evktSqCmPerSec
    else if cUnit='�2/�' then ESBVisKinConvertor1.VisKinUnitsOut:=evktSqMPerSec

// ��������
    else if cUnit='��' then ESBPowerConvertor1.PowerUnitsOut:=eptWatts
    else if cUnit='���' then ESBPowerConvertor1.PowerUnitsOut:=eptKilowatts
    else if cUnit='���/�' then ESBPowerConvertor1.PowerUnitsOut:=eptCaloriesITPerHour
    else if cUnit='����/�' then ESBPowerConvertor1.PowerUnitsOut:=eptKilocaloriesITPerHour
    else if cUnit='���*�/�' then ESBPowerConvertor1.PowerUnitsOut:=eptKilogramMetresPerSecond
    else if cUnit='�.�.' then ESBPowerConvertor1.PowerUnitsOut:=eptHorsepower
    else if cUnit='��/�' then ESBPowerConvertor1.PowerUnitsOut:=eptJoulesPerHour
    else if cUnit='���/�' then begin
      ESBPowerConvertor1.PowerUnitsOut:=eptJoulesPerHour;
      Result:=1e-3;
    end
    else if cUnit='���/�' then begin
      ESBPowerConvertor1.PowerUnitsOut:=eptJoulesPerHour;
      Result:=1e-6;
    end
    else if cUnit='����/�' then begin
      ESBPowerConvertor1.PowerUnitsOut:=eptKilocaloriesITPerHour;
      Result:=1e-6;
    end

// ���������
    else if cUnit='����/��' then ESBPowerConvertor1.PowerUnitsOut:=eptKilocaloriesITPerHour
    else if cUnit='��/��' then ESBPowerConvertor1.PowerUnitsOut:=eptJoulesPerHour
    else if cUnit='���/��' then begin
      ESBPowerConvertor1.PowerUnitsOut:=eptJoulesPerHour;
      Result:=1e-3;
    end
    else if cUnit='���/��' then begin
      ESBPowerConvertor1.PowerUnitsOut:=eptJoulesPerHour;
      Result:=1e-6;
    end
    else if cUnit='����/��' then begin
      ESBPowerConvertor1.PowerUnitsOut:=eptKilocaloriesITPerHour;
      Result:=1e-6;
    end

    else if cUnit='��2' then ESBAreaConvertor1.AreaUnitsOut:=eatSqMillimetres
    else if cUnit='��2' then ESBAreaConvertor1.AreaUnitsOut:=eatSqCentimetres
    else if cUnit='�2' then ESBAreaConvertor1.AreaUnitsOut:=eatSqMetres

    else Result:=0;
  end;

end;

procedure TForm1.UnitAdvEditBtn1UnitChanged(Sender: TObject;
  NewUnit: String);
var UnitScaleIn, UnitScaleOut, val: Real;
begin
  if NewUnit=OldUnit then Exit;
  UnitScaleIn:=SetESBConvertorsUnits(OldUnit, dirIn);
  UnitScaleOut:=SetESBConvertorsUnits(NewUnit, dirOut);
  case (Sender as TUnitAdvEditBtn).Tag of
    0: begin
        ESBTempConvertor1.ValueIn:=(Sender as TUnitAdvEditBtn).FloatValue;
        val:=ESBTempConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
       end;
    1: begin
        ESBPressureConvertor1.ValueIn:=(Sender as TUnitAdvEditBtn).FloatValue;
        val:=ESBPressureConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
       end;
    2: begin
        ESBDensityConvertor1.ValueIn:=(Sender as TUnitAdvEditBtn).FloatValue;
        val:=ESBDensityConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
       end;
    3: begin
        ESBFlowConvertor1.ValueIn:=(Sender as TUnitAdvEditBtn).FloatValue;
        val:=ESBFlowConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
       end;
    4: begin
        ESBFlowMassConvertor1.ValueIn:=(Sender as TUnitAdvEditBtn).FloatValue;
        val:=ESBFlowMassConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
       end;
    5: begin
        ESBVisDynConvertor1.ValueIn:=(Sender as TUnitAdvEditBtn).FloatValue;
        val:=ESBVisDynConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
       end;
    6: begin
        ESBVisKinConvertor1.ValueIn:=(Sender as TUnitAdvEditBtn).FloatValue;
        val:=ESBVisKinConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
       end;
    7: begin // Kf
        val:=1000/(Sender as TUnitAdvEditBtn).FloatValue;
       end;
    8: begin // �����
        ESBAreaConvertor1.ValueIn:=(Sender as TUnitAdvEditBtn).FloatValue;
        val:=ESBAreaConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
       end;
    9: begin // ���������
        ESBPowerConvertor1.ValueIn:=(Sender as TUnitAdvEditBtn).FloatValue;
        val:=ESBPowerConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
       end;
    else  begin
        val:=0;
       end;
  end;
  (Sender as TUnitAdvEditBtn).FloatValue:=val;
end;

procedure TForm1.UnitAdvEditBtn1ClickBtn(Sender: TObject);
begin
  OldUnit:=(Sender as TUnitAdvEditBtn).UnitID;
end;

procedure TForm1.AdvComboBox9Change(Sender: TObject);
var NewUnit: String;
var UnitScaleIn, UnitScaleOut, val: Real;
begin
  NewUnit:=(Sender as TAdvComboBox).Text;
  if NewUnit=OldUnit then Exit;

  UnitScaleIn:=SetESBConvertorsUnits(OldUnit, dirIn);
  UnitScaleOut:=SetESBConvertorsUnits(NewUnit, dirOut);
  case (Sender as TAdvComboBox).Tag of
    0: begin
        ESBTempConvertor1.ValueIn:=AdvSpinEdit4.FloatValue;
        val:=ESBTempConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
        AdvSpinEdit4.FloatValue:=val;
        ESBTempConvertor1.ValueIn:=AdvSpinEdit5.FloatValue;
        val:=ESBTempConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
        AdvSpinEdit5.FloatValue:=val;
        ESBTempConvertor1.ValueIn:=AdvSpinEdit6.FloatValue;
        val:=ESBTempConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
        AdvSpinEdit6.FloatValue:=val;
       end;
    1: begin
        ESBPressureConvertor1.ValueIn:=AdvSpinEdit1.FloatValue;
        val:=ESBPressureConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
        AdvSpinEdit1.FloatValue:=val;
        ESBPressureConvertor1.ValueIn:=AdvSpinEdit2.FloatValue;
        val:=ESBPressureConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
        AdvSpinEdit2.FloatValue:=val;
        ESBPressureConvertor1.ValueIn:=AdvSpinEdit3.FloatValue;
        val:=ESBPressureConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
        AdvSpinEdit3.FloatValue:=val;
       end;
    2: begin
        ESBDensityConvertor1.ValueIn:=UnitAdvEditBtn45.FloatValue;
        val:=ESBDensityConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
        UnitAdvEditBtn45.FloatValue:=val;
        ESBDensityConvertor1.ValueIn:=UnitAdvEditBtn46.FloatValue;
        val:=ESBDensityConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
        UnitAdvEditBtn46.FloatValue:=val;
        ESBDensityConvertor1.ValueIn:=UnitAdvEditBtn47.FloatValue;
        val:=ESBDensityConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
        UnitAdvEditBtn47.FloatValue:=val;
       end;
    3: begin
        ESBFlowConvertor1.ValueIn:=AdvSpinEdit11.FloatValue;
        val:=ESBFlowConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
        AdvSpinEdit11.FloatValue:=val;
        ESBFlowConvertor1.ValueIn:=AdvSpinEdit12.FloatValue;
        val:=ESBFlowConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
        AdvSpinEdit12.FloatValue:=val;
        ESBFlowConvertor1.ValueIn:=AdvSpinEdit13.FloatValue;
        val:=ESBFlowConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
        AdvSpinEdit13.FloatValue:=val;
       end;
    4: begin
        if ((Sender as TAdvComboBox).Text='����/�')
        or ((Sender as TAdvComboBox).Text='����/�')
        or ((Sender as TAdvComboBox).Text='���/�') then begin
          ESBPowerConvertor1.ValueIn:=AdvSpinEdit7.FloatValue;
          val:=ESBPowerConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
          AdvSpinEdit7.FloatValue:=val;
          ESBPowerConvertor1.ValueIn:=AdvSpinEdit8.FloatValue;
          val:=ESBPowerConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
          AdvSpinEdit8.FloatValue:=val;
          ESBPowerConvertor1.ValueIn:=AdvSpinEdit9.FloatValue;
          val:=ESBPowerConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
          AdvSpinEdit9.FloatValue:=val;
        end else begin
          ESBFlowMassConvertor1.ValueIn:=AdvSpinEdit7.FloatValue;
          val:=ESBFlowMassConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
          AdvSpinEdit7.FloatValue:=val;
          ESBFlowMassConvertor1.ValueIn:=AdvSpinEdit8.FloatValue;
          val:=ESBFlowMassConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
          AdvSpinEdit8.FloatValue:=val;
          ESBFlowMassConvertor1.ValueIn:=AdvSpinEdit9.FloatValue;
          val:=ESBFlowMassConvertor1.ValueOut*UnitScaleIn*UnitScaleOut;
          AdvSpinEdit9.FloatValue:=val;
        end;
       end;
  end;
  OldUnit:=NewUnit;
end;

procedure TForm1.AdvComboBox9Enter(Sender: TObject);
begin
  OldUnit:=(Sender as TAdvComboBox).Text;
end;

procedure TForm1.Page4Add2TableButtonClick(Sender: TObject);
var n: ShortInt;
begin
  cxGrid11TableView1.BeginUpdate;
  n:=cxGrid11TableView1.DataController.AppendRecord;
  cxGrid11TableView1.DataController.SetValue(n,cxGrid11TableView1.FindItemByTag(0).index,cxGrid11TableView1.DataController.RecordCount);
  cxGrid11TableView1.DataController.SetValue(n,cxGrid11TableView1.FindItemByTag(1).index,UnitAdvEditBtn9.Text);
  cxGrid11TableView1.DataController.SetValue(n,cxGrid11TableView1.FindItemByTag(2).index,UnitAdvEditBtn17.Text);
  cxGrid11TableView1.DataController.SetValue(n,cxGrid11TableView1.FindItemByTag(3).index,UnitAdvEditBtn14.Text);
  cxGrid11TableView1.DataController.SetValue(n,cxGrid11TableView1.FindItemByTag(4).index,UnitAdvEditBtn15.Text);
  cxGrid11TableView1.EndUpdate;
end;

procedure TForm1.UnitAdvEditBtn36UnitChanged(Sender: TObject;
  NewUnit: String);
begin
  if (NewUnit='���') or (NewUnit='��') then cxSpinButton1.Properties.Increment:=50
  else if (NewUnit='���') or (NewUnit='���') or (NewUnit='���') or (NewUnit='���/��2') then cxSpinButton1.Properties.Increment:=0.1
  else cxSpinButton1.Properties.Increment:=1;
  cxSpinButton1.Properties.LargeIncrement:=cxSpinButton1.Properties.Increment*10;
  UnitAdvEditBtn1UnitChanged(Sender,NewUnit);
end;

procedure TForm1.CheckBox8Click(Sender: TObject);
begin
  if CheckBox8.Checked then begin // ���������� ���
    UnitAdvEditBtn50.EditorEnabled:=False;
    UnitAdvEditBtn50.Color:=clSilver;
    UnitAdvEditBtn50.ReadOnly:=True;
  end else begin
    UnitAdvEditBtn50.EditorEnabled:=True;
    UnitAdvEditBtn50.Color:=clWindow;
    UnitAdvEditBtn50.ReadOnly:=False;
  end;
  AirSteamFlowRangeButtonClick(Sender);
end;

procedure TForm1.AdvComboBox27Change(Sender: TObject);
begin
  AirSteamFlowRangeButtonClick(Sender);
end;

procedure TForm1.UnitAdvEditBtn55MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button=mbRight) and (ssShift in Shift) then GroupBox1.Visible:=not(GroupBox1.Visible);
end;

procedure TForm1.AdvComboBox25Change(Sender: TObject);
var i: ShortInt;
begin
  i:=AdvComboBox25.ItemIndex;
  UnitAdvEditBtn62.FloatValue:=AirSteam_FlowParams[i,1]; // K1' ����.
  UnitAdvEditBtn63.FloatValue:=AirSteam_FlowParams[i,2]; // K2
  UnitAdvEditBtn64.FloatValue:=AirSteam_FlowParams[i,3]; // Vmin
  UnitAdvEditBtn19.FloatValue:=AirSteam_FlowParams[i,4]; // Vmax' ����.
  UnitAdvEditBtn65.FloatValue:=AirSteam_FlowParams[i,5]; // Vmin ��15
  AirSteamFlowRangeButtonClick(Sender);
end;

procedure TForm1.cxPageControl1DrawTabEx(AControl: TcxCustomTabControl;
  ATab: TcxTab; Font: TFont);
begin
  if ATab.HotTrack and not(ATab.IsMainTab) then Font.Color:=clYellow;
end;

procedure TForm1.AdvComboBox1Change(Sender: TObject);
begin
  DrawPressureLossGraph(False);
end;

procedure TForm1.DrawPressureLossGraph(allDN: Boolean);
var d,q,Qmax,dp,pl,A,Vmax,v: Real;
var i, idn, nmax, medium: Integer;
var TypeTitle: string;
var imin, imax: ShortInt;
var LS: TLineSeries;
begin
// ������ ������
  CheckBox10.Visible:=allDN;
  Chart1.Legend.Visible:=allDN;
  Chart1.BottomAxis.Logarithmic:=CheckBox10.Checked and allDN;
  SetESBConvertorsUnits(UnitAdvEditBtn22.UnitID, dirIn);
  ESBDensityConvertor1.DensityUnitsOut:=edtKgPerCuM;
  ESBDensityConvertor1.ValueIn:=UnitAdvEditBtn22.FloatValue;
  pl:=ESBDensityConvertor1.ValueOut;
  if pl>500 then medium:=0 else medium:=1;
  nmax:=10;
  Chart1.SeriesList.Clear;
  if allDN then begin
    imin:=0;
    imax:=AdvComboBox1.Items.Count-1;
  end else begin
    imin:=AdvComboBox1.ItemIndex;
    imax:=imin;
  end;
  for idn:=imin to imax do begin
    Qmax:=EV200_Qmax[medium,idn];
    d:=0; A:=0;
    TypeTitle:='';
    if AdvComboBox2.ItemIndex<>4 then d:=StrToFloat(AdvComboBox1.Items[idn]);
    case AdvComboBox2.ItemIndex of
      0: begin
           if medium=0 then Vmax:=9 else Vmax:=68;
           Qmax:=Vmax*3.1416*d*d/4*0.0036;  // ����������������
           A:=0;
         end;
      1: A:=EV200_APLoss_WF[idn]; // �
      2: A:=EV200_APLoss_WF[idn]; // �
      3: begin
           A:=EV200_APLoss_FR[idn]; // ��200 ��
           d:=EV200_FR_InnerD[idn];
           TypeTitle:='��';
           case idn of
             0: Qmax:=EV200_Qmax[medium,0]; // ��25/15
             1: Qmax:=EV200_Qmax[medium,1]; // ��32/25
             2: Qmax:=EV200_Qmax[medium,2]; // ��50/32
             3: Qmax:=EV200_Qmax[medium,4]; // ��80/50
             4: Qmax:=EV200_Qmax[medium,6]; // ��100/80
           end;
         end;
      4: begin
           A:=EV200PPD_APLoss[idn];   // ��200 ���
           Qmax:=EV200PPD_Qmax[1,idn];
           d:=EV200PPD_InnerD[idn];
         end;
      5: begin
           A:=EV205_APLoss; // ��-205
           Qmax:=EV205_Qmax[medium,idn];
         end;
    end;
    Qmax:=1.05*Qmax;

    LS:=TLineSeries.Create(Self);
    Chart1.AddSeries(LS);
    LS.LinePen.Width:=2;
    LS.Title:=TypeTitle+AdvComboBox1.Items[idn];
    LS.Marks.Visible:=allDN;
    LS.Marks.Style:=smsLegend;
    for i:=1 to nmax do
    begin
      q:=Qmax*i/nmax;
      if AdvComboBox2.ItemIndex=0 then begin
        v:=q*4/3.1416/d/d/0.0036;   // ��1200
        dp:=3*0.5*pl*v*v/1000;
      end else dp:=A*pl*q*q/(d*d*d*d); //��200-205
      if i=nmax then LS.AddXY(q,dp,AdvComboBox1.Items[idn])
      else LS.AddXY(q,dp);
    end;
  end;
end;

procedure TForm1.CheckBox10Click(Sender: TObject);
begin
  DrawPressureLossGraph(True);
end;

procedure TForm1.CheckBox11Click(Sender: TObject);
begin
  Chart2.Visible:=CheckBox11.Checked;
  if Chart2.Visible then AdvComboBox18Change(Sender);
end;

procedure TForm1.AdvComboBox19KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if not((ord(key)=13) and (ssShift in Shift)) then exit;
  CheckBox12.Checked:=False;
  if AdvComboBox19.ItemIndex=0 then begin // ��������
    AdvSpinEdit1.FloatValue:=1;
    AdvSpinEdit2.FloatValue:=2;
    AdvSpinEdit3.FloatValue:=3;
    AdvComboBox9.ItemIndex:=0;
    AdvSpinEdit4.FloatValue:=30;
    AdvSpinEdit5.FloatValue:=40;
    AdvSpinEdit6.FloatValue:=50;
    AdvComboBox10.ItemIndex:=0;
    AdvSpinEdit11.FloatValue:=3;
    AdvSpinEdit12.FloatValue:=20;
    AdvSpinEdit13.FloatValue:=50;
    AdvComboBox20.ItemIndex:=0;
  end;

  if AdvComboBox19.ItemIndex=1 then begin // ���
    AdvSpinEdit1.FloatValue:=8;
    AdvSpinEdit2.FloatValue:=10;
    AdvSpinEdit3.FloatValue:=12;
    AdvComboBox9.ItemIndex:=0;
    AdvSpinEdit4.FloatValue:=30;
    AdvSpinEdit5.FloatValue:=40;
    AdvSpinEdit6.FloatValue:=50;
    AdvComboBox10.ItemIndex:=0;
    AdvSpinEdit7.FloatValue:=1000;
    AdvSpinEdit8.FloatValue:=2000;
    AdvSpinEdit9.FloatValue:=12000;
    AdvComboBox11.ItemIndex:=0;
  end;

  if AdvComboBox19.ItemIndex=2 then begin // ���
    AdvSpinEdit1.FloatValue:=4;
    AdvSpinEdit2.FloatValue:=5;
    AdvSpinEdit3.FloatValue:=8;
    AdvComboBox9.ItemIndex:=0;
    AdvSpinEdit7.FloatValue:=1;
    AdvSpinEdit8.FloatValue:=2;
    AdvSpinEdit9.FloatValue:=10;
    AdvComboBox11.ItemIndex:=0;
    CheckBox5.Checked:=False;
  end;
end;

procedure TForm1.N4Click(Sender: TObject);
begin
  if cxPageControl1.ActivePageIndex=5 then begin // ������ ��������
    SaveDialog2.FileName:='������ ������ ��������.wmf';
    if SaveDialog2.Execute then Chart1.SaveToMetafile(SaveDialog2.FileName);
  end;
  if cxPageControl1.ActivePageIndex=11 then begin // ������ ��������� ��������
    SaveDialog2.FileName:='������ ��������� ��������.wmf';
    if SaveDialog2.Execute then Chart2.SaveToMetafile(SaveDialog2.FileName);
  end;
end;

procedure TForm1.AdvGlowButton1Click(Sender: TObject);
var i, n: ShortInt;
var Kf,Qmax,Fmax,t: Real;
begin
  cxGrid9TableView1.BeginUpdate;
  cxGrid9TableView1.DataController.SetRecordCount(0);
  for i:=0 to AdvComboBox3.Items.Count-1 do begin
    AdvComboBox3.ItemIndex:=i;
    Kf:=EV200_Kf[AdvComboBox8.ItemIndex, i];
    Qmax:=EV200_Qmax[AdvComboBox8.ItemIndex, i];
    Fmax:=Qmax/3.6/Kf;
    Fmax:=RoundTo(Fmax,-2);
    t:=RoundTo(1024/Fmax,-1);
    n:=cxGrid9TableView1.DataController.AppendRecord;
    cxGrid9TableView1.DataController.SetValue(n,cxGrid9TableView1.FindItemByTag(0).index,cxGrid9TableView1.DataController.RecordCount);
    cxGrid9TableView1.DataController.SetValue(n,cxGrid9TableView1.FindItemByTag(1).index,AdvComboBox3.Text);
    cxGrid9TableView1.DataController.SetValue(n,cxGrid9TableView1.FindItemByTag(2).index,Kf);
    cxGrid9TableView1.DataController.SetValue(n,cxGrid9TableView1.FindItemByTag(3).index,Fmax);
    cxGrid9TableView1.DataController.SetValue(n,cxGrid9TableView1.FindItemByTag(4).index,Qmax);
    cxGrid9TableView1.DataController.SetValue(n,cxGrid9TableView1.FindItemByTag(5).index,t);
  end;
  cxGrid9TableView1.EndUpdate;
end;

procedure TForm1.DNFlowTableButtonClick(Sender: TObject);
var i,n: Integer;
//var p,t: Real;
begin
  cxGrid8TableView1.BeginUpdate;
  cxGrid8TableView1.DataController.SetRecordCount(0);
//  SetESBConvertorsUnits(UnitAdvEditBtn50.UnitID, dirIn);
//  ESBTempConvertor1.TempUnitsOut:=ettKelvin;
//  ESBTempConvertor1.ValueIn:=UnitAdvEditBtn50.FloatValue;
//  t:=ESBTempConvertor1.ValueOut;
  for i:=0 to AdvComboBox18.Items.Count-1 do begin
    AdvComboBox18.ItemIndex:=i;
    AdvComboBox18Change(Sender);
    n:=cxGrid8TableView1.DataController.AppendRecord;
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(0).index,cxGrid8TableView1.DataController.RecordCount);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(1).index,StrToFloat(AdvComboBox18.Text));
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(2).index,UnitAdvEditBtn36.Text);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(3).index,UnitAdvEditBtn57.Text);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(4).index,UnitAdvEditBtn44.Text);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(5).index,UnitAdvEditBtn48.Text);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(6).index,UnitAdvEditBtn51.Text);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(7).index,UnitAdvEditBtn52.Text);
    cxGrid8TableView1.DataController.SetValue(n,cxGrid8TableView1.FindItemByTag(8).index,'1:'+FloatToStr(Round(UnitAdvEditBtn48.FloatValue/UnitAdvEditBtn44.FloatValue)));
  end;
  cxGrid8TableView1.EndUpdate;
end;

end.
