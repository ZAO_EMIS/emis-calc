"use strict";
angular.module("emis-calc").controller("pressureLosesCntr",["$scope", "ServerService","ev200ConstService",'highchartsNG', function($scope, ServerService, ev200ConstService, highchartsNG) {

	$scope.rho = {userValue : 1000, userMu : "кг/м3"};	
	$scope.typeDevice = '0';
	$scope.Q = 0;
	
	$scope.pressureMU = "кПа";
	
	$scope.calcPressLoses = calcPressLoses;
	
	$scope.chartConfig = {

			  options: {
			      //This is the Main Highcharts chart config. Any Highchart options are valid here.
			      //will be overriden by values specified below.
			      chart: {
			          type: 'spline'
			      },
			      tooltip: {
			          style: {
			              padding: 10,
			              fontWeight: 'bold'
			          }
			      }
			  },
			  //The below properties are watched separately for changes.

			  //Series object (optional) - a list of series using normal Highcharts series options.
			  series: [{
			    
			  	}],
			  //Title configuration (optional)
			  title: {
			     text: 'Потери давления на расходомере'
			  },
			  //Boolean to control showing loading status on chart (optional)
			  //Could be a string if you want to show specific loading text.
			  loading: false,
			  //Configuration for the xAxis (optional). Currently only one x axis can be dynamically controlled.
			  //properties currentMin and currentMax provided 2-way binding to the chart's maximum and minimum
			  xAxis: {
				  type: 'logarithmic',
				  minorTickInterval: 1,
				  title: {text: 'Расходы Q, м3/ч'}
			  },
			  yAxis: {
				  title:{text: 'Потери давления dP, кПа'}
			  },
			  //Whether to use Highstocks instead of Highcharts (optional). Defaults to false.
			  useHighStocks: false,
			  //size (optional) if left out the chart will default to size of the div or something sensible.			  
			  //function (optional)
			  func: function (chart) {
			   //setup some logic for the chart
			  }
			};
	ServerService.getMeasurmentUnits(function(data){		
		$scope.measurmentUnits = data;
		var mu = data;
		$scope.densityMU = mu.density.standart;
		$scope.QMU = mu.volFlow.standart;
		changeDevType();
	});

	//changeDevType();				   
	
	$scope.changeDevType = changeDevType;
	
	function changeDevType(){
		switch (parseInt($scope.typeDevice)) {
			case 0: 
				$scope.duList = ev200ConstService.constants.EV1200_DN;
				break;
			case 1:
				$scope.duList = ev200ConstService.constants.EV200_DN;
				break;
			case 2:
				$scope.duList = ev200ConstService.constants.EV200_DN;
				break;
			case 3:
				$scope.duList = ev200ConstService.constants.EV200_FRDN;
				break;
			case 4:
				$scope.duList = ev200ConstService.constants.EV200PPD_DN;
				break;
			case 5:
				$scope.duList = ev200ConstService.constants.EV205_DN;
				break;
		}
		$scope.du = $scope.duList[0].toString();
		drawGraph(false);
	}
	function calcPressLoses(){
		var rhoSt = $scope.rho.getStandartValue();
		var q = ServerService.convertToStandart("volFlow",$scope.QMU,$scope.Q);
		var duIndex = null;
		for(var i = 0 ; i < $scope.duList.length; i++){
			if ($scope.du == $scope.duList[i]) {
				duIndex = i;
				break;
			}
		}
		var d; var A;
		
		if($scope.typeDevice == 4) {
			d = ev200ConstService.constants.EV200PPD_InnerD[duIndex];
			A = ev200ConstService.constants.EV200PPD_APLoss[duIndex];
			
		}
		
		else if($scope.typeDevice == 3) {
			d = ev200ConstService.constants.EV200_FR_InnerD[duIndex];
			A = ev200ConstService.constants.EV200_APLoss_FR[duIndex];
		}
		else if($scope.typeDevice == 2){
			d = $scope.du;
			A = ev200ConstService.constants.EV200_APLoss_FR[duIndex];
		}
		else if($scope.typeDevice == 1){
			d = $scope.du;
			A = ev200ConstService.constants.EV200_APLoss_WF[duIndex];
		}
		
		else d = $scope.du;
		
		var v = q * 4 / 3.1416 / d /d / 0.0036;
		var dp = ($scope.typeDevice == 0) ? 3 * 0.5 * rhoSt * v * v / 1000 : A * rhoSt * q * q / (d * d * d *d);
		
		$scope.pressure = (ServerService.convertTo("pressure", "кПа", dp , $scope.pressureMU)).toFixed(DECIMALS);
	}
	
	function drawGraph(isOneDu){
		var rho = ($scope.rho.getStandartValue != typeof "function") ? 1000 : $scope.rho.getStandartValue();
		var medium = (rho > 500) ? 0 : 1;
		
		var points = {};
		for(var i = 0; i < $scope.duList.length ; i++){			
			if((isOneDu) && ($scope.du == $scope.duList[i])){
				points = calculatePointsForGraph(i,medium,rho);
				break;
			}
			else if(!isOneDu){
				points[$scope.duList[i]] = calculatePointsForGraph(i,medium, rho);
			}
			else{
				continue;
			}
		}
		$scope.chartConfig.series = new Array();
		for (var key in points){
			$scope.chartConfig.series.push({data: points[key], name: key,pointStart : 0.001});
		}
		
	}
	
	function calculatePointsForGraph(indexDU, medium, rho){
		var Qmax = ev200ConstService.constants.EV200_Qmax[medium][indexDU];
		var A = 0;
		var d = (+$scope.typeDevice != 4) ? $scope.duList[indexDU] : 0;
		
		switch (+$scope.typeDevice){
			case 0:// струевыпрямитель
				var Vmax = (medium == 0) ? 9 : 68;
				Qmax = Vmax * 3.1416 * d * d/4 * 0.0036;
				A = 0;
				break;
			case 1: // C
				A = ev200ConstService.constants.EV200_APLoss_WF[indexDU];
				break;
			case 2: // Ф
				A = ev200ConstService.constants.EV200_APLoss_WF[indexDU];
				break;
			case 3: 
				A = ev200ConstService.constants.EV200_APLoss_FR[indexDU];
				d = ev200ConstService.constants.EV200_FR_InnerD[indexDU];
				switch(indexDU){
					case 0: 
						Qmax = EV200_Qmax[medium][0];
						break;
					case 1: 
						Qmax = EV200_Qmax[medium][1];
						break;
					case 2: 
						Qmax = EV200_Qmax[medium][2];
						break;
					case 3: 
						Qmax = EV200_Qmax[medium][4];
						break;
					case 4: 
						Qmax = EV200_Qmax[medium][6];
						break;
				};
				break;
			case 4: 
				A = ev200ConstService.constants.EV200PPD_APLoss[indexDU];
				Qmax = ev200ConstService.constants.EV200PPD_Qmax[1][indexDU];
				d = ev200ConstService.constants.EV200PPD_InnerD[indexDU];
				break;
			case 5:
				A = ev200ConstService.constants.EV205_APLoss;
				Qmax = ev200ConstService.constants.EV205_Qmax[medium][indexDU];
				break;
		}
		Qmax *= 1.05;
		var pointCount = 10;
		var arrPointsForDU = [];
		for (var i = 1; i <= pointCount ; i++){
			var q = Qmax * i/pointCount;
			var onePoint = new Array();
			onePoint[0] = q;			
			if (+$scope.typeDevice == 0){
				var v = q * 4 / 3.1416 / d / d / 0.0036;
				onePoint[1] = 3 * 0.5 * rho * v * v / 1000;
			}
			
			else onePoint[1] = A * rho *q * q / (d * d * d * d);
			arrPointsForDU.push(onePoint);
		}
		return arrPointsForDU;
	}	
	
}])