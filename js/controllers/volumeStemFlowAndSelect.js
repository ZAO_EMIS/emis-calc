"use strict";
angular.module("emis-calc").controller("volumeStemFlowAndSelect",["$q","$scope","$http", "ev200ConstService",  "ServerService", function($q,$scope, $http, ev200ConstService ,ServerService) {
	$scope.isCalcforP = true;
	$scope.isCalcforT = true;

	$scope.Pmin = $scope.Pnom = $scope.Pmax = $scope.Tnom = $scope.Tmin = $scope.Tmax = $scope.QvMin = $scope.QvNom = $scope.QvMax = $scope.QvnNom = $scope.QvnMin = $scope.QvnMax = $scope.QmMin = $scope.QmNom = $scope.QmMax = 0;
	$scope.densityStand = 1.2;
	ServerService.getMeasurmentUnits(function(data){		
		$scope.measurmentUnits = data;
		var mu = data;
		$scope.temperatureUI = mu.temperature.standart; 
		$scope.pressureUI = mu.pressure.standart;
		$scope.massFlowUI = mu.massFlow.standart;
		$scope.densityUI = mu.density.standart;
		$scope.volFlowUI = mu.volFlow.standart;
	});
	$scope.matchDevice = Array();
	$scope.calcQvButtonClick = calculateQv;
	$scope.selectBtnClick = selectBtnClick;
	
	
	function selectBtnClick(){
		$scope.matchDevice = [];
		var Qmin = (+$scope.QvMIn == 0) ? Math.min.apply(null, [$scope.QvNom, $scope.QvMax]) : Math.min.apply(null, [$scope.QvMin, $scope.QvNom, $scope.QvMax]);
		var Qmax = Math.max.apply(null, [$scope.QvMin, $scope.QvNom, $scope.QvMax]);
		var medium = ($scope.medium == "liquid") ? 0 : 1;
		
		var Pmin = Math.min.apply(null, [$scope.Pmin, $scope.Pnom, $scope.Pmax]);
		var Pmax = Math.max.apply(null,[$scope.Pmin, $scope.Pnom, $scope.Pmax]);
		Pmin =  ServerService.convertToStandart("pressure", $scope.pressureUI, Pmin) + 101325;
		Pmax =  ServerService.convertToStandart("pressure", $scope.pressureUI, Pmax) + 101325;
		
		
		var Tmin = null; var Tmax = null;
		
		Tmin = Math.min.apply(null, [$scope.Tmin, $scope.Tnom, $scope.Tmax]);
		Tmax = Math.max.apply(null,[$scope.Tmin, $scope.Tnom, $scope.Tmax]);
		Tmin = ServerService.convertToStandart("temperature", $scope.temperatureUI,Tmin);
		Tmax = ServerService.convertToStandart("temperature", $scope.temperatureUI,Tmax);
		
		var tn;
		if(Tmax > 593) tn = 3;
		else if(Tmax > 523) tn = 2;
		else if(Tmax > 373) tn = 1;
		else tn = 0;			
		
		var densRequestMax = null;
		var densRequestMin = null;
		if ($scope.isCalcQvP && $scope.medium == 'steam'){
			
			if ($scope.isCalcforP ^ $scope.isCalcforT){				
				Tmin = null;
				Tmax = null;
			}
			densRequestMax = inquireDenFromServer(Pmax, Tmax);
			densRequestMin = inquireDenFromServer(Pmin, Tmin);
			
			$q.all([densRequestMax, densRequestMin]).then(function(values){
				var resultQvsMax = getFlows(Pmax,Tmax,medium,$scope.isCalcQvP,+values[0].data.response, tn);
				var resultQvsMin = getFlows(Pmin,Tmin,medium,$scope.isCalcQvP,+values[1].data.response, tn);				
				var matchEV200 = getMatchDevices(Qmin,Qmax,resultQvsMax,resultQvsMin);
				$scope.matchDevice = $scope.matchDevice.concat(matchEV200);
			});			
		}
		else if($scope.medium == 'gas'){ 

			var tmpDens = $scope.densityStand * Pmin / 101325 * 293 / Tmin;
			densRequestMin = $q.when(tmpDens);
			tmpDens = $scope.densityStand * Pmax / 101325 * 293 / Tmax;
			densRequestMax = $q.when(tmpDens);
			$q.all([densRequestMax, densRequestMin]).then(function(values){
				var resultQvsMax = getFlows(Pmax,Tmax,medium,$scope.isCalcQvP,+values[0],tn);
				var resultQvsMin = getFlows(Pmin,Tmin,medium,$scope.isCalcQvP,+values[1], tn);		
				var matchEV200 = getMatchDevices(Qmin,Qmax,resultQvsMax,resultQvsMin);
				$scope.matchDevice = $scope.matchDevice.concat(matchEV200);
			});		
		}
		else{			
			
			for(var i = 0; i < ev200ConstService.constants.EV200_DN.length; i++){
				if((Qmax <= ev200ConstService.constants.EV200_Qmax[medium][i]) && (ev200ConstService.constants.EV200_QminExt[tn][medium][i] <= Qmin)){
					$scope.matchDevice.push(ev200ConstService.constants.EV200_DN[i].toString() + "-ЭВ200");
				}
			}
		}
		
		if((tn < 2) && (Pmax <= 4101325)){
			for(var i = 0; i < ev200ConstService.constants.EV205_DN.length; i++){
				if((ev200ConstService.constants.EV205_Qmax[medium][i] >= Qmax) && (ev200ConstService.constants.EV205_QminExt[medium][i] < Qmin)){
					$scope.matchDevice.push(ev200ConstService.constants.EV205_DN[i].toString + "-ЭВ205");
				}
			}
		}
		

	}
	
	function getMatchDevices(Qmin,Qmax,resultQvsMax,resultQvsMin){
		var matchDevices = Array();
		for(var i = 0 ; i < resultQvsMax.length; i++){
			if((Qmax <= resultQvsMax[i].Qmax) && (Qmin >= resultQvsMin[i].Qmin)){
				var dev = ev200ConstService.constants.EV200_DN[i].toString() + "-ЭВ200";
				/*var obj =  { };
				obj[keyK] =  resultQvsMin[i].Qmin + '-' + resultQvsMax[i].Qmax;*/
				matchDevices.push(dev);
				
			} 
		}
		return matchDevices;
	}
	
	function inquireDenFromServer(pressure, temperature){
		var tempPromise =  (temperature == null) ? ServerService.getDataFromDll('wspTSP', [pressure], function(data){}) : temperature;		
		if(temperature < 100) {
			alert("Температура должна быть больше 100 К");
			return;
		}
		return $q.when(tempPromise).then(function(value){
				var temperature = (typeof(value) === 'number') ? value : value.data.response;
				return ServerService.getDataFromDll('wspDPT',[pressure, temperature], function(data){
			});
		});
		//return tempPromise;
	}
//=============GETFLOWS=======================================================	
	function getFlows(p,t,medium, isQvDependP,dens, tn){
		var pressureIndex = -1; var Vmin = -1;		
		var temperatureIndex = (isQvDependP) ? tn : 0;
		
		if(isQvDependP == false) pressureIndex = 0;
		else if(+p > 6.4e6) pressureIndex = 2;
		else if(+p > 2.5e6) pressureIndex = 1;
		else pressureIndex = 0;
				
				
		var duArr = ev200ConstService.constants.EV200_DN;
		
		var resultQ = Array();
		for (var i = 0; i < duArr.length; i++){			
			
			if(isQvDependP && medium != 0){
				resultQ[i] = calculateQvForPressure(i);
			}
			else{
				var qMaxP = ev200ConstService.constants.EV200_Qmax[medium][i];
				var qMinP = ev200ConstService.constants.EV200_QminExt[tn][medium][i];
				resultQ[i] = {"Qmax" : qMaxP, "Qmin" : qMinP};
			}
		}
		return resultQ;
		
// ====================FUNCTIONS ==================================		
		function calculateQvForPressure(duIndex){
			var densAir = 1.2;
			var innerD = (medium == 3 && pressureIndex == 0 && duArr[duIndex] > 100) ? ev200ConstService.constants.EV200_InnerD[2][duIndex] : ev200ConstService.constants.EV200_InnerD[pressureIndex][duIndex];
			var S = 3.1416 * innerD * innerD / 4;
			var qMin = 0;
			
			switch (temperatureIndex){
				case 0 : 
					qMin = ev200ConstService.constants.EV200_Qmin[0][1][duIndex];
					Vmin=(innerD <= 15) ? 2 : 1.5;
					break;
				case 1: 
					qMin = ev200ConstService.constants.EV200_QminExt[0][1][duIndex];
					Vmin = (innerD <= 15) ? 2 : 1.3;
					break;
				case 2: 
					qMin = ev200ConstService.constants.EV200_Qmin[1][1][duIndex];
					Vmin =  (innerD <= 15) ? 3 : 2;
					break;
				case 3: 
					qMin = ev200ConstService.constants.EV200_Qmin[3][1][duIndex];
					Vmin =  (innerD <= 15) ? 4 : 3;
					break;
			}
			var qMax = ev200ConstService.constants.EV200_Qmax[1][duIndex];
			var k1 = Math.pow(qMin / S / 0.0036, 2) * densAir;
			var Vmax = qMax / S / 0.0036;
			var k2 = 120000;					
			qMax = Math.min.apply(null,[Math.sqrt(k2/dens) , Vmax]) * S * 0.0036;
			qMin = Math.max.apply(null,[Math.sqrt(k1/dens) , Vmin]) * S * 0.0036;							
			return {"Qmin" : qMin, "Qmax" : qMax};				
		
		}
	}
	
//================END============================	
	
	function calculateQv(){
		var Pmin = ServerService.convertToStandart("pressure", $scope.pressureUI, $scope.Pmin) + 101325;
		var Pnom = ServerService.convertToStandart("pressure", $scope.pressureUI, $scope.Pnom) + 101325;
		var Pmax = ServerService.convertToStandart("pressure", $scope.pressureUI, $scope.Pmax) + 101325;
		
		var Tmin = ServerService.convertToStandart("temperature", $scope.temperatureUI,$scope.Tmin);
		var Tnom = ServerService.convertToStandart("temperature", $scope.temperatureUI,$scope.Tnom);
		var Tmax = ServerService.convertToStandart("temperature", $scope.temperatureUI,$scope.Tmax);
		if((Tmin < 100) | (Tnom <100) | (Tmax < 100)){
			alert("Температура должна быть больше 100 К");
			return;
		}
		if($scope.medium == "gas"){
			$scope.QvMin = ($scope.QvnMin * Tmin / 273.15 * 101325 / Pmin).toFixed(DECIMALS);
			$scope.QvNom = ($scope.QvnNom * Tnom / 273.15 * 101325 / Pnom).toFixed(DECIMALS);
			$scope.QvMax = ($scope.QvnMax * Tmax / 273.15 * 101325 / Pmax).toFixed(DECIMALS);
			
			var dens0 = $scope.densityStand;
			$scope.DensMin = (dens0 * Pmin / 101325 * 293.15 / Tmin).toFixed(DECIMALS);
			$scope.DensNom = (dens0 * Pnom / 101325 * 293.15 / Tnom).toFixed(DECIMALS);
			$scope.DensMax = (dens0 * Pmax / 101325 * 293.15 / Tmax).toFixed(DECIMALS);			
		}
		else if($scope.medium == 'steam'){
			var promises = [];
			var Qmmin =  ServerService.convertToStandart("massFlow", $scope.massFlowUI, $scope.QmMin);
			var Qmnom =  ServerService.convertToStandart("massFlow", $scope.massFlowUI, $scope.QmNom);
			var Qmmax =  ServerService.convertToStandart("massFlow", $scope.massFlowUI, $scope.QmMax);
			if($scope.isCalcforP && $scope.isCalcforT){ // Расчет по давлению и температуре
				
			 	promises[promises.length] =  ServerService.getDataFromDll('wspDPT', [Pmin,Tmin], function(data){
					$scope.DensMin = (+data.response).toFixed(DECIMALS);
				});
				
			 	promises[promises.length] = ServerService.getDataFromDll('wspPHASESTATEPT', [Pmin,Tmin], function(data){
					$scope.SubsMin = data.response;
				});
				
			 	promises[promises.length] = ServerService.getDataFromDll('wspDPT', [Pnom,Tnom], function(data){
					$scope.DensNom = (+data.response).toFixed(DECIMALS);
				});
				
			 	promises[promises.length] = ServerService.getDataFromDll('wspPHASESTATEPT', Array(Pnom,Tnom), function(data){
					$scope.SubsNom = data.response;
				});
				
			 	promises[promises.length] = ServerService.getDataFromDll('wspDPT', Array(Pmax,Tmax), function(data){
					$scope.DensMax = (+data.response).toFixed(DECIMALS);
				});
				
			 	promises[promises.length] = ServerService.getDataFromDll('wspPHASESTATEPT', Array(Pmax,Tmax), function(data){
					$scope.SubsMax = data.response;
				});
				
				
				} else if($scope.isCalcforP){ // Расчет по давлению
				promises[promises.length] = ServerService.getDataFromDll('wspTSP', [Pmin], function(data){
					if(!isNaN(data.response)) {
						$scope.Tmin = ServerService.convertTo("temperature", "K", data.response, $scope.temperatureUI).toFixed(DECIMALS);
						promises[promises.length] = ServerService.getDataFromDll('wspDSST', [data.response], function(data){
							var value = (!isNaN(data.response)) ? (+data.response).toFixed(DECIMALS) : data.response;
							$scope.DensMin = value;
						});
					}
					else {
						$scope.Tmin = "Ошибка";
						$scope.DensMin = "Ошибка";
					}
					
				});
				
				promises[promises.length] = ServerService.getDataFromDll('wspTSP', [Pnom], function(data){
					if(!isNaN(data.response)) {
						$scope.Tnom = ServerService.convertTo("temperature", "K", data.response, $scope.temperatureUI).toFixed(DECIMALS);
						promises[promises.length] = ServerService.getDataFromDll('wspDSST', [data.response], function(data){
							var value = (!isNaN(data.response)) ? (+data.response).toFixed(DECIMALS) : data.response;
							$scope.DensNom = value;
						});
					}
					else {
						$scope.Tnom = "Ошибка";
						$scope.DensNom = "Ошибка";
					}
				});
				

				promises[promises.length] = ServerService.getDataFromDll('wspTSP', [Pmax], function(data){
					if(!isNaN(data.response)) {
						$scope.Tmax = ServerService.convertTo("temperature", "K", data.response, $scope.temperatureUI);
						promises[promises.length] = ServerService.getDataFromDll('wspDSST', [data.response], function(data){
							var value = (!isNaN(data.response)) ? (+data.response).toFixed(DECIMALS) : data.response;
							$scope.DensMax = value;
						});
					}
					else {
						$scope.Tmax = "Ошибка";
						$scope.DensMax = "Ошибка";
					}
				});
				$scope.SubsMin = "Насыщеный пар";
				$scope.SubsNom = "Насыщеный пар";
				$scope.SubsMax = "Насыщеный пар";				
			} 
			else if($scope.isCalcforT){// расчет по темературе
				promises[promises.length] = ServerService.getDataFromDll('wspDSST', [Tmin], function(data){
					if(!isNaN(data.response)){
						$scope.DensMin = +data.response.toFixed(DECIMALS);
						promises[promises.length] = ServerService.getDataFromDll("wspPST", [data.response], function(data){
							var value = (!isNaN(data.response)) ? (+data.response).toFixed(DECIMALS) - 101325 : data.response;
							$scope.Pmin = ServerService.convertTo("pressure", "Па", value, $scope.pressureUI).toFixed(DECIMALS);
						})
					}
				});
				promises[promises.length] = ServerService.getDataFromDll('wspDSST', [Tnom], function(data){
					if(!isNaN(data.response)){
						$scope.DensNom = +data.response.toFixed(DECIMALS);
						promises[promises.length] = ServerService.getDataFromDll("wspPST", [data.response], function(data){
							var value = (!isNaN(data.response)) ? (+data.response).toFixed(DECIMALS)  - 101325 : data.response;
							$scope.Pnom =  ServerService.convertTo("pressure", "Па", value, $scope.pressureUI).toFixed(DECIMALS);;
						})
					}
				});
				promises[promises.length] = ServerService.getDataFromDll('wspDSST', [Tmax], function(data){
					if(!isNaN(data.response)){
						$scope.DensMax = +data.response.toFixed(DECIMALS);
						promises[promises.length] = ServerService.getDataFromDll("wspPST", [data.response], function(data){
							var value = (!isNaN(data.response)) ? (+data.response).toFixed(DECIMALS) - 101325 : data.response;
							$scope.Pmax = ServerService.convertTo("pressure", "Па", value, $scope.pressureUI).toFixed(DECIMALS);;;
						})
					}
				});
				$scope.SubsMin = "Насыщеный пар";
				$scope.SubsNom = "Насыщеный пар";
				$scope.SubsMax = "Насыщеный пар";
			}
			$q.all(promises).then(function(){
				$scope.QvMin = (Qmmin/$scope.DensMin).toFixed(DECIMALS);
				$scope.QvNom = (Qmnom/$scope.DensNom).toFixed(DECIMALS);
				$scope.QvMax = (Qmmax/$scope.DensMax).toFixed(DECIMALS);
			});
			
			
		}
		
	}	
	//changePressure()
		
}])