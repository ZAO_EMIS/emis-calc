"use strict";
angular.module("emis-calc").controller("flowEV200205Cntr",["$scope", "ev200ConstService", function($scope, ev200ConstService) {
	$scope.typeDev = '0';	
	$scope.variant = {isReadonly : false, value : '0'};
	$scope.mediumArray = [];
	$scope.tempArray = [];
	typeDevChange();

	$scope.typeDevChange =typeDevChange; 
	function typeDevChange(){
		$scope.mediumArray = [];
		$scope.mediumArray.push('Жидкость');
		$scope.tempArray = [];
		switch(+$scope.typeDev){
			case 0:				
				$scope.tempArray.push('100','250','320','460');
				$scope.variant.isReadonly = false;
				$scope.mediumArray.push('Газ');
				break;
			case 1:
				$scope.tempArray.push('100','250');
				$scope.variant.isReadonly = true;
				$scope.mediumArray.push('Газ');
				break;
			case 2:
				$scope.tempArray.push('100');
				$scope.variant.isReadonly = true;
				break;
		}
		$scope.maxTemp = '0';
		$scope.medium = '0';
		calcTable();
	}
	
	$scope.variantChange = function(){
		var tempIndex = $scope.maxTemp;
		$scope.tempArray = [];
		$scope.tempArray.push('100');
		$scope.tempArray.push('250');
		if ($scope.variant.value < 2){
			$scope.tempArray.push('320');
			$scope.tempArray.push('460');
		}
		tempIndex = ($scope.tempArray.length-1 <= tempIndex) ? tempIndex : 0;
		$scope.maxTemp = tempIndex;
		calcTable();
	}
	
	$scope.calcTable =calcTable; 
	
	function calcTable(){
		var columnNames = ['№', 'ДУ'];
		var deviceCount = 0;
		switch (+$scope.typeDev){
			case 0:
				columnNames.push('Внут D',"Qmin","Qmax","Vmin","Vmax","Дин. диап.", "Цена имп.");
				deviceCount = ev200ConstService.constants.EV200_DN.length;
				break;
			case 1:
				columnNames.push("Qmin","Qmax","Vmin","Vmax","Дин. диап.", "Цена имп.");
				deviceCount = ev200ConstService.constants.EV205_DN.length;
				break;
			case 2:
				columnNames.push('Внут D',"Qmin экс.","Qmax экс.","Qmin полн.", "Qmax полн.","Vmin","Vmax","Дин. диап.", "Цена имп.");
				deviceCount = ev200ConstService.constants.EV200PPD_DN.length;
				break;
		}
		
		if($scope.variant.value == 2) deviceCount = 8;
		var startIndex = ($scope.maxTemp == 3) ? 3 : 0;
		var tableValues = [];
		var consts = ev200ConstService.constants;
		for(var i = startIndex, n = 1; i < deviceCount ; i++, n++)
		{
			var tableRow = [];
			switch(+$scope.typeDev){
				case 0:
					var sdn = consts.EV200_DN[i];
					var qmin = consts.EV200_Qmin[+$scope.maxTemp][+$scope.medium][i];
					var qminExt = consts.EV200_QminExt[+$scope.maxTemp][+$scope.medium][i];
					var qmax = consts.EV200_Qmax[+$scope.medium][i];
					var pW = consts.EV200_PulseWeight[+$scope.medium][i];
					var innerD = consts.EV200_InnerD[+$scope.variant.value][i];
					if (($scope.maxTemp == 3) && ($scope.variant.value == 0) && (consts.EV200_DN[i] > 100))	innerD = consts.EV200_InnerD[2][i];
					var vmin = (qmin * 4/ 3.1416 / innerD / innerD / 0.0036).toFixed(DECIMALS);
					var vminExt = (qminExt * 4 / 3.1416 / innerD / innerD / 0.0036).toFixed(DECIMALS);
					var vmax = (qmax * 4 / 3.1416 / innerD / innerD / 0.0036).toFixed(DECIMALS);
					var dd = '1:' + Math.round(vmax/vmin);
					tableValues.push([n,sdn,innerD, qmin,qmax, vmin, vmax, dd, pW]);
					break;
				case 1:
					var sdn = consts.EV205_DN[i];
					var qmin = consts.EV205_Qmin[+$scope.medium][i];
					var qminExt = consts.EV205_QminExt[+$scope.maxTemp][+$scope.medium][i];
					var qmax = consts.EV205_Qmax[+$scope.medium][i];
					var pW = consts.EV205_PulseWeight[+$scope.medium];
					var innerD = consts.EV205_DN[i];
					
					var vmin = (qmin * 4/ 3.1416 / innerD / innerD / 0.0036).toFixed(DECIMALS);
					var vminExt = (qminExt * 4 / 3.1416 / innerD / innerD / 0.0036).toFixed(DECIMALS);
					var vmax = (qmax * 4 / 3.1416 / innerD / innerD / 0.0036).toFixed(DECIMALS);
					var dd = '1:' + Math.round(vmax/vmin);
					tableValues.push([n,sdn, qmin,qmax, vmin, vmax, dd, pW]);	
					break;
				case 2:
					var sdn = consts.EV200PPD_DN[i];
					var qmin = consts.EV200PPD_Qmin[0][i];					
					var qmax = consts.EV200PPD_Qmax[0][i];
					var qminFull = consts.EV200PPD_Qmin[1][i];
					var qmaxFull = consts.EV200PPD_Qmax[1][i];
					var pW = consts.EV200PPD_PulseWeight[i];
					var innerD = consts.EV200PPD_InnerD[i];
					
					var vmin = (qmin * 4/ 3.1416 / innerD / innerD / 0.0036).toFixed(DECIMALS);					
					var vmax = (qmax * 4 / 3.1416 / innerD / innerD / 0.0036).toFixed(DECIMALS);
					var dd = '1:' + Math.round(vmax/vmin);					
					tableValues.push([n,sdn,innerD, qmin,qmax,qminFull, qmaxFull, vmin, vmax, dd, pW]);	
					break;
			}
		}
		$scope.tableConfig = {columnNames: columnNames, rows : tableValues};
		
	}
		
}])