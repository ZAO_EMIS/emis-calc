"use strict";
angular.module("emis-calc").controller("steamWaterCntr",["$scope", "ServerService", function($scope, ServerService) {
	
	var columnNames = ["№","Давление", "Температура", "Ст. сухости", "Состояние.", "Плотность", "Дин. вязкость", "Кин. вязкость", "Энтальпия", "Тепловой поток", "Масс. расход"];
	$scope.tableConfig = {columnNames: columnNames, rows : []};
	$scope.addRow = addRow;
	function addRow(){		
		var n = $scope.tableConfig.rows.length+1; 
		$scope.tableConfig.pushRow([n,$scope.pressure, $scope.temperature,$scope.dryK,$scope.substance,$scope.density, $scope.dynVisc, $scope.cinVisc, $scope.entalphy,$scope.heatFlow,$scope.Qm]);
	}
	ServerService.getMeasurmentUnits(function(data){		
		$scope.measurmentUnits = data;
		var mu = data;
		$scope.pressureUI = mu.pressure.standart;
		$scope.pressure = 0;
		$scope.temperatureUI ="C";
		$scope.temperature = 0;
		$scope.entalphyUI = 'ккал/кг';
		$scope.cinViscUI = mu.cinViscosity.standart;
		$scope.dynViscUI = mu.dynViscosity.standart;
		$scope.heatFlowUI = mu.power.standart;
		$scope.QmUI = mu.massFlow.standart;
		$scope.QvUI = mu.volFlow.standart;
		$scope.typePress = "exc";
		$scope.isCalcForP = $scope.isCalcForT = true;
		$scope.dryK = 1;
	});
	
	
	$scope.calculateFlow = calculateFlow;
	$scope.calculateEnvirement = calculateEnvirement;
	//$scope.changeMeasUnits  = changeMeasUnits
	
	function calculateFlow(){
		var G = ServerService.convertToStandart("power", $scope.heatFlowUI, $scope.heatFlow);
		var entalphy = ServerService.convertToStandart("entalphy", $scope.entalphyUI, $scope.entalphy);
		var Qm = ($scope.entalphy == 0) ? 0 : G / entalphy;
		$scope.Qm = ServerService.convertTo("massFlow", "кг/ч", Qm, $scope.QmUI).toFixed(DECIMALS);
		$scope.Qv = ServerService.convertTo("volFlow", "м3/ч", (Qm/$scope.density), $scope.QvUI).toFixed(DECIMALS);
	};
	
	function calculateEnvirement(){
		$scope.substance = (!$scope.isCalcForP | !$scope.isCalcForT) ? "Насыщенный пар" : "Пар";
		var pressure =  ServerService.convertToStandart("pressure", $scope.pressureUI, $scope.pressure);
		pressure = ($scope.typePress == "abs") ? pressure : pressure + 101325;
		if(pressure <= 0) {
			alert("Избыточное давление должно быть больше -1 атм");
			return;
		}
		var temperature =  ServerService.convertToStandart("temperature", $scope.temperatureUI, $scope.temperature);
		var dllFuncNameDens; var dllFuncNamedv; var dllFuncNamekv ; var dllFuncNameent;// имя функций из dll
		var argsDens; var argsDynVisc; var argsCinVisc; var argsEnt; // массивы аргументов для вызова функций из dll
		/*Рачет по температуре и давлению*/
		if($scope.isCalcForP && $scope.isCalcForT){
			dllFuncNameDens = "wspDPT"; dllFuncNamedv = "wspDYNVISPT"; dllFuncNamekv = "wspKINVISPT"; dllFuncNameent = "wspHPT";
			argsDens = [pressure,temperature]; 
			if($scope.dryKOnOff) {
				argsDens.push($scope.dryK);
				dllFuncNameDens = "wspDPTX"; dllFuncNamedv = "wspDYNVISPTX"; dllFuncNamekv = "wspKINVISPTX"; dllFuncNameent = "wspHPTX";
			}
			argsDynVisc = argsDens; argsCinVisc = argsDens; argsEnt = argsDens;
			calculateParams();
		}
		// only for pressure
		else if($scope.isCalcForP){
			ServerService.getDataFromDll('wspTSP',[pressure], function(data){
				var temperature = +data.response;
				$scope.temperature = ServerService.convertTo("temperature", "K", data.response, $scope.temperatureUI).toFixed(DECIMALS);
				dllFuncNameDens = "wspDPTX"; dllFuncNamedv = "wspDYNVISPTX"; dllFuncNamekv = "wspKINVISPTX"; dllFuncNameent = "wspHPTX";				 
				 argsDens = argsDynVisc = argsCinVisc = [temperature]; 
				 argsEnt = [pressure, temperature];
				if($scope.dryKOnOff) {
					argsDens.push($scope.dryK);
					argsEnt.push($scope.dryK);
					dllFuncNameDens = "wspDSST"; dllFuncNamedv = "wspDYNVISSST"; dllFuncNamekv = "wspKINVISSST"; dllFuncNameent = "wspHPT";
				}
				calculateParams();
			});				
		}
		else if($scope.isCalcForT){
			ServerService.getDataFromDll('wspPST',[temperature], function(data){
				var pressure = +data.response;
				$scope.pressure = ServerService.convertTo("pressure", "Па", data.response - 101325, $scope.pressureUI).toFixed(DECIMALS);
				dllFuncNameDens = "wspDSST"; dllFuncNamedv = "wspDYNVISSST"; dllFuncNamekv = "wspKINVISSST"; dllFuncNameent = "wspHPT";				 
				 argsDens = argsDynVisc = argsCinVisc = [temperature]; 
				 argsEnt = [pressure, temperature];
				if($scope.dryKOnOff) {
					argsDens.push($scope.dryK);
					argsEnt.push($scope.dryK);
					dllFuncNameDens = "wspDSTX"; dllFuncNamedv = "wspDYNVISSST"; dllFuncNamekv = "wspKINVISSST"; dllFuncNameent = "wspHPTX";
				}
				calculateParams();
			});			
		};
		

		function calculateParams(){
			ServerService.getDataFromDll(dllFuncNameDens,argsDens, function(data){
				$scope.density = (!isNaN(data.response)) ? (+data.response).toFixed(DECIMALS) : data.response;
			});
			ServerService.getDataFromDll(dllFuncNamedv,argsDynVisc, function(data){
				var value = (!isNaN(data.response)) ?  ServerService.convertTo("dynViscosity", "Па*с", data.response, $scope.dynViscUI).toFixed(DECIMALS) : data.response;
				$scope.dynVisc = value;
			}); 
			ServerService.getDataFromDll(dllFuncNamekv,argsCinVisc, function(data){
				var value = (!isNaN(data.response)) ?  ServerService.convertTo("cinViscosity", "сСт", data.response, $scope.cinViscUI).toFixed(DECIMALS) : data.response;
				$scope.cinVisc = value;
			}); 
			ServerService.getDataFromDll(dllFuncNameent,argsEnt, function(data){
				var value = (!isNaN(data.response)) ?  ServerService.convertTo("entalphy", "Дж/кг", data.response, $scope.entalphyUI).toFixed(DECIMALS) : data.response;
				$scope.entalphy = value;
			});
		};
	}
	

	
	function changeMeasUnits(paramName, muType ,oldVal){
		var mu = $scope.measurmentUnits;
		var elemUIName = paramName+'UI';
		$scope[paramName] = ServerService.convertTo(muType, oldVal, $scope[paramName], $scope[elemUIName]).toFixed(DECIMALS);
	}
		
}])