"use strict";
angular.module("emis-calc").controller("freqFlowCntr",["$scope", "ev200ConstService", function($scope, ev200ConstService) {
	
	$scope.diametersArr = ev200ConstService.constants.EV200_DN;
	$scope.diameter = "0";
	$scope.medium = "0";
	$scope.kfUI = "л/имп";
	changeDiam();
	$scope.changeDiameter = changeDiam;
		
	var columnNames = ["№","Ду", "К-фактор", "Частота", "Расход.", "Время"];
	$scope.tableConfig = {columnNames: columnNames, rows : []};
	$scope.addRow = addRow;
	function addRow(){		
		var n = $scope.tableConfig.rows.length+1;
		var du = $scope.diametersArr[$scope.diameter];
		$scope.tableConfig.pushRow([n,du,$scope.kf, $scope.freq, $scope.Q, $scope.time]);
	}
	
	$scope.allDUBtnClick = function(){
		var currentDU = $scope.diameter;
		for(var duInd in $scope.diametersArr){
			$scope.diameter = duInd;
			changeDiam();
			addRow();
		}
		$scope.diameter = currentDU;
		changeDiam();
	}
	
	$scope.changeFreq = function(){
		$scope.Q = (3.6 * $scope.freq * $scope.kf).toFixed(DECIMALS);
		$scope.time = (1024/$scope.freq).toFixed(DECIMALS);
	};
	
	$scope.changeFlow = function(){
		$scope.freq = (!isNaN($scope.kf)) ? ($scope.Q/3.6/$scope.kf).toFixed(DECIMALS) : "Кф должен быть больше 0";
		$scope.time = (!isNaN($scope.kf)) ? (1024/$scope.freq).toFixed(DECIMALS) : "Ошибка";
	};
	
	function changeDiam(){
		var kaf = ev200ConstService.constants.EV200_Kf[$scope.medium][$scope.diameter];
		var Qmax = ev200ConstService.constants.EV200_Qmax[$scope.medium][$scope.diameter];
		$scope.fMax = (Qmax/3.6/kaf).toFixed(DECIMALS);
		$scope.kf = ($scope.kfUI == "имп/м3") ? 1000/kaf : kaf; 
		$scope.time = (1024/$scope.fMax).toFixed(DECIMALS);
	}
}])