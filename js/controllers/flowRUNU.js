"use strict";
angular.module("emis-calc").controller("flowRUNUCntr",["$scope", "ServerService", function($scope, ServerService) {
/*	ServerService.getMeasurmentUnits(function(data){
		
		$scope.measurmentUnits = data;
		var mu = data;		
	});*/
	$scope.overP = {userMu : 'МПа', userValue : 25};		
	$scope.temperature = {userMu : 'C', userValue : 0};
	$scope.airP = {userValue : 0.101325 , userMu : 'МПа'};	
	$scope.kC = {userValue : 1 };
	$scope.Qr = {userMu : 'м3/ч', userValue : 0};
	$scope.Qct = { userValue : 0};
	//$scope.QctMU = 'нм3/ч';
	//$scope.QrMU = 'м3/ч';
	$scope.terms = "normal";
	$scope.rho = {userValue : 1.292};
	$scope.rhoSt = {userValue : 1.292};
	
	
	$scope.butTopName = "Перевод в Р.У.";
	$scope.butBotName = "Перевод в Н.У.";
	$scope.flowName = "Расход в Н.У. Qн";
	$scope.rhoStName = "ПЛотность в Н.У. рн";
	
	$scope.standartToCurrent = standartTermsToCurrent;
	$scope.currentTermsToStandart = currentTermsToStandart;
	
	var columnNames = ["№","Давление", "Температура", "Расход Р.У.", "Расход Н.У."];
	$scope.tableConfig = {columnNames: columnNames, rows : []};
	$scope.addRow = addRow;
	function addRow(){		
		var n = $scope.tableConfig.rows.length+1; 
		$scope.tableConfig.pushRow([n,$scope.overP.userValue,$scope.temperature.userValue, $scope.Qr.userValue, $scope.Q.userValue, $scope.time.userValue]);
	}
	
	
	$scope.changeNames = function(){
		if($scope.terms == "normal"){
			$scope.butBotName = "Перевод в Ст.У.";
			$scope.rhoStName = "Плотность в Ст.У.&rho;ст";
			$scope.flowName = "Расход в Ст.У. Qст";
		}
		else{
			$scope.butTopName = "Перевод в Р.У.";
			$scope.butBotName = "Перевод в Н.У.";
			$scope.flowName = "Расход в Н.У. Qн";
			$scope.rhoStName = "ПЛотность в Н.У. &rho;н";
		}
	}
	
	
	function standartTermsToCurrent(){
		var overP = $scope.overP.getStandartValue();
		var t = $scope.temperature.getStandartValue();
		var airP = $scope.airP.getStandartValue();
		var t0 = ($scope.terms == "normal") ? 273.15 : 293.15;
		var Qct = +$scope.Qct.userValue;
		$scope.Qr.setStandartValue(Qct/t0*t*airP/(overP + airP) / (+$scope.kC.userValue));		 
		$scope.rho.userValue = ($scope.rhoSt.userValue * (overP + airP) * t0 / airP / t * $scope.kC.userValue).toFixed(DECIMALS);
		$scope.G.userValue = ($scope.rhoSt.userValue * Qct).toFixed(DECIMALS);
	}

	function currentTermsToStandart(){
		var overP = $scope.overP.getStandartValue();
		var t = $scope.temperature.getStandartValue();
		var airP = $scope.airP.getStandartValue();
		var t0 = ($scope.terms == "normal") ? 273.15 : 293.15;		
		var Qr = $scope.Qr.getStandartValue();
		$scope.Qct.userValue = Qr * (overP + airP) * t0 / t / airP * $scope.kC.userValue;		
		$scope.rhoSt.userValue = ($scope.rho.userValue * t * airP / (overP + airP) / t0 / $scope.kC.userValue).toFixed(DECIMALS);
		$scope.G.userValue = ($scope.rho.userValue * Qr).toFixed(DECIMALS);
		
	}
		
}])