"use strict";
angular.module("emis-calc").controller("flowRangeCntr",["$scope", '$q' ,"ServerService", "ev200ConstService" ,flowRangeFunc]);

function flowRangeFunc($scope, $q, ServerService, ev200ConstService) {	
	$scope.medium = '0';	
	$scope.isSaturedSteam = true;
	$scope.duArray = ev200ConstService.constants.EV200_DN;
	$scope.temperature = {userValue: '20', userMu : 'C'};
	$scope.pressure = {userValue : '0' , userMy : "МПа"};
	$scope.rho0 = 1.2;
	$scope.variantPress = $scope.variantTemp = $scope.du = '0';
	$scope.mediumChange = mediumChange;
	$scope.createGraph = createGraph;
	$scope.addRow = addRow;
	$scope.calcAirSteamFlowRange = calcAirSteamFlowRange;
	$scope.fillTableAllDUs = fillTableAllDUs;
	var columnNames = ["№","Ду", "Изб. давление", "Плотность", "Qmin объем.", "Qmax объем.", "Дип.дипазон", "Qmin масс.", "Qmax масс."];
	$scope.tableConfig = {columnNames: columnNames, rows : []};
	$scope.chartConfig = {

			  options: {
			      //This is the Main Highcharts chart config. Any Highchart options are valid here.
			      //will be overriden by values specified below.
			      chart: {
			          type: 'spline'
			      },
			      tooltip: {
			          style: {
			              padding: 10,
			              fontWeight: 'bold'
			          }
			      }
			  },
			  //The below properties are watched separately for changes.

			  //Series object (optional) - a list of series using normal Highcharts series options.
			  series: [{
			    
			  	}],
			  //Title configuration (optional)
			  title: {
			     text: 'Потери давления на расходомере'
			  },
			  //Boolean to control showing loading status on chart (optional)
			  //Could be a string if you want to show specific loading text.
			  loading: false,
			  //Configuration for the xAxis (optional). Currently only one x axis can be dynamically controlled.
			  //properties currentMin and currentMax provided 2-way binding to the chart's maximum and minimum
			  xAxis: {
				  type: 'spline',
				  minorTickInterval: 1,
				  title: {text: 'Давление, МПа'}
			  },
			  yAxis: {
				  title:{text: 'Расход'}
			  },
			  //Whether to use Highstocks instead of Highcharts (optional). Defaults to false.
			  useHighStocks: false,
			  //size (optional) if left out the chart will default to size of the div or something sensible.			  
			  //function (optional)
			  func: function (chart) {
			   //setup some logic for the chart
			  }
			};
	var consts = ev200ConstService.constants;
	
	function addRow(){
		var du = consts.EV200_DN[$scope.du];
		var n = $scope.tableConfig.rows.length; 
		$scope.tableConfig.pushRow([n,du,$scope.pressure.userValue, $scope.rho, $scope.qmin, $scope.qmax, '1:'+ Math.round($scope.qmax / $scope.qmin), $scope.qminMass, $scope.qmaxMass]);
	}
	function mediumChange(){
		switch (+$scope.medium) {
			case 0 :
				$scope.rho0 = 1.2;
				$scope.temperature.userValue = '20';
				break;
			case 1:
				$scope.rho0 = 0.6;
				//isSaturedSteamChanged();
				break;
			case 2:
				$scope.rho0 = 0.67;
				$scope.temperature.userValue = '20';
				break;
		};
		calcAirSteamFlowRange();
		
	};//AirSteamFlowRangeButtonClick
	
	function calcAirSteamFlowRange(pressure,temperature,diameterInd){
		var p = (pressure != null) ? pressure :  $scope.pressure.getStandartValue() + 101325;
		var t = (temperature != null) ? temperature : $scope.temperature.getStandartValue();
		var pressInd = +$scope.variantPress;
		var tInd = +$scope.variantTemp;
		var du = (diameterInd != null) ? diameterInd : +$scope.du;		
		var innerD = consts.EV200_InnerD[pressInd][du];
		if((tInd == 3) && (pressInd == 0) && (consts.EV200_DN[du] > 100)) innerD = consts.EV200_InnerD[2][4];
		var s = 3.1416 * innerD * innerD / 4;
		var rho0 = +$scope.rho0;
		
		var Qmin = 0;
		switch (tInd) {
			case 0:
				Qmin = consts.EV200_Qmin[0][1][du];
				break;
			case 1:
				Qmin = consts.EV200_QminExt[0][1][du];
				break;
			case 2:
				Qmin = consts.EV200_Qmin[1][1][du];
				break;
			case 3:
				Qmin = consts.EV200_Qmin[3][1][du];
				break;
		}
		var Qmax = consts.EV200_Qmax[1][du];
		var dens; 
		if (+$scope.medium == 1){
			if($scope.isSaturedSteam){
				t = null;
			}
			
			else {
				ServerService.getDataFromDll('wspPHASESTATEPT',[p,t], function(data){
					$scope.state = data.response;
				});
			}
			var tmp = inquireDenFromServer(p,t).then(function(data){
				dens = data.data.response;
				$scope.temperature.setStandartValue(data.data.temperature);
				return calculateFlows();
			});
		}
		else{
			dens = rho0  * p / 101325 * 293.15 / t;
			return calculateFlows();
		}	
		return tmp;
		
		
		function calculateFlows(){
			var k1 = Math.pow(Qmin / s / 0.0036, 2) * 1.2;
			var Vmax = Qmax / s / 0.0036;
			var k2 = 120000;					
			var Vmin ;
			if (tInd == 0 ){
				Vmin=(innerD <= 15) ? 2 : 1.5;
			}
			else if(tInd == 1){
				Vmin = (innerD <= 15) ? 2 : 1.3;
			}
			else if(tInd == 2){
				Vmin =  (innerD <= 15) ? 3 : 2;
			}
			else if(tInd == 3){
				Vmin =  (innerD <= 15) ? 4 : 3;
			}
		
			var qMax = (Math.min.apply(null,[Math.sqrt(k2/dens) , Vmax]) * s * 0.0036);
			$scope.qmax = qMax.toFixed(DECIMALS);
			var qMin = (Math.max.apply(null,[Math.sqrt(k1/dens) , Vmin]) * s * 0.0036);
			$scope.qmin = qMin.toFixed(DECIMALS);
			$scope.qminMass = (+$scope.qmin * dens).toFixed(DECIMALS);
			$scope.qmaxMass = (+$scope.qmax * dens).toFixed(DECIMALS);
			$scope.rho = (+dens).toFixed(DECIMALS);
			return {qmax : qMax, qmin : qMin, rho : dens, p : p};
		}
		
	}
	
	
	
	function inquireDenFromServer(pressure, temperature){
		var tempPromise =  (temperature == null) ? ServerService.getDataFromDll('wspTSP', [pressure], function(data){}) : temperature;				
		return $q.when(tempPromise).then(function(value){
				var temperature = (typeof(value) === 'number') ? value : value.data.response;
				return ServerService.getDataFromDll('wspDPT',[pressure, temperature], function(data){
					data.temperature = temperature;
			});
		});
		
	}
	
	function fillTableAllDUs(type){
		$scope.tableConfig.clearRows();
		var arrFor = (type == 'pressure') ? consts.Pressure_List : consts.EV200_DN; 		
		for(var n in arrFor){
			var p =  (type == 'pressure') ? arrFor[n] * 1000 + 101325 : null;	
			var duInd = (type == 'pressure') ? null : n; 
			var promise = $q.all([calcAirSteamFlowRange(p,null,duInd), n]).then(function(value){				
				var du = (type == 'pressure') ? consts.EV200_DN[$scope.du] : consts.EV200_DN[value[1]];
				var pressure = (type == 'pressure') ? consts.Pressure_List[value[1]] : $scope.pressure.userValue;
				if(pressure == 1600) {
					console.log(value);
				}
				var qmin = value[0].qmin.toFixed(DECIMALS-3);
				var qmax = value[0].qmax.toFixed(DECIMALS-3);
				var qminMass = (value[0].qmin * value[0].rho).toFixed(DECIMALS-3);
				var qmaxMass = (value[0].qmax * value[0].rho).toFixed(DECIMALS-3);
				$scope.tableConfig.pushRow([du,pressure, value[0].rho.toFixed(DECIMALS), qmin, qmax, '1:'+ Math.round(qmax / qmin), qminMass, qmaxMass]);
			})
			//promises.push([calcAirSteamFlowRange())
		}
	}
	
	function createGraph(){
		var pointsQmin = new Array();
		var pointsQmax = new Array();
		var arrPromises = [];
		for(var n in consts.Pressure_List){
			var p = consts.Pressure_List[n] * 1000 + 101325;
			var promise = $q.all([calcAirSteamFlowRange(p, null), n]).then(function(value){
				pointsQmin[value[1]] = [consts.Pressure_List[value[1]] / 1000, +value[0].qmin];
				pointsQmax[value[1]] = [consts.Pressure_List[value[1]] / 1000, +value[0].qmax];
			});
			arrPromises.push(promise);
			
		}		
		$q.all(arrPromises).then(function(){
			calcAirSteamFlowRange();
			$scope.chartConfig.series = [];
			$scope.chartConfig.series.push({data: pointsQmin, name: "Qmin" ,pointStart : 0.001});
			$scope.chartConfig.series.push({data: pointsQmax, name: "Qmax" ,pointStart : 0.001});
		})
				
	}
	
	
}