"use strict";
angular.module("emis-calc").controller("flowVelocity", ['$scope', '$http', 'ServerService',function($scope,$http,ServerService) {
	
	$scope.diameter = 15;
	$scope.density = 1000;
	$scope.volFlow = 0;
	$scope.massFlow = 0;
	$scope.velocity = 0;
	$scope.square = 0;
	var columnNames = ["№","Диаметр", "Масс. расход", "Объем. расход", "Скорость"];
	$scope.tableConfig = {columnNames: columnNames, rows : []};
	$scope.addRow = addRow;
	function addRow(){		
		var n = $scope.tableConfig.rows.length+1; 
		$scope.tableConfig.pushRow([n,$scope.diameter,$scope.massFlow, $scope.volFlow, $scope.velocity]);
	}
	
	ServerService.getMeasurmentUnits(function(data){
		$scope.measurmentUnits = data;
		var mu = data;
		$scope.volFlowMU = mu.volFlow.standart;
		$scope.massFlowMU = mu.massFlow.standart;
		$scope.densityUI = mu.density.standart;

	});
	
	
	$scope.changeDiameter = function(){
		var standartdiameter = $scope.diameter;
		var stVolFlow = ServerService.convertToStandart("volFlow", $scope.volFlowMU, $scope.volFlow);
		var value = (stVolFlow * 4) / (3.1416 * standartdiameter * standartdiameter * 0.0036);		
		this.velocity = (!(isNaN(value))) ? value.toFixed(DECIMALS) : 0;
		$scope.square = 3.1416 * standartdiameter * standartdiameter / 4;
	}
	
	$scope.changeDensity = function(){
		var stVolFlow = ServerService.convertToStandart("volFlow", $scope.volFlowMU, $scope.volFlow);
		var stDensity = ServerService.convertToStandart("density", $scope.densityUI, $scope.density);		
		var value = stVolFlow * stDensity;
		value = ServerService.convertToStandart("massFlow",$scope.massFlowMU,value);
		this.massFlow = (!isNaN(value)) ? value.toFixed(DECIMALS) : 0;
		this.velocity = (!isNaN(value)) ? ((stVolFlow * 4) / (3.1416 * this.diameter * this.diameter * 0.0036)).toFixed(5) : 0;			
	}
	
	$scope.changeMassFlow = function(){
		var stMassFlow = ServerService.convertToStandart("massFlow",$scope.massFlowMU,$scope.massFlow);
		var stDensity = ServerService.convertToStandart("density", $scope.densityUI, $scope.density);
		var stVolFlow = stMassFlow / stDensity;
		var volumeFlow = ServerService.convertToStandart("volFlow", $scope.volFlowMU, stVolFlow);
		this.volFlow = (!isNaN(volumeFlow)) ? volumeFlow.toFixed(DECIMALS) : 0;
		this.velocity = (!isNaN(volumeFlow)) ? ((stVolFlow * 4) / (3.1416 * this.diameter * this.diameter * 0.0036)).toFixed(5) : 0;		
	}
	
	$scope.changeVolFlow = function(){
		var stVolFlow = ServerService.convertToStandart("volFlow", $scope.volFlowMU, $scope.volFlow);
		var stDensity = ServerService.convertToStandart("density", $scope.densityUI, $scope.density);		
		var mFlow = ServerService.convertToStandart("massFlow",$scope.massFlowMU,stVolFlow * stDensity);
		this.massFlow = (!isNaN(mFlow)) ? mFlow : 0;		
		this.velocity = (!isNaN(stVolFlow)) ? ((stVolFlow * 4) / (3.1416 * this.diameter * this.diameter * 0.0036)).toFixed(10) : 0 ;		
	}
	
	$scope.changeVelocity = function(){
		var volFlowSt = this.velocity * 3.1416 * this.diameter * this.diameter * 0.0036 / 4;
		var volF = ServerService.convertToStandart("volFlow", $scope.volFlowMU, volFlowSt);
		this.volFlow = (!isNaN(volF)) ? volF : 0;
		var massFlowSt = volFlowSt * this.density;		
		this.massFlow = (!isNaN(massFlowSt)) ? ServerService.convertToStandart("massFlow", $scope.massFlowMU ,volFlowSt * this.density) : 0;
	}		
		
}]);