"use strict";
angular.module("emis-calc").controller("reinoldsCntr",["$scope", "ev200ConstService",reinoldsCntrFunc])
 
function reinoldsCntrFunc($scope, ev200ConstService) {	
	$scope.medium = '0';	
	$scope.duArray = ev200ConstService.constants.EV200_DN;
	$scope.du = '0';
	$scope.rho = {userMu : 'кг/м3', userValue : '1000'};
	$scope.dinVisc = {userMu : 'сП', userValue : '1'};
	$scope.cinVisc = {userMu : 'сСт', userValue : '1'};
	$scope.changeMedium = changeMedium;
	$scope.reinoldQCalc = reinoldsQCalc;
	$scope.reinoldsVCalc = reinoldsVCalc;
	$scope.QVReinoldsCalc = QVReinoldsCalc;
	
	
	function dyn2CinBtnClick(){
		var rho = $scope.rho.getStandartValue();
		var dynVisc = $scope.dinVisc.getStandartValue();
		$scope.cinVisc.setStandartValue(dynVisc / rho);
		reinoldsQCalc();
	}
	
	function cin2DynVisc(){
		var rho = $scope.rho.getStandartValue();
		var cinVisc = $scope.cinVisc.getStandartValue();
		$scope.dinVisc.setStandartValue(rho * cinVisc);
		reinoldsQCalc();
	}
	
	function changeMedium(){
		var rho = ev200ConstService.constants.MediumDensVisc[+$scope.medium][0];
		var dynVisc =  ev200ConstService.constants.MediumDensVisc[+$scope.medium][1];
		var cinVisc = dynVisc/rho * 1000;
		$scope.rho.setStandartValue(rho);
		$scope.dinVisc.setStandartValue(dynVisc/1000);
		$scope.cinVisc.setStandartValue(cinVisc);
		reinoldsQCalc();
	}
	
	function reinoldsQCalc(){
		var q = $scope.Q.getStandartValue();
		var cinVisc = $scope.cinVisc.getStandartValue();
		var d = ev200ConstService.constants.EV200_DN[+$scope.du];
		$scope.V = (q * 4 / 3.1416 / d / d / 0.0036).toFixed(DECIMALS);
		var s = 3.1416 * d * d / 4;
		$scope.Re = (q * d / cinVisc / s / 3.6).toFixed(DECIMALS);		
	}
	
	function reinoldsVCalc(){
		var v = $scope.V;
		var d = ev200ConstService.constants.EV200_DN[+$scope.du];
		var s = 3.1416 * d * d / 4;
		var q = v * s * 0.0036;
		$scope.Q.setStandartValue(q);
		reinoldsQCalc();
		$scope.V = v;
	}
	
	function QVReinoldsCalc(){
		var d = ev200ConstService.constants.EV200_DN[+$scope.du];
		var re = $scope.Re;
		var kv = $scope.cinVisc.getStandartValue();
		var s = 3.1416 * d * d / 4;
		var q = re * kv * s * 3.6 / d;
		$scope.Q.setStandartValue(q);
		$scope.V = q * 4 / 3.1416 / d / d / 0.0036;
	}
	
	function changeDU(){
		if ($scope.isMinQ == false) return;
		var medium = ($scope.medium == 0) ? 0 : 1;
		var qMin = ev200ConstService.constants.EV200_Qmin[0, medium, +$scope.du];
		$scope.Q.setStandartValue(qMin);
		reinoldsQCalc();
	}
}