'use strict';
angular.module("emis-calc").controller("emisCalcCntr", ["$scope" ,"$location","$http", 'ServerService', function($scope,$location, $http,ServerService) {
	
	//$scope.tabs.error = false;
	$scope.getClass = function (path) {		
		 return ($location.path().substr(0, path.length) === path) ? 'active' : '';
	}
	
	ServerService.getTabs(function(data){
		$scope.tabs = data;
	});
	
	/*$http.get('jsonData/tabs.json').success(function(data){
		ServerService.tabs = data;		
		$scope.tabs = ServerService.tabs;
	});
	$http.get('jsonData/measurmentUnits.json').success(function(data){
		
		ServerService.measurmentUnits = data;		
		//$scope.measurmentUnits = ServerService.measurmentUnits;
	});*/
}])