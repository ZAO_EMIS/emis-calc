"use strict";
angular.module("emis-calc").controller("calcEV205Cntr",["$scope", "ServerService", function($scope, ServerService) {
	ServerService.getMeasurmentUnits(function(data){
		
		$scope.measurmentUnits = data;
		$scope.QMU = data.volFlow.standart;
	});
	
	$scope.point = "0";
	$scope.Kz = 0;
	$scope.du = 0;
	changePoint();
	$scope.calcFactQ = function(){
		$scope.S = ((+$scope.du) * (+$scope.du) / 40 / 40 * (+$scope.Kv) * (+$scope.Kn) * (+$scope.Kz)).toFixed(DECIMALS);
		$scope.Qfact = (+$scope.Q *(+$scope.S)).toFixed(DECIMALS);
		
	};
	
	$scope.changePoint = changePoint;
	function changePoint(){
		if($scope.point == '0')	$scope.Kn = $scope.Kv = '0.84';
		else $scope.Kn = $scope.Kv = 1;
	}
	
		
}]);