"use strict";
angular.module("emis-calc").controller("pipePressureCntr", ["$scope", '$q', "ev200ConstService", function($scope, $q , ev200ConstService) {
	$scope.resistItems = ev200ConstService.constants.Steel_Mechprop;
	$scope.pipeSizeItems = ev200ConstService.constants.EV200_PipeSizes;
	$scope.resist = null;
	$scope.pipeSize = null;
	$scope.n = 5;
	$scope.thickness = 0;
	$scope.pressure = 0;	
	$('.input-dropdown input').addClass("form-control");
	
	var columnNames = ["№","Врем.сопротив", "Запас прочности", "Наруж. диаметр", "Толчища стенки", "Давление"];
	$scope.tableConfig = {columnNames: columnNames, rows : []};
	$scope.addRow = addRow;
	function addRow(){		
		var n = $scope.tableConfig.rows.length+1; 
		$scope.tableConfig.pushRow([n,$scope.resist,$scope.n, $scope.pipeSize, $scope.thickness, $scope.pressure]);
	}
	
	$scope.dropDownChange = function(item, modelName){
		$scope[modelName] = parseInt(item).toString()
		if(modelName == "pipeSize"){
			
			var indexA = item.indexOf('х') + 1;
			var indexB = item.indexOf(' ') ;
			$scope.thickness = item.substring(indexA,indexB).replace(',', '.');
			$scope.pipePressureCalc();
		}
	}
	
	$scope.pipePressureCalc = function() {
		$scope.pressure = (2 * $scope.thickness * $scope.resist / $scope.pipeSize / $scope.n).toFixed(DECIMALS);
	}
	
	$scope.pipeThicknessCalc = function(){
		$scope.thickness = ($scope.pressure * $scope.pipeSize * $scope.n / 2 / $scope.resist).toFixed(DECIMALS); 
	}
	
    $scope.filterStringList = function(userInput, array) {
        var filter = $q.defer();
        var normalisedInput = userInput.toLowerCase();

        var filteredArray = array.filter(function(item) {
          return item.toLowerCase().indexOf(normalisedInput) === 0;
        });

        filter.resolve(filteredArray);
        return filter.promise;
      };
}]);