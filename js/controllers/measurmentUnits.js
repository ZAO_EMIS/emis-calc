"use strict";
angular.module("emis-calc").controller("measurmentUnits",["$scope", "ServerService", function($scope, ServerService) {
	$scope.muFrom = 0;
	ServerService.getMeasurmentUnits(function(data){		
		$scope.measurmentUnits = data;
		$scope.unitsType = 'pressure';
		$scope.muFromUnit = $scope.measurmentUnits[$scope.unitsType].standart;
		$scope.muToUnit = $scope.measurmentUnits[$scope.unitsType].standart;
	});	
	$scope.calcMeas = function(){		
		 $scope.muTo = ServerService.convertTo(this.unitsType,this.muFromUnit,this.muFrom, this.muToUnit);
	}
	
	$scope.changeType = function(){
		$scope.muFromUnit = $scope.measurmentUnits[$scope.unitsType].standart;
		$scope.muToUnit = $scope.measurmentUnits[$scope.unitsType].standart;
	}
	

		
}])