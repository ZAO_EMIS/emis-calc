"use strict";
angular.module("emis-calc").service('ServerService', function($http, $q){
	
	var tabs = null;
	var measurmentUnits = null;
	getMeasurmentUnits(function(){});
	function getTabs(cb){
		if (tabs == null){
	       $http.get('jsonData/tabs.json').success(function (data) {
	         tabs = data;
	         cb(tabs);
	       });
		}
		else{
			cb(tabs);
		}
	}
	
	function getMeasurmentUnits(cb){
		if (measurmentUnits == null){
		       return  $http.get('jsonData/measurmentUnits.json').success(function (data) {
		    	 measurmentUnits = data;
		         cb(measurmentUnits);
		       });
			}
			else{
				return $q.when(cb(measurmentUnits));
			}
		
	}
	
	
	 var getDataFromDll =  function(funcName, arrayArgs, callBack){
		var url = 'http://127.0.0.1:8888?callback=JSON_CALLBACK&' + funcName;
		for (var i = 0 ; i < arrayArgs.length ; i++){
			var val = (!isNaN(arrayArgs[i])) ? arrayArgs[i] : 0;
			url += '&' + val;
		}
		return $http({
		    method: 'jsonp',
		    url: url, 
		    responseType: "json"
		}).success(callBack).error(callBack);
		
		
	}
	
	var ConvertTo = function(type, unitFrom, val, unitTo){
		var fromMUObj = measurmentUnits[type].measUnits[unitFrom];
		var toUnitObj = measurmentUnits[type].measUnits[unitTo];
		val = (typeof val == 'string') ?  val.replace(',', '.') : val;
		var result = ((+val + fromMUObj.shift) * fromMUObj.coeff)/toUnitObj.coeff - toUnitObj.shift  ;
		var defaultValue;		
		result = (isNaN(result)) ? 0 : result;
		return result;
	}
	
	
	var ConvertToStandart = function(type, unitFrom, val){
		var unitTo = measurmentUnits[type].standart;
		return ConvertTo(type,unitFrom,val,unitTo);
	}
	
	var convertFromStandart = function(type,val,unitTo){
		var unitFrom = measurmentUnits[type].standart;
		return ConvertTo(type,unitFrom,val,unitTo);
	}
	

	
	return {
		getDataFromDll : getDataFromDll,
		convertTo : ConvertTo,
		convertToStandart : ConvertToStandart,
		tabs : tabs,
		measurmentUnits : measurmentUnits,
		getTabs : getTabs,
		getMeasurmentUnits:getMeasurmentUnits,
		convertFromStandart: convertFromStandart
	}
		
})/*.run(function ($http){
	 
	$http.get('jsonData/tabs.json').success(function (data) {
    	tabs = data;
    });    
	
	$http.get('jsonData/measurmentUnits.json').success(function(data){
			measurmentUnits = data;		
			//$scope.measurmentUnits = ServerService.measurmentUnits;
		});
})*/;
	