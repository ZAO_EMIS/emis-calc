"use strict";
var tabs = {};
const DECIMALS = 5;
var emisCalc = angular.module("emis-calc", ["ngRoute",'highcharts-ng','inputDropdown'])
		.config(function($routeProvider){

			$routeProvider.when("/", {
				templateUrl : "views/flowVelocity.html"
			});
			$routeProvider.when("/flowVelocity", {
				templateUrl : "views/flowVelocity.html"
			});
			
			$routeProvider.when("/measurmentUnits", {
				templateUrl : "views/measurmentUnits.html"
			});			
			$routeProvider.when("/volumeStemFlowAndSelect", {
				templateUrl : "views/volumeStemFlowAndSelect.html"
			});	
			$routeProvider.when("/freqFlow", {
				controller : 'freqFlowCntr',
				templateUrl : "views/freqFlow.html"
			});	
			$routeProvider.when("/steamWaterProps", {
				controller : 'steamWaterCntr',
				templateUrl : "views/steamWaterProps.html"
			});	
			$routeProvider.when("/flowRUNU", {
				controller : 'flowRUNUCntr',
				templateUrl : "views/flowRUNU.html"
			});	
			$routeProvider.when('/pressureLoses', {
				controller: 'pressureLosesCntr',
				templateUrl : 'views/pressureLoses.html'
			});
			$routeProvider.when('/calcEV205', {
				controller: 'calcEV205Cntr',
				templateUrl : 'views/calcEV205.html'
			});
			$routeProvider.when('/flowEV200205', {
				controller: 'flowEV200205Cntr',
				templateUrl : 'views/flowEV200205.html'
			});
			$routeProvider.when('/pipePressure', {
				controller: 'pipePressureCntr',
				templateUrl : 'views/pipePressure.html'
			});
			$routeProvider.when('/reinolds', {
				controller: 'reinoldsCntr',
				templateUrl : 'views/reinolds.html'
			});
			$routeProvider.when('/flowRange', {
				controller: 'flowRangeCntr',
				templateUrl : 'views/flowRange.html'
			});
		})/*.run(function ($http, ServerService) {
		    $http.get('jsonData/tabs.json').success(function (data) {
		    	ServerService.tabs = data;
		    });
		    
			$http.get('jsonData/measurmentUnits.json').success(function(data){
				ServerService.measurmentUnits = data;		
				//$scope.measurmentUnits = ServerService.measurmentUnits;
			});
		});*/;
