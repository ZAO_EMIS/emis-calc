﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Reflection;


namespace wsp_server
{
    public class Wsp : IDisposable
    {
        private HttpListener listener;
        private readonly int port;
        private Thread listenerThread;
        private bool is_listening = true;
        public Wsp(int port = 8888)
        {
            this.port = port;
        }

        public void Dispose()
        {
            listener.Stop();
            listener.Stop();            
        }

        public void Start()
        {
            /*is_listening = true;
            listenerThread = new Thread(portListener);
            listenerThread.IsBackground = true;
            listenerThread.Name = "ListenerInquire";
            listenerThread.Start();*/
            portListener();
        }

        public void Stop()
        {
            is_listening = false;
            listener.Stop();
        }

        public void Suspend()
        {
            listener.Stop();
        }

        public void Resume()
        {
            this.Start();            
        }
        protected void portListener()
        {
            if (!HttpListener.IsSupported)
            {
                Console.WriteLine("Windows XP SP2 or Server 2003 is required to use the HttpListener class.");
                //return;
            }
            string prefixes = "http://*:8888/";
            // URI prefixes are required,            
            // Create a listener.
            listener = new HttpListener();
            // Add the prefixes.
            listener.Prefixes.Add(prefixes);

            listener.Start();
            // Note: The GetContext method blocks while waiting for a request. 
            while (is_listening)
            {
                HttpListenerContext context = listener.GetContext();
                HttpListenerRequest request = context.Request;
                // Obtain a response object.
                HttpListenerResponse response = context.Response;
                
                byte[] buff = new byte[1000];
                Console.WriteLine("Query: {0}", request.RawUrl);

                string inquireBody = request.RawUrl;
                string responseString = proceedInquire(inquireBody).Replace(',', '.');
                Console.WriteLine("Answer: {0}", responseString);
                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
                // Get a response stream and write the response to it.
                response.ContentLength64 = buffer.Length;
                System.IO.Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
                // You must close the output stream.
                output.Close();
            }
            listener.Stop();
        }


        static  string proceedInquire(string inquireBody)
        {
            string[] partsInquire = inquireBody.Split('&');
            partsInquire[0] = partsInquire[0].Remove(0, 2);
            object[] args = new object[partsInquire.Length - 2];
            for (int i = 2; i < partsInquire.Length; i++)
            {
                double value;
                string cleanStr = partsInquire[i].Replace('.', ',');
                if (double.TryParse(cleanStr, out value))
                {
                    args[i - 2] = value;
                }
            }
            MethodInfo method = typeof(DllWrapper).GetMethod(partsInquire[1]);
            if (method.GetParameters().Length == partsInquire.Length - 2)
            {
                //object[] argsO = (object[])args.ToArray(typeof(object));
                try
                {
                    object result = method.Invoke(method, args);//DllWrapper.wspDPT((double)args[0], (double)args[1]);
                    //double resultTyped = (result is double) ? (double)result : (int)result;//(method.Invoke(method, args)).ToString();
                    string resultStr = result.ToString();
                    if (partsInquire[1] == "wspPHASESTATEPT")
                    {
                        switch ((int)result)
                        {
                            case 1:
                                resultStr = "Жидкость";
                                break;
                            case 2:
                                resultStr = "Пар";
                                break;
                            default:
                                resultStr = "Вне диапазона";
                                break;
                        }
                    }
                    else if ((double)result < 0)
                    {
                        resultStr = "Ошибка";
                    }
                    string responseString = partsInquire[0] + @"({""response"" : """ + resultStr + @""" });";
                    return responseString;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.InnerException.Message);
                }                
            }
            return "";
        }
    }
}
