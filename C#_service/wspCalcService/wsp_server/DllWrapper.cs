﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;


namespace wsp_server
{
    internal static class DllWrapper
    {
        /*
         * 
         * */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern double wspDPT(double p, double tK);

        /*   Плотность пара на линии насыщения [кг/м3] как функция величин: температура t [K]:
         * 
         *  t – температура [K], тип: double;
         *  Возвращаемый результат – плотность пара на линии насыщения [кг/м3], тип: double.
         * */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern double wspDSST(double tK);

        /*   Удельная энтальпия [Дж/кг] как функция величин: давление p [Па], температура t [K]:
         *  wspHPT(p, t)
            где:

                p – давление [Па], тип: double;
                t – температура [K], тип: double;
                Возвращаемый результат – удельная энтальпия [Дж/кг], тип: double.

         * */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern double wspHPT(double p, double tK);

        /* Плотность в двухфазной области [кг/м3] как функция величин: температура t [K], степень сухости x [-]:
        wspDSTX(t, x)
        где:
            t – температура [K], тип: double;
            x – степень сухости [-], тип: double;
            Возвращаемый результат – плотность в двухфазной области [кг/м3], тип: double.
        */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern double wspDSTX(double tK, double dryRate);

        /*

        Температура на линии насыщения [K] как функция величин: давление p [Па]:

        wspTSP(p)

        где:

            p – давление [Па], тип: double;
            Возвращаемый результат – температура на линии насыщения [K], тип: double.
        */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern double wspTSP(double p);

        /* 
        Удельная энтальпия [Дж/кг] как функция величин: давление p [Па], температура t [K], степень сухости x [-]:
        wspHPTX(p, t, x)
        где:

            p – давление [Па], тип: double;
            t – температура [K], тип: double;
            x – степень сухости [-], тип: double;
            Возвращаемый результат – удельная энтальпия [Дж/кг], тип: double.
        */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern double wspHPTX(double p, double tK, double dryRate);


        /* 
        Кинематическая вязкость пара на линии насыщения [м2/с] как функция величин: температура t [K]:
        wspKINVISSST(t)
        где:
            t – температура [K], тип: double;
            Возвращаемый результат – кинематическая вязкость пара на линии насыщения [м2/с], тип: double.
        */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern double wspKINVISSST(double tK);

        /* 
        Динамическая вязкость пара на линии насыщения [Па·с] как функция величин: температура t [K]:
        wspDYNVISSST(t)
        где:
            t – температура [K], тип: double;
            Возвращаемый результат – динамическая вязкость пара на линии насыщения [Па·с], тип: double.
        */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern double wspDYNVISSST(double tK);

        /* 
        Давление на линии насыщения [Па] как функция величин: температура t [K]:
        wspPST(t)
        где:
            t – температура [K], тип: double;
            Возвращаемый результат – давление на линии насыщения [Па], тип: double.
        */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern double wspPST(double tK);

        /*  Кинематическая вязкость [м2/с] как функция величин: давление p [Па], температура t [K], степень сухости x [-]:
            wspKINVISPTX(p, t, x)
            где:
                p – давление [Па], тип: double;
                t – температура [K], тип: double;
                x – степень сухости [-], тип: double;
                Возвращаемый результат – кинематическая вязкость [м2/с], тип: double.
        */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern double wspKINVISPTX(double p, double tK, double dryRate);

        /*  Динамическая вязкость [Па·с] как функция величин: давление p [Па], температура t [K], степень сухости x [-]:
            wspDYNVISPTX(p, t, x)
            где:
                p – давление [Па], тип: double;
                t – температура [K], тип: double;
                x – степень сухости [-], тип: double;
                Возвращаемый результат – динамическая вязкость [Па·с], тип: double.
        */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern double wspDYNVISPTX(double p, double tK, double dryRate);

        /* 

            Область фазового состояния как функция величин: давление p [Па], температура t [K]:
            wspPHASESTATEPT(p, t)
            где:

                p – давление [Па], тип: double;
                t – температура [K], тип: double;
                Возвращаемый результат – область фазового состояния, тип: long.
            */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern int wspPHASESTATEPT(double p, double tK);

        /*
            Динамическая вязкость [Па·с] как функция величин: давление p [Па], температура t [K]:
            wspDYNVISPT(p, t)
            где:

                p – давление [Па], тип: double;
                t – температура [K], тип: double;
                Возвращаемый результат – динамическая вязкость [Па·с], тип: double.
            */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern double wspDYNVISPT(double p, double tK);

        /*
        Кинематическая вязкость [м2/с] как функция величин: давление p [Па], температура t [K]:
        wspKINVISPT(p, t)
        где:

            p – давление [Па], тип: double;
            t – температура [K], тип: double;
            Возвращаемый результат – кинематическая вязкость [м2/с], тип: double.
        */
        [DllImport("okawsp6.dll", SetLastError = true)]
        public static extern double wspKINVISPT(double p, double tK);

    }
}
