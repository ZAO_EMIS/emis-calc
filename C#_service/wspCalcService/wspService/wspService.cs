﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace wspService
{
    public partial class wspService : ServiceBase
    {
        private wsp_server.Wsp wspSever;
        public wspService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            this.wspSever = new wsp_server.Wsp();
            this.wspSever.Start();
        }

        protected override void OnStop()
        {
            this.wspSever.Stop();
        }

        protected override void OnContinue()
        {
            this.wspSever.Resume();
        }
        protected override void OnPause()
        {
            this.wspSever.Suspend();
        }

    }
}
