/**
 * 
 */
describe("flowVelocityCntr test", function(){
	var mockScope = {};
	
	var controller, temp;
	beforeEach(angular.mock.module('emis-calc'));
	beforeEach(angular.mock.inject(function($controller, $rootScope, $httpBackend){
		mockScope= $rootScope.$new();
		temp = $httpBackend;
		controller = $controller('freqFlowCntr', {
			$scope: mockScope
		});
	
	}));	
	
	it("change diameters", function(){
		//			
		mockScope.diameter = 0;
		mockScope.medium = 0;		
		mockScope.changeDiameter();
		expect(mockScope.fMax).toBeCloseTo(478.93,2);
		expect(mockScope.time).toBeCloseTo(2.1,1);
		expect(mockScope.kf).toBeCloseTo(0.0029, 4);
		
		mockScope.diameter = 5;
		mockScope.medium = 0;		
		mockScope.changeDiameter();
		expect(mockScope.fMax).toBeCloseTo(124.41,2);
		expect(mockScope.time).toBeCloseTo(8.2,1);
		expect(mockScope.kf).toBeCloseTo(0.2389, 4);
		
		mockScope.kf = 0.2389;
		mockScope.Q = 12;
		mockScope.changeFlow();
		expect(mockScope.freq).toBeCloseTo(13.95283,4);
		expect(mockScope.time).toBeCloseTo(73.4,1);
		
		mockScope.kf = 0.5;
		mockScope.Q = 32;
		mockScope.changeFlow();
		expect(mockScope.freq).toBeCloseTo(17.77777,4);
		expect(mockScope.time).toBeCloseTo(57.6,1);
		
		mockScope.freq = 2;
		mockScope.changeFreq();
		expect(mockScope.Q).toBeCloseTo(3.6,1);
		expect(mockScope.time).toBeCloseTo(512,0);
		
		mockScope.freq = 122;
		mockScope.changeFreq();
		expect(mockScope.Q).toBeCloseTo(219.6,1);
		expect(mockScope.time).toBeCloseTo(8.4,1);
		
		
	});
	
	
});