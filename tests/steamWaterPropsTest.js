/**
 * 
 */
describe("flowVelocityCntr test", function(){
	var mockScope = {};
	var q;
	var controller, temp;
	beforeEach(angular.mock.module('emis-calc'));
	beforeEach(angular.mock.inject(function($controller, $rootScope, $httpBackend,$q){
		mockScope= $rootScope.$new();
		temp = $httpBackend;
		q = $q;
		controller = $controller('flowVelocity', {
			$scope: mockScope
		});
	
	}));
	var response = readJSON('jsonData/measurmentUnits.json');
	
	it("exc pressure", function(){
		//
		
		temp.whenGET('jsonData/measurmentUnits.json').respond(response);
		temp.flush();
		mockScope.pressure = 0;
		mockScope.temperature = 0;
		mockScope.dynViscUI = "мПа*с";
		mockScope.entalphyUI = "Дж/кг";
		//mockScope. = 0;
		q.when([mockScope.calculateEnvirement]).then(function(){
			expect(mockScope.entalphy).toBeCloseTo(0,3);
		});
				
	});
	
	
});