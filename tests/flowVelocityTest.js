/**
 * 
 */
describe("flowVelocityCntr test", function(){
	var mockScope = {};
	
	var controller, temp;
	beforeEach(angular.mock.module('emis-calc'));
	beforeEach(angular.mock.inject(function($controller, $rootScope, $httpBackend){
		mockScope= $rootScope.$new();
		temp = $httpBackend;
		controller = $controller('flowVelocity', {
			$scope: mockScope
		});
	
	}));
	var response = readJSON('jsonData/measurmentUnits.json');
	
	it("change diameters", function(){
		//
		
		temp.whenGET('jsonData/measurmentUnits.json').respond(response);
		temp.flush();
		mockScope.diameter = 10;
		mockScope.density = 1000;
		mockScope.massFlow = 10;
		mockScope.changeMassFlow();
		
		mockScope.changeDiameter();
		expect(mockScope.velocity).toBeCloseTo(0.03537,4);
		expect(mockScope.volFlow).toBeCloseTo(0.01, 2);
		expect(mockScope.square).toBeCloseTo(78.54, 2);										
		
		mockScope.diameter = 1;
		mockScope.changeDiameter();
		expect(mockScope.volFlow).toBeCloseTo(0.01,2);
		expect(mockScope.velocity).toBeCloseTo(3.53677, 5);
		expect(mockScope.square).toEqual(0.7854);
		
		
				
	});
	
	it("change density", function(){
		temp.whenGET('jsonData/measurmentUnits.json').respond(response);
		temp.flush();
		
		mockScope.volFlow = 100;
		mockScope.diameter = 40;
		
		mockScope.density = 1000;		
		mockScope.changeDensity();		
		expect(mockScope.massFlow).toBeCloseTo(100000,0);
		
		mockScope.density = 0.5555;
		mockScope.changeDensity();		
		expect(mockScope.massFlow).toBeCloseTo(55.55,2);
				
	});
	it("change Flows", function(){
		temp.whenGET('jsonData/measurmentUnits.json').respond(response);
		temp.flush();
		
		
		mockScope.diameter = 40;		
		mockScope.density = 1000;
		
		mockScope.massFlow = 100;
		mockScope.changeMassFlow();		
		expect(mockScope.volFlow).toBeCloseTo(0.1,1);
		expect(mockScope.velocity).toBeCloseTo(0.022105, 5);
		
		mockScope.massFlow = 0.555;
		mockScope.changeMassFlow();		
		expect(mockScope.volFlow).toBeCloseTo(0.000555,5);
		expect(mockScope.velocity).toBeCloseTo(0.000123, 5);
		
		mockScope.volFlow = 123;
		mockScope.changeVolFlow();		
		expect(mockScope.massFlow).toBeCloseTo(123000,0);
		expect(mockScope.velocity).toBeCloseTo(27.1889, 4);
		
		mockScope.volFlow = 0.123;
		mockScope.changeVolFlow();		
		expect(mockScope.massFlow).toBeCloseTo(123,0);
		expect(mockScope.velocity).toBeCloseTo(0.0271889, 5);
				
	});
	
	it("change velocity", function(){
		temp.whenGET('jsonData/measurmentUnits.json').respond(response);
		temp.flush();
		
		
		mockScope.diameter = 40;		
		mockScope.density = 1000;
		
		
		mockScope.velocity = 0.123;
		mockScope.changeVelocity();		
		expect(mockScope.massFlow).toBeCloseTo(556.44049,0);
		expect(mockScope.volFlow).toBeCloseTo(0.55644, 5);
		
		mockScope.velocity = 7;
		mockScope.changeVelocity();		
		expect(mockScope.massFlow).toBeCloseTo(31667.328,3);
		expect(mockScope.volFlow).toBeCloseTo(31.667328, 5);
				
	});
});